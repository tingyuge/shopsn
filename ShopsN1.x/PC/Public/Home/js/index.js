$(function(){
	//头部广告关闭
	$('#advertisement_delete').on('click',function(){
		$(this).parent('.advertisement').remove();
	});
	//购物车
	$('#cart').on('mouseenter',function(){
		$('#cart .catr_none').css('display','block');
		$('#cart .catr_block').addClass('active');
	}).on('mouseleave',function(){
		$('#cart .catr_none').css('display','none');
		$('#cart .catr_block').removeClass('active');
	});
	//二级菜单
	$('#nav_li1').on('mouseenter',function(){
		$(this).addClass('active');
		$('#menu').css('display','block');
	}).on('mouseleave',function(){
		$(this).removeClass('active');
		$('#menu').css('display','none');
	});
	$('#menu  .mainCate').on('mouseenter',function(){
		$('#menu .mainCate').removeClass('active');
		$(this).addClass('active');
		$('#menu .mainCate ul').eq($(this).index()).css('display','block');
	}).on('mouseleave',function(){
		$('#menu .mainCate ul').css('display','none');
	})
	$('#menu').on('mouseleave',function(){
		$('#option .mainCate').css('display','none');
		$('#menu .mainCate').removeClass('active');
	});
	//轮播
	var iNow = 0;
	var timer = null;
	var Width = $('#banner_parent li').eq(0).outerWidth();
	$('#btn_parent a').on('click',function(){
		iNow = $(this).index();
		$('#btn_parent a').removeClass('active');
		$(this).addClass('active');
		$('#banner_parent').animate({left:-$(this).index()*$('#banner_parent li').outerWidth()});
	});
	function tab(){
		$('#btn_parent a').removeClass('active');
		$('#btn_parent a').eq(iNow%$('#btn_parent a').length).addClass('active');
		$('#banner_parent').animate({left:-iNow*$('#banner_parent li').outerWidth()});
	}
	$('#btn_parent a').on('mouseenter',function(){
		iNow = $(this).index();
		clear = setTimeout(function(){
			tab();
		},100)
	});
	$('#btn_parent a').on('mouseleave',function(){
		clearInterval(clear);
	})
	if(document.getElementById('banner_parent'))
	{
		//轮播
		var oBanner = document.getElementById('banner_parent');
		var Length = $('#banner_parent li').length;
		oBanner.innerHTML += oBanner.innerHTML;
		var iNow = 0;
		var timer = null;
		var clear = null;
		
		var Width = $('#banner_parent li').eq(0).outerWidth();
	}
	

	function tab(){
		$('#btn_parent a').removeClass('active');
		$('#btn_parent a').eq(iNow%$('#btn_parent a').length).addClass('active');
		$('#banner_parent').animate({left:-iNow*$('#banner_parent li').outerWidth()});
	}
	$('#btn_parent a').on('mouseenter',function(){
		iNow = $(this).index();
		clear = setTimeout(function(){
			tab();
		},100)
	});
	$('#btn_parent a').on('mouseleave',function(){
		clearInterval(clear);
	})
	//自动轮播
	function carousel(){
		iNow++;
		tab();
		$('#banner_parent').animate({left:-iNow*$('#banner_parent li').outerWidth()},function(){
			if(iNow >= Length){
				iNow = 0;
				$('#banner_parent').css('left',0);
			}
		});
	}
	timer = setInterval(carousel,5000);
	$('#banner').on('mouseenter',function(){
		clearInterval(timer);
	}).on('mouseleave',function(){
		timer = setInterval(carousel,5000);
	}); 
	
	function useless(){
		iNow++;
		if(iNow >= $('#banner_parent li').length){
			iNow = 0;
			$('#banner_parent').css('left',0);
			$('#btn_parent a').eq(0).addClass('active');
		}
		$('#btn_parent a').removeClass('active');
		$('#btn_parent a').eq(iNow).addClass('active');
		$('#banner_parent').animate({left:-iNow*$('#banner_parent li').outerWidth()},function(){
			if(iNow >= $('#banner_parent li').length-1){
				iNow = 0;
				$('#banner_parent').css('left',0);
				$('#btn_parent a').eq(0).addClass('active');
			}
		});
	}
	//广告换页效果
	;(function(){
		
		if(document.querySelector('#advertisement_parent ul'))
		{
			var oUl = $('#advertisement_parent ul');
			var aLi = $('#advertisement_parent li');
			var oUl2 = document.querySelector('#advertisement_parent ul');
			var Width = $(aLi).outerWidth();
			var iNow=0;
			var bFlag = false;
			//滑入显示按钮
			$('#advertisement_parent').on('mouseenter',function(){
				$('#advertisement_parent span').css('display','block');
			}).on('mouseleave',function(){
				$('#advertisement_parent span').css('display','none');
			});
			var Length = $('#advertisement_parent li').length;
			oUl2.innerHTML += oUl2.innerHTML;
			oUl.css('width',$('#advertisement_parent li').length*Width);
			// 右按钮
			$('#advertisement_btn_buttom').on('click',function(){
				if(bFlag == true)return;
				bFlag = true;
				if(iNow >= Length){
					iNow = 0;
					$('#advertisement_parent ul').css('left',0);
				}
				iNow++;
				oUl.animate({left:-iNow*Width},500,function(){
					bFlag = false;
					if(iNow >= Length){
						iNow = 0;
						$('#advertisement_parent ul').css('left',0);
					}
				});
			});
			//左按钮
			$('#advertisement_btn_top').on('click',function(){
				if(bFlag == true)return;
				bFlag = true;
				iNow--;
				if(iNow < 0){
					iNow = Length;
					oUl.css('left',-Length*Width);
					iNow--;
				}
				oUl.animate({left:-iNow*Width},500,function(){
					if(iNow <= 0){
						iNow = Length;
						oUl.css('left',-Length*Width);
					}
					bFlag = false;
				});
			});
			
			//内容滑入效果
			$('.groundfloor_con_center').on('mouseenter',function(){
				$('.groundfloor_con_center span').eq($(this).index()).animate({left:$(this).outerWidth()-$('.groundfloor_con_center span').outerWidth()});
			}).on('mouseleave',function(){
				$('.groundfloor_con_center span').eq($(this).index()).css('left',-$('.groundfloor_con_center span').outerWidth());
			});
			
		}
		
		$(document).on('scroll',function(){
			var scrollTop = $(document).scrollTop();
			if($('#groundfloor_one').offset().top - $(window).scrollTop() < 168 && $('#groundfloor_one').offset().top - $(window).scrollTop() > -3200){
				$('#side_nev').css('display','block');
			}else{
				$('#side_nev').css('display','none');
			}
		});
		var txt = new Array();
		$('.groundfloor_con .groundfloor_top h2').each(function(){
			txt[$(this).attr('number')] = $(this).text();
		});
		if (txt != null)
		{
			for(var i in txt)
			{
				$("#side_nev .etitle").eq(i).text(Tool.getShortFileName(txt[i], 3));
			}
		}
		$('.side_nev li').on('mouseenter',function(){
			$('.side_nev .etitle1').eq($(this).index()).css('display','none');
			$('.side_nev .etitle').eq($(this).index()).css({display:'block',color : 'gray'});
		}).on('mouseleave',function(){
			$('.side_nev .etitle1').css('display','block');
			$('.side_nev .etitle').css('display','none');
		});
	})();
});

function gotofloor(thiz) {
    dataslide = $(thiz).attr('data-title');
    var pos = $('.data-slide[data-title="' + dataslide + '"]').offset().top;// 获取该点到头部的距离
    $("html,body").animate({
        scrollTop: pos - 100
    }, 800);
    
}

//搜素
function isSuccess()
{
	//获取子节点
	return $('.search_input').find('input[type="text"]').val() == '' ? false : true;
}
var Tool = {
	//
	getChildNodes : function(ele)
	{
	   if(!(ele instanceof Object))
	   {
		   return [];
	   }
	   var childArr=ele.children || ele.childNodes,
	         childArrTem=new Array();  //  临时数组，用来存储符合条件的节点
	   console.log(ele);
	   for(var i=0,len=childArr.length;i<len;i++){
	        if(childArr[i].nodeType==1){
	            childArrTem.push(childArr[i]);
	        }
	    }
	    return childArrTem;
	},	
	//得到字符总数
	getChars : function (str) {
		 var i = 0;
		 var c = 0.0;
		 var unicode = 0;
		 var len = 0;
		 if (str == null || str == "") {
		  return 0;
		 }
		
		 len = str.length;
		 for(i = 0; i < len; i++) {
		   unicode = str.charCodeAt(i);
		   if (unicode < 127) { //判断是单字符还是双字符
			   c += 1;
		   } else {  //chinese
			   c += 2;
		   }
		 }
		 return c;
	},
	sb_strlen : function (str) {
		    return this.getChars(str);
	},
	//截取字符
	sb_substr : function (str, startp, endp) {
	    var i=0; c = 0; unicode=0; rstr = '';
	    var len = str.length;
	    var sblen = this.sb_strlen(str);
	    if (startp < 0) {
	        startp = sblen + startp;
	    }
	    
	    if (endp < 1) {
	        endp = sblen + endp;// - ((str.charCodeAt(len-1) < 127) ? 1 : 2);
	    }
	    // 寻找起点
	    var unicode = '';
	    for(i = 0; i < len; i++) {
	        if (c >= startp) {
	            break;
	        }
	        unicode = str.charCodeAt(i);
		    if (unicode < 127) {
		    	c += 1;
		    } else {
		   	 	c += 2;
		    }
	    }
		 // 开始取
		 for(i = i; i < len; i++) {
		     unicode = str.charCodeAt(i);
		     if (unicode < 127) {
		    	 c += 1;
		     } else {
		    	 c += 2;
		     }
		     rstr += str.charAt(i);
		     if (c >= endp) {
		    	 break;
		     }
		 }
		 return rstr;
	},
	//调用示例：
	getShortFileName :	function(filename, length) {
	    var short_filename = filename;
	    if (this.sb_strlen(short_filename) > length) {
	        short_filename = this.sb_substr(short_filename, 0, length);
	    }
	    return short_filename;
	}	
};

function make_qr_code(){
	layer.open({
		type: 2,
		title:'二维码',
		/*skin: 'layui-layer-rim', //加上边框*/
		area: ['770px','360px'], //宽高
		content: "/index.php/Home/index/qr_code_show.html"
	});
}
