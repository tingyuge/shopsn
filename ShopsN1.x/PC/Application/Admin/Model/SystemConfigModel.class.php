<?php
namespace Admin\Model;

use Think\Model;

class SystemConfigModel extends Model
{
    private static $obj;
    public function saveData(array $data)
    {
        if (empty($data))
        {
            return false;
        }
        $classId = $data['class_id'];
        unset($data['class_id']);
        $sesData = serialize($data);
        $isHave = $this->where('class_id ="'.$classId.'"')->find();
        if (empty($isHave))
        {
            $isSuccess = $this->add(array(
                'class_id'     => $classId,
                'config_value' => $sesData
            ));
        } else {
           $isSuccess =  $this->where('class_id = "'.$classId.'"')->save(array(
                'config_value' => $sesData
            ));
        }
        return $isSuccess;
    }
    
    protected function _before_update(&$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
    
    protected function _before_insert(&$data, $options)
    {
        $data['update_time'] = time();
        $data['create_time'] = time();
        return $data;
    }
    public function getValue(array $options = null)
    {
        $data = $this->field('create_time,update_time', true)->where($options)->select();
        if (!empty($data))
        {
            foreach ($data as $key => &$value)
            {
                if (!empty($value['config_value']))
                {
                    $unData = unserialize($value['config_value']);
                    unset($data[$key]['config_value']);
                    $value = array_merge($value, $unData);
                }
            }
        }
        return $data;
    }
    public static function getInitnation()
    {
        $class = __CLASS__;
        
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
}