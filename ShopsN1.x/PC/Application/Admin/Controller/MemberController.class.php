<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Think\Auth;

//事业合伙人
class MemberController extends AuthController {
    public function member_list(){
        $goods_class = M('goods_class');
        $result_class = $goods_class->where('fid!=0 AND type=4')->order('sort_num ASC')->select();
        $this->assign('result_class',$result_class);
        $m = M('goods');
        $nowPage = isset($_GET['p'])?$_GET['p']:1;
        if(!empty($_POST['title'])){
            $where['title'] = array('like','%'.$_POST['title'].'%');
        }
        if(!empty($_POST['keyword'])){
            $where['keyword'] = array('like','%'.$_POST['keyword'].'%');
        }
        if(!empty($_POST['class_id'])){
            $where['class_id'] = array('eq',$_POST['class_id']);
        }
        $where['type'] = 4;//事业合伙人
        // page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
        $result = $m->where($where)->order('sort_num ASC')->page($nowPage.','.PAGE_SIZE)->select();

        $nid = count($result);
        foreach ($result as $k=>$v){
            $result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
            $result[$k]['nid'] = $nid--;
            $class_name = $goods_class->field('class_name,id')->where('id='.$v['class_id'])->find();
            $result[$k]['class_name'] = $class_name['class_name'];
        }
        //dump($result);
        //分页
        $count = $m->where($where)->count(id);		// 查询满足要求的总记录数
        $page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
        $show = $page->show();		// 分页显示输出
        $this->assign('page',$show);// 赋值分页输出
        $this->assign('result',$result);

        $this->display();
    }
    
    //隐藏或者打开
    public function class_hide(){
    	$m = M('GoodsClass');
    	$_POST['update_time'] = time();
    	$result = $m->save($_POST);
    	$this->ajaxReturn(1);	//操作成功
    	if($result){
    		$this->ajaxReturn(1);	//操作成功
    	}else{
    		$this->ajaxReturn(0);	//操作失败
    	}
    }


    //添加商品
    public function member_add(){
    if(!empty($_POST)){
			$upload = new \Think\Upload();// 实例化上传类
			$upload->maxSize = 3145728;// 设置附件上传大小
			$upload->exts = array('jpg', 'gif', 'bmp', 'png', 'jpeg');// 设置附件上传类型
			$upload->rootPath = './Public/Uploads/goods/';		//设置文件根目录
			//上传文件
			$info = $upload->upload();
			$_POST['pic_url'] = $info[0]['savepath'].$info[0]['savename'];	//上传文件的路径
			foreach ($info as $k=>$v){
				if($k >= 1){
					$arr[] = $info[$k]['savepath'].$info[$k]['savename'];
				}
			}
			$_POST['pic_tuji'] = serialize($arr);
			if(!$info) {		// 上传错误提示错误信息
				$this->error($upload->getError());
			}else{		// 上传成功
				$m = M('goods');
				$_POST['create_time'] = time();
				$_POST['type'] = 4;
				//dump($_POST);exit;
				if($m->add($_POST)){
					$this->success('添加成功');
				}else{
					$this->success('添加失败');
				}
			}
		}else{
			$m = M('goods_class');
			$result_class = $m->where('type=4 AND fid=0')->order('sort_num ASC')->select();
			$this->assign('result_class',$result_class);
			$this->display();
		}
    }
    
    public function ajaxgoods() {
    	$id = I('post.id');
    	$info = M('goods_class')->where('fid='.$id)->select();
    	if(empty($info)) {
    		$this->ajaxReturn(0);
    	}
    	$this->ajaxReturn($info);
    }
    
    
    //删除活动
    public function member_del(){
        $where['id'] = $_POST['id'];	//活动ID
        $m = M('goods');
        $result = $m->where($where)->delete();
        if($result){
            $data['code'] = '1';	//删除成功
            $this->ajaxReturn($data);
        }else{
            $data['code'] = '0';	//删除失败
            $this->ajaxReturn($data);
        }
    }

    //更新排序
    public function member_sort(){
        $m = M('goods');
        $str_id = explode(',', substr($_GET['str_id'],1));
        $str_sort = explode(',', substr($_GET['str_sort'],1));
        foreach ($str_id as $k=>$v){
            $data['sort_num'] = $str_sort[$k];
            $m->where('id='.$v)->save($data);
        }
        $this->ajaxReturn(1);
    }

    //商品编辑
    public function member_edit(){
    	if(!empty($_POST)){
    		$m = M('goods');
    		$where['id'] = $_POST['id'];	//活动ID
    		$upload = new \Think\Upload();// 实例化上传类
    		$upload->maxSize = 3145728;// 设置附件上传大小
    		$upload->exts = array('jpg', 'gif', 'bmp', 'png', 'jpeg');// 设置附件上传类型
    		$upload->rootPath = './Public/Uploads/goods/';		//设置文件根目录
    		//上传文件
    		$info = $upload->upload();
    		$_POST['pic_url'] = $info[0]['savepath'].$info[0]['savename'];	//上传文件的路径
    		foreach ($info as $k=>$v){
    			if($k >= 1){
    				$arr[] = $info[$k]['savepath'].$info[$k]['savename'];
    			}
    		}
    		$_POST['pic_tuji'] = serialize($arr);
    		if(empty($_POST['pic_url'])){
    			unset($_POST['pic_url']);
    		}
    		if($_POST['pic_tuji'] == 'N;'){
    			unset($_POST['pic_tuji']);
    		}
    		$_POST['update_time'] = time();		//更新时间
    		$result = $m->where($where)->save($_POST);
    		if($result){
    			$this->success('修改成功');
    		}else{
    			$this->error('修改失败');
    		}
    	}else{
    		$goods_class = M('goods_class');
    		$result_class = $goods_class->where('type=4 AND fid=0')->order('sort_num ASC')->select();
    		$this->assign('result_class',$result_class);
    		$m = M('goods');
    		$where['id'] = $_GET['id'];	//活动ID
    		$result = $m->where($where)->find();
    		$where2['id'] = $result['class_id'];
    		$class_name = $goods_class->where($where2)->find();
    		$result['class_name'] = $class_name['class_name'];
    		$this->assign('result',$result);
    		$result_tuji = unserialize($result['pic_tuji']);
    		$this->assign('result_tuji',$result_tuji);
    		$this->display();
    	}
    }

    //用户评论展示
    public function comment_list(){
        $m = M('comment');
        $nowPage = isset($_GET['p'])?$_GET['p']:1;
        if(!empty($_POST['title'])){
            $where['title'] = array('like','%'.$_POST['title'].'%');
        }
        if(!empty($_POST['keyword'])){
            $where['keyword'] = array('like','%'.$_POST['keyword'].'%');
        }
        // page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
        $result = $m->where($where)->order('id DESC')->page($nowPage.','.PAGE_SIZE)->select();
        $nid = count($result);
        foreach ($result as $k=>$v){
            $result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
            $result[$k]['nid'] = $nid--;
        }
        //分页
        $count = $m->where($where)->count(id);		// 查询满足要求的总记录数
        $page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
        $show = $page->show();		// 分页显示输出
        $this->assign('page',$show);// 赋值分页输出
        $this->assign('result',$result);
        $this->display();
    }

    //删除评论
    public function comment_del(){
        $where['id'] = $_POST['id'];	//评论ID
        $m = M('comment');
        $result = $m->where($where)->delete();
        if($result){
            $data['code'] = '1';	//删除成功
            $this->ajaxReturn($data);
        }else{
            $data['code'] = '0';	//删除失败
            $this->ajaxReturn($data);
        }
    }


    //商品分类列表
    public function class_list(){
        if(!empty($_POST)){
            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize = 3145728;// 设置附件上传大小
            $upload->exts = array('jpg', 'gif', 'bmp', 'png', 'jpeg');// 设置附件上传类型
            $upload->rootPath = './Public/Uploads/class/';      //设置文件根目录
            //上传文件
            $info = $upload->upload();
            $_POST['pic_url'] = $info['pic_url']['savepath'].$info['pic_url']['savename'];  //上传文件的路径
            //var_dump($_POST['pic_url']);die;
            if(!$info) {        // 上传错误提示错误信息
                $this->error($upload->getError());
            }else{
                $m = M('goods_class');
                $_POST['creatte_time'] = time();
                $_POST['type']=4;
                $result = $m->add($_POST);

                if($result){
                    $this->success('保存成功');
                }else{
                    $this->error('保存失败');
                }
            }

        }else{
            $m = M('goods_class');
            $where['fid'] = 0;
            $where['type']  = 4;
            $result = $m->where($where)->order('sort_num ASC')->select();
            foreach ($result as $k=>$v){
                $result[$k]['create_time'] = date('Y-m-d H:i:s',$result[$k]['create_time']);
            }
            foreach($result as $n=>$val){
                $result[$n]['vo'] = $m->where('fid='.$val['id'])->select();
            }
            $this->assign('result',$result);
            $this->display();
        }
    }
    
    //更新分类
    public function class_update(){
    	$m = M('goods_class');
    	$_POST['update_time'] = time();
    	$result = $m->save($_POST);
    	if($result){
    		$this->ajaxReturn(1);	//删除成功
    	}else{
    		$this->ajaxReturn(0);	//删除失败
    	}
    }

    //更新排序
    public function class_sort(){
    	$m = M('goods_class');
    	$str_id = explode(',', substr($_GET['str_id'],1));
    	$str_sort = explode(',', substr($_GET['str_sort'],1));
    	foreach ($str_id as $k=>$v){
    		$data['sort_num'] = $str_sort[$k];
    		$m->where('id='.$v)->save($data);
    	}
    	$this->ajaxReturn(1);
    }
    
    //删除分类
    public function class_del(){
    	$m = M('goods_class');
    	$where['id'] = $_POST['id'];
    	$checkone = M('goods')->where('class_id='.$where['id'])->find();
    	if(!empty($checkone)){
    		$this->ajaxReturn(2);//分类下有商品
    	}else{
    		$result = $m->where($where)->delete();
    		if($result){
    			$this->ajaxReturn(1);	//删除成功
    		}else{
    			$this->ajaxReturn(0);	//删除失败
    		}
    	}
    }
    
    //添加二级分类
    public function member_addpage(){
    	if(IS_POST){
    		$data = I('post.');
    		$data['fid'] = $_GET['id'];
    		$data['type'] = 4;
    		$goods_class = M('goods_class');
    		if($goods_class->create($data)){
    			if($goods_class->add($data)){
    				$this->success('子类添加成功',U('Leaguer/leaguer_addpage'));
    			}else{
    				$this->error('添加失败');
    			}
    		}else{
    			$this->error('添加失败');
    		}
    	}else{
    		$resone = M('goods_class')->where('fid=0')->select();
    		$this->assign('resone',$resone);
    		$this->display();
    	}
    }
    
    //属性管理
    public function goods_shuxing_add(){
    	if(!empty($_POST)){
    		$m = M('goods_shuxing');
    		$_POST['create_time'] = time();
    		$result = $m->add($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加错误');
    		}
    	}else{
    		$this->display();
    	}
    }
    
    public function goods_shuxing_edit(){
    	if(!empty($_POST)){
    		$m = M('goods_shuxing');
    		$_POST['update_time'] = time();
    		$result = $m->save($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加错误');
    		}
    	}else{
    		$m = M('goods_shuxing');
    		$where['id'] = $_GET['id'];
    		$result = $m->where($where)->find();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
    //属性列表
    public function goods_shuxing_list(){
    	$m = M('goods_shuxing');
    	$result = $m->where($where)->select();
    	foreach ($result as $k=>$v){
    		$result[$k]['shuxing_content'] = explode('|', $v['shuxing_content']);
    	}
    	$this->assign('result',$result);
    	$this->display();
    }
    
    public function goods_shuxing_del(){
    	$where['id'] = $_POST['id'];
    	$m = M('goods_shuxing');
    	$result = $m->where($where)->delete();
    	if($result){
    		$data['code'] = '1';	//删除成功
    	}else{
    		$data['code'] = '0';	//删除失败
    	}
    	$this->ajaxReturn($data);
    }
    
    //商品属性内容添加
    public function goods_shuxing_content_add(){
    	if(!empty($_POST)){
    		$m = M('goods_shuxing');
    		$_POST['update_time'] = time();
    		$_POST['shuxing_content'] = implode('|', $_POST['shuxing_content']);
    		$result = $m->save($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加错误');
    		}
    	}else{
    		$m = M('goods_shuxing');
    		$where['id'] = $_GET['id'];
    		$result = $m->where($where)->find();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
	
	//一级分类是否首页推荐
	public function shoutui(){
		if(IS_POST){
			$id = $_POST['id'];
			$findone = M('goods_class')->where('id='.$id)->find();
			$count = M('goods_class')->where('shoutui=1')->count();
				if($findone['shoutui'] == 0){
					if($count>9){
						$this->ajaxReturn(3);
					}else{
						$data['shoutui'] = 1;
						$resone = M('goods_class')->where('id='.$id)->save($data);
						$this->ajaxReturn(1);
					}
				}else if($findone['shoutui'] == 1){
						$data['shoutui'] = 0;
						$resone = M('goods_class')->where('id='.$id)->save($data);
						$this->ajaxReturn(2);
				}
		}
	}

}








