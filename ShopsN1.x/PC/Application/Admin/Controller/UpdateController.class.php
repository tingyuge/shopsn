<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Think\Auth;

class UpdateController extends AuthController{
     public function index(){
		 $app=M('app');
		 $rows= $app->select();
		 $this->assign('rows',$rows);
		  $this->display();
	 }
	 public function add(){
		  $app=M('app');
		if(IS_POST){
			$version_num=strtolower(I('version_num'));
			$map['version_num']=$version_num;
			$map['app_type']=I('app_type');
			$map['url']=I('url');
			$map['intro']=I('intro');
			$map['version']=str_replace('v','',$version_num);
			$map['create_time']=NOW_TIME;
			 $rst=$app->add($map);
			 //dump($app->getLastSql());exit;
			 if($rst){
				 $this->success('发布成功',U('Update/index'));
			 }else{
				 $this->error('发布失败',U('Update/index'));
			 }
		 }else{
			 $this->display();
		 }
	 }
	 public function del(){
		 $app=M('app');
		 $rst=$app->where(array('id'=>I('get.id')))->delete();
			 if($rst){
				 $this->success('删除成功');
			 }else{
				 $this->error('删除失败');
			 }
	 }
}