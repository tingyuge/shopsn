<?php
namespace Admin\Controller;

use Common\Controller\AuthController;
use Admin\Model\OrderModel;
use Common\Tool\Tool;
use Common\Model\UserAddressModel;
use Admin\Model\UserModel;
use Common\Model\OrderGoodsModel;
use Admin\Model\GoodsModel;
use Common\TraitClass\CancelOrder;
/**
 * 订单控制器
 * @author 王强
 * @copyright 亿速网络
 * @version  v1.1.2
 * @link http://yisu.cn
 */
class OrderController extends AuthController
{
    use CancelOrder;
    //订单列表 - 全部订单
    public function index()
    {
       if(!($cache = S('order')))
       {
           //获取全部订单状态
           $orderModel = new \ReflectionClass('Admin\Model\OrderModel');
        
           $data       = $orderModel->getConstants();
           Tool::connect('ArrayChildren');
           //删除不是状态的属性
           $data = Tool::deleteByCondition($data);
           //状态 改为键  value改为汉字提示；
           $data = Tool::changeKeyValueToPrompt($data, C('order'));   
           $cache = $data;
           S('order', $data, 86400);
       }
       $this->condition = $cache;   
       $this->display();
    }
    
    /**
     * ajax 获取数据 
     */
    public function ajaxGetData()
    {
//         showData($_POST, 1);
        // 获取订单
        //初始化页数排序
        $userArray = array();
        if (!empty($_POST['realname'])) {
            $userArray = UserAddressModel::getInitation()->getOrderByRealName($_POST);
        }
        
        Tool::isSetDefaultValue($_GET, array('page'));
        Tool::isSetDefaultValue($_POST, array('orderBy' => 'id'));
        Tool::connect('ArrayChildren');
        //获取订单数据
        $data = OrderModel::getInitation()->getOrder($userArray);
        Tool::connect('parseString');
        //传递用户地址模型
        $data = UserAddressModel::getInitation()->goodsAdressByOrder($data);
        $this->order        = $data;  
        $this->orderStatus  = S('order');
        $this->display();
    }
    
    /**
     * 订单详情
     */
    public function orderDetail()
    {
       
        $data = $this->getOrder();
       
        //传递给用户模型
        $userData = UserModel::getInitation()->userInfoByOrder($data, array('username', 'email', 'mobile'));
        
        $this->prompt($userData, null, '未知错误', false);
        
        //收货人信息
        $receive = UserAddressModel::getInitation()->receiveManByOrder($data,  array('id', 'user_id', 'status'), true);
        
        $this->prompt($receive, null, '未知错误', false);
       
        //传递给商品模型
        $goodsDatail = $this->getOrderGoodsInfo($data);
        
        $this->goods = $goodsDatail;
        $this->receive = $receive;
        $this->order = array_merge($data, $userData);
        $this->orderStatus  = S('order');
        $this->display();
    }
    
    /**
     * 发货
     */
    public function sendGoods()
    {
        $order = $this->getOrder();
        
        //收货人信息
        $receive = UserAddressModel::getInitation()->receiveManByOrder($order,  array('id', 'user_id', 'status'), true);
        
        $this->prompt($receive, null, '未知错误', false);
        
        $goodsInfo = $this->getOrderGoodsInfo($order);
        
        $this->prompt($goodsInfo, null, '未知错误', false);
        
        $this->order   =   array_merge($order, $receive);
        $this->goodsInfo = $goodsInfo;
        
        $this->display();
    }
    
    
    /**
     * 填写快递单号 
     */
    
    public function delivery()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('id', 'express_id')), true, array('id', 'express_id')) ? true : $this->error('参数错误');
        
        $orderStatus = OrderModel::getInitation()->getOrderStatusByUser($_POST['id']);
        
        if ($orderStatus != OrderModel::YesPaid && $orderStatus > OrderModel::AlreadyShipped)
        {
            $this->error('订单状态有误');
        }
        $_POST['order_status'] = OrderModel::AlreadyShipped;
        $status = OrderModel::getInitation()->save($_POST);
        
        $status ? $this->success('成功',U('index')) : $this->error('失败');
        
    }
    
    /**
     * 公共方法 
     */
    private function getOrder()
    {
        // 检测传值
        Tool::checkPost($_GET, array('is_numeric' => array('order_id')), true, array('order_id')) ? true : $this->error('参数错误');
        //获取订单信息
        $data = OrderModel::getInitation()->getOrderById($_GET['order_id']);
       
        $this->prompt($data, null, '订单有误', false);
        
        
        return $data;
    }
    
    private function getOrderGoodsInfo(array $data)
    {
        if (!is_array($data) || empty($data))
        {
            return array();
        }
        Tool::connect('parseString');
        //传递给商品订单模型
        $orderGoods  = OrderGoodsModel::getInitnation()->getGoodsId($data, array('order_id', 'goods_id', 'goods_num', 'goods_price'));
        //传递给商品模型
        $goodsDatail = GoodsModel::getInitation()->getOrderInfo($orderGoods, array('id as goods_id', 'title'));
        
        return $goodsDatail;
    }
    
    /**
     * 退货 
     */
    public function returnOrder()
    {
         $orderStatus = $this->flagOption(OrderModel::ReturnAudit, OrderModel::AuditSuccess);
         
         if (empty($orderStatus)) {
             $this->updateClient(null, '失败');
         }
         
         $orderStatus['url'] = U('cancelOrderMonery', array('idsaw' => $_POST['id']));
         
         $this->updateClient($orderStatus, '成功');
         
    }
    /**
     * 退款 
     */
    public function cancelOrderMonery()
    {
        $status = $this->cancelOrder();
      
        $update = false;
        if (!empty($status)) {
           $update = OrderModel::getInitation()->save(array(
               'order_status' => OrderModel::ReturnMonerySucess
           ),array(
               'where' => array('id' => $status['id'])
           ));
        }
        $this->prompt($update, null, '系统异常', false);
        
        $this->status = $status;
        $this->display();       
    }
    
    /**
     * 不予退款 
     */
    public function noReturn()
    {
        $orderStatus = $this->flagOption(OrderModel::ReturnAudit, OrderModel::AuditFalse);
         
        $this->updateClient($orderStatus, '成功');
    }
    
    /**
     * 退货，不退货操作 
     */
    private  function flagOption($status, $editStatus)
    {
        if (!is_numeric($status) || !is_numeric($editStatus)) {
            return false;
        }
        Tool::checkPost($_POST, array('is_numeric' => array('id')), true, array('id')) ? true : $this->error('参数错误');
        
        //获取订单
        $orderStatus = OrderModel::getInitation()->find(array(
            'field' => array('id', 'price_sum', 'order_status'),
            'where' => array('id' => $_POST['id'])
        ));
        
      
        if ( empty($orderStatus) ||$orderStatus['order_status'] != $status ) {
           return false;
        }
         
        $_POST['order_status'] = $editStatus;
         
        $status = OrderModel::getInitation()->save($_POST);
         
        
        
        return $status ? $status : false;
    }
}