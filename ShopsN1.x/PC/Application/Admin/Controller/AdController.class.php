<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Think\Auth;

//广告管理
class AdController extends AuthController {
    //广告列表
    public function ad_list(){
    	$m = M('ad');
    	$mm = M('ad_space');
    	
    	if(!empty($_GET['type'])){
    		$result = $m->order('sort_num ASC')->where(array('type'=>$_GET['type']))->select();
    	}else{
    		$result = $m->order('sort_num ASC')->select();
    	}
		foreach ($result as $k=>$v){
			$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
			if(!empty($v['ad_space_id'])){
				$where2['id'] = $v['ad_space_id'];
				$res = $mm->where($where2)->find();
			}
			$result[$k]['ad_space_name'] = $res['name'];
		}
		$this->assign('result',$result);

		$result_space = $m->select();
		$this->assign('result_space',$result_space);
		$this->display();
    }

    
    //添加广告
    public function ad_add(){
        
    	if(!empty($_POST)){
    	    C('GOODS_UPLOAD.rootPath', './Uploads/ad/');
    		$upload = new \Think\Upload( C('GOODS_UPLOAD'));// 实例化上传类
    		//上传文件
    		$info = $upload->upload();
    		$_POST['pic_url'] = str_replace('.', '', C('GOODS_UPLOAD.rootPath')).$info['pic_url']['savepath'].$info['pic_url']['savename'];	//上传文件的路径    		
    		if(!$info) {		// 上传错误提示错误信息
    			$this->error($upload->getError());
    		}else{		// 上传成功
    			$m = M('ad');
    			$_POST['create_time'] = time();
    			if($m->add($_POST)){
    				$this->success('添加成功');
    			}else{
    				$this->success('添加失败');
    			}
    		}
    	}else {
			$m = M('ad_space');
			$result = $m->select();
			$this->assign('result',$result);
    		$this->display();
    	}
    }
	
	//编辑广告
	public function ad_edit(){
		if(!empty($_POST)){
		     
		    $data =  $this->shg('./Uploads/ad/', $_FILES['pic_url']['name']);
			$m = M('ad');
			$_POST['update_time'] = time();
			$where['id'] = $_POST['id'];
			
			//删除原来的图片
			$this->delPic($data, $m, $where, 'pic_url');
			
			$_POST = array_merge($_POST, $data);
			$result = $m->where($where)->save($_POST);
			if($result){
				$this->success('修改成功');
			}else{
				$this->error('修改失败');	
			}
		}else{
			$m = M('ad');
			$where['id'] = $_GET['id'];
			$result = $m->where($where)->find();
			$m = M('ad_space');
			$res = $m->where('id='.$result['ad_space_id'])->find();
			$result['ad_space_name'] = $res['name'];
			$this->assign('result',$result);
	
			$result_space = $m->select();
			$this->assign('result_space',$result_space);
			$this->display();
		}		
	}
    
    //删除广告
    public function ad_del(){
    	$m = M('ad');
    	$where['id'] = $_POST['id'];
    	$result = $m->where($where)->delete();
    	if(!empty($result)){
    		$data['code'] = 1;
    		$data['message'] = '修改成功';
    		$this->ajaxReturn($data);
    	}else{
    		$data['code'] = 0;
    		$data['message'] = '修改失败';
    		$this->ajaxReturn($data);
    	}
    }
    
    //更新排序
    public function ad_sort(){
    	$m = M('ad');
    	$str_id = explode(',', substr($_GET['str_id'],1));
    	$str_sort = explode(',', substr($_GET['str_sort'],1));
    	foreach ($str_id as $k=>$v){
    		$data['sort_num'] = $str_sort[$k];
    		$m->where('id='.$v)->save($data);
    	}
    	$this->ajaxReturn(1);
    }
	
	//广告位列表
	public function ad_space(){
		if(!empty($_POST)){
			$m = M('ad_space');
			$_POST['update_time'] = time();
			$where['id'] = $_POST['id'];
			$result = $m->where($where)->save($_POST);
			if($result){
				$this->success('修改成功');
			}else{
				$this->error('修改失败');	
			}
		}else if(!empty($_GET['type'])){
			$m = M('ad_space');
			$result = $m->where(array('type'=>$_GET['type']))->select();
			foreach ($result as $k=>$v){
				$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
			}
			$this->assign('result',$result);
			$this->display();
		}else{
			$m = M('ad_space');
			$result = $m->select();
			foreach ($result as $k=>$v){
				$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
			}
			$this->assign('result',$result);
			$this->display();			
		}

	}
	
	//添加广告位
	public function ad_space_add(){
		if(!empty($_POST)){
			$m = M('ad_space');
			$_POST['create_time'] = time();
			if($m->add($_POST)){
				$this->success('添加成功');
			}else{
				$this->success('添加失败');
			}			
		}else{
			$this->display();
		}
	}

}




