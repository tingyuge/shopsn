<?php
namespace Home\Model;

use Think\Model;
use Common\Tool\Tool;
use Common\TraitClass\callBackClass;
use Think\Page;

/**
 * 订单模型 
 */
class OrderModel extends Model
{
    use callBackClass;
    // -1:取消订单,0 未支付，1已支付，2，发货中，3已发货，4已收货，5退货审核中，6审核失败，7审核成功，8退款中，9退款成功, 10：代发货，11待收货
    const CancellationOfOrder = -1;
    
    const NotPaid = 0;
    
    const YesPaid = 1;
    
    const InDelivery = 2;
    
    const AlreadyShipped = 3;
    
    const ReceivedGoods = 4;
    
    const ReturnAudit = 5;
    
    const AuditFalse  = 6;
    
    const AuditSuccess = 7;
    
    const Refund = 8;
    
    const ReturnMonerySucess = 9;
    
    const ToBeShipped = 10;
    
    const ReceiptOfGoods = 11;
    
    
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    protected function _before_update( &$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
     
    protected function _before_insert(&$data, $options)
    {
        $data['update_time'] = time();
        $data['create_time'] = time();
        $data['order_sn_id'] = Tool::toGUID();
        $data['user_id']     = $_SESSION['user_id'];
        $_SESSION['order_sn_id'] = $data['order_sn_id'];
        $data['order_status'] = 0;
        return $data;
    }
    
    /**
     * {@inheritDoc}
     * @see \Think\Model::add()
     */
    
    public function add($data='', $options=array(), $replace=false)
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
        
        return parent::add($data, $options, $replace);
    }
    
    /**
     * {@inheritDoc}
     * @see \Think\Model::save()
     */
    
    public function save($data='', $options=array())
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
    
        return parent::save($data, $options);
    }
    
    /**
     * 根据订单号获取商品编号 
     */
    public function getGoodsByOrderSn($orderSn)
    {
        if (empty($orderSn) || !is_numeric($orderSn))
        {
            return false;
        }
        
        return $this->where('order_sn_id = "%s"', $orderSn)->getField('price_sum');
    }
    /**
     * 获取 该用户下的全部订单 
     */
    public function getOrderByUser(array $whereValue, $status = null, $field = null, $default = 'select')
    {
        $field = $field === null ?  $this->getDbFields() : $field;
        if (is_array($field) && isset($field[0]))
        {
            $field[0] = 'id as order_id';
            
            foreach ($field as $key => $value)
            {
                if ('user_id' === $value)
                {
                    unset($field[$key]);
                }
            }
        }
        $count = $this->where('user_id = "%s"', $_SESSION['user_id'])->count();
        $page = new Page($count, 10);
        
        $where = $status === null ? null : $status;
        
        $data =  $this
            ->field($field)
            ->where('user_id = "%s"'.$where, $whereValue)->order('id DESC')
            ->limit($page->firstRow, $page->listRows)
            ->$default();
        cookie('page', $page->show(), 86400000000000);
        return $data;
    }
    
    /**
     * 获取订单状态 
     */
    public function getOrderStatusByUser($id, $filed='id')
    {
        if (!is_numeric($id) || empty($filed))
        {
            return false;
        }
        
        return $this->where($filed.' = "%s"', $id)->getField('order_status');
    }
}