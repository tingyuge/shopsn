<?php
namespace  Home\Model;

use Think\Model;

/**
 * 购物车 模型 
 */
class GoodsCartModel extends Model
{
    private static $obj ;
    
    protected $noSelect = 'create_time,update_time,user_id';
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    /**
     *@{\Think\Model add}
     */
    public function add($data='',$options=array(),$replace=false)
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
        
        return parent::add($data, $options, $replace);
    }
    
    
    protected function _before_insert(& $data, $options)
    {
        $data['create_time'] = time();
        $data['update_time'] = time();
        $data['user_id']     = $_SESSION['user_id'];
        return $data;
    }
    
    
    protected function _before_update(& $data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
    
    // 添加购物车
    public function addCart(array $data)
    {
        if (empty($data) || !is_array($data))
        {
            return array();
        }
        $result = $this->field('id,goods_num')->where('user_id = "'.$_SESSION['user_id'].'" and goods_id = "'.$_POST['goods_id'].'" and taocan_name = "'.$_POST['taocan_name'].'"')->find();
        //购物车中无商品，添加一条新信息，购物车中已有信息，则数量 +1
        $id = 0;
        if(empty($result)){
            $id   = $this->add($data);
        }else{
            $_POST['user_id'] = $_SESSION['user_id'];
            $_POST['goods_num'] = $result['goods_num'] + $_POST['goods_num'];
            $id = $this->where('id="'.$result['id'].'"')->save($_POST);
        }
        return empty($id) ? false : true;
    }
    /**
     * 获取购物车数量 
     */
    public function getCartCount(array $options)
    {
       $isSuccess =  \Common\Tool\Tool::checkPost($options);
       
       if (!$isSuccess) {
           return false;
       }
       
       $count = $this->where($options)->count();
       
       return $count;
    }
   
    /**
     * 获取最新添加购物车的商品 
     */
    public function getNewCartForGood(array $options, Model $model)
    {
        $isSuccess =  \Common\Tool\Tool::checkPost($options);
         
        if (!$isSuccess || !($model instanceof Model)) {
            return false;
        }
        
        $data =  $this->find($options);
       
        if (!empty($data)) {
           $goods = $model->field('title,pic_url,description')->where('id = "'.$data['goods_id'].'"')->find();
           $data = array_merge((array)$goods, $data);
        }
        return $data;
    }
    /**
     * 获取购物车中商品 
     */
    public function getCartGoodsByUserId($field, $where, $isSelect = true, $default='select')
    {
        if (empty($field) || empty($where))
        {
            return array();
        }
        return $this->field($field, $isSelect)->where($where)->$default();
    }
    /**
     * @return the $noSelect
     */
    public function getNoSelect()
    {
        return $this->noSelect;
    }

    /**
     * @param multitype:string  $noSelect
     */
    public function setNoSelect($noSelect)
    {
        $this->noSelect = $noSelect;
    }

} 