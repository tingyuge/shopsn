<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Home\Controller;
use Think\Controller;

//后台管理员
class RewardController extends Controller {
	//充值成为合伙人的提成
	/**
	 * 我的类型是合伙人的提成逻辑
	 *  $mon[0]=需要分配到的年份,建议按照付款时间来
	 *  $mon[1]=需要分配到的月份
	 */
	public function sVipLogic($id,$mon=array()){
		R("User/sVipLogic",array($id,$mon));
	}
	/**
	 * 我的类型是会员的提成逻辑
	 *  $mon[0]=需要分配到的年份,建议按照付款时间来
	 *  $mon[1]=需要分配到的月份
	 */
	public function vipLogic($id,$mon=array()){
		$year=$mon[0];
		$month=$mon[1];
		R("User/vipLogic",array($id,$year,$month));
	}

	/**
	 * @param $orders_num   订单号
	 * @return bool
	 */
	public function buy($id,$myjifen){
		//$orders_num=$order->where(array('id'=>$id))->find();
		$order=D('Orders');
		$orderMessage=$order->where(array('id'=>$id))->find();
		$orders_num=$orderMessage['orders_num'];
		if(count($orderMessage)==0){
			return 0;//不存在此订单
		}
		if($orderMessage['is_used']==1){
			return 9;//已经被使用
		}
		if($orderMessage['price_shiji']<=0){
			return -1;//支付金额为0或者为负数,不执行逻辑
		}
		if($orderMessage['pay_status']!=1){
			return 2;//订单未支付
		}
		if($orderMessage['pay_status']==1){
			//标记订单号已经用于提成，防止重复调用
			$order->where(array('orders_num'=>$orders_num))->save(array('is_used',1));
			//执行提成逻辑
			//$year=date('Y',$orderMessage['pay_time']);
			//$month=date('m',$orderMessage['pay_time']);
			$year=date('Y',NOW_TIME);
			$month=date('m',NOW_TIME);
			$this->makeCompanyData($year,$month);
			$money=$orderMessage['price_shiji'];
			$member_id=$orderMessage['user_id'];
			$member=M('member');
			$my=$member->where(array('id'=>$member_id))->find();
			$person=M('person_month');
			$company=M('company_month');
			$parent=$member->where(array('id'=>$my['pid']))->find();
			$pparent=$member->where(array('id'=>$parent['pid']))->find();
			$ppparent=$member->where(array('id'=>$pparent['pid']))->find();
			//公司部分先增加购物收益金额
			$company->where(array('year'=>$year,'month'=>$month))->setInc('shop_add',$money);
			//保存自己的分月记录
			$this->makeMonth($my['id'],$year,$month);
			$person->where(array('member_id'=>$my['id'],'year'=>$year,'month'=>$month))->setInc('my_buy_jf',$myjifen);
			if($my['grade_name']=='合伙人'){
				//增加自身购物返利积分
				$this->addMyMoney($my['id'],array('add_jf_currency'=>$myjifen));
				if($parent['id']){
					$this->makeMonth($parent['id'],$year,$month);
					$bfb=$this->getPercent(array(6,1));
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_money',$money);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_pay',$money*$bfb);
					if($parent['id']!=10000000){
						$company->where(array('year'=>$year,'month'=>$month))->setInc('shop_pay',$money*$bfb);
					}
					$this->addMyMoney($parent['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($parent['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($parent['id'],array('Gross_income'=>$money*$bfb));
				}
				if($pparent['id']){
					$this->makeMonth($pparent['id'],$year,$month);
					$bfb=$this->getPercent(array(6,2));
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_buy_money',$money);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_buy_pay',$money*$bfb);
					if($pparent['id']!=10000000) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
					}
					$this->addMyMoney($pparent['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($pparent['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($pparent['id'],array('Gross_income'=>$money*$bfb));
				}
				if($ppparent['id']){
					$this->makeMonth($ppparent['id'],$year,$month);
					$bfb=$this->getPercent(array(6,3));
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_buy_money',$money);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_buy_pay',$money*$bfb);
					if($ppparent['id']!=10000000) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
					}
					$this->addMyMoney($ppparent['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($ppparent['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($ppparent['id'],array('Gross_income'=>$money*$bfb));
				}
			}elseif($my['grade_name']=='会员'){
				//增加自身购物返利积分
				$this->addMyMoney($my['id'],array('add_jf_currency'=>$myjifen/2));
				$this->addMyMoney($my['id'],array('add_jf_limit'=>$myjifen/2));
				//上级三级之内的会员增加收益
				if($parent['id'] && $parent['grade_name']=="会员" && $this->isVipEnd($parent['id'])){
					$this->makeMonth($parent['id'],$year,$month);
					$bfb=$this->getPercent(array(2,1));
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_money',$money);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_pay',$money*$bfb);//
					//两个积分账户
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$money*$bfb/2);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$money*$bfb/2);
					$this->addMyMoney($parent['id'],array('add_jf_currency'=>$money*$bfb));
					$this->addMyMoney($parent['id'],array('add_jf_limit'=>$money*$bfb));
					if($parent['id']!=10000000) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_jifen_pay', $money * $bfb);
					}
				}
				if($pparent['id'] && $pparent['grade_name']=="会员" && $this->isVipEnd($pparent['id'])){
					$this->makeMonth($pparent['id'],$year,$month);
					$bfb=$this->getPercent(array(2,2));
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_money',$money);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_pay',$money*$bfb);//
					//两个积分账户
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$money*$bfb/2);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$money*$bfb/2);
					$this->addMyMoney($pparent['id'],array('add_jf_currency'=>$money*$bfb));
					$this->addMyMoney($pparent['id'],array('add_jf_limit'=>$money*$bfb));
					if($pparent['id']!=10000000) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_jifen_pay', $money * $bfb);
					}
				}
				if($ppparent['id'] && $ppparent['grade_name']=="会员" && $this->isVipEnd($ppparent['id'])){
					$this->makeMonth($ppparent['id'],$year,$month);
					$bfb=$this->getPercent(array(2,3));
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_money',$money);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_pay',$money*$bfb);//
					//两个积分账户
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$money*$bfb/2);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$money*$bfb/2);
					$this->addMyMoney($ppparent['id'],array('add_jf_currency'=>$money*$bfb));
					$this->addMyMoney($ppparent['id'],array('add_jf_limit'=>$money*$bfb));
					if($ppparent['id']!=10000000) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_jifen_pay', $money * $bfb);
					}
				}
				//最接近的合伙人,向上数三级每个0.1
				$path=explode('-',$my['path']);
				array_pop($path);
				array_pop($path);
				$path=array_reverse($path,false);
				$least=array();
				foreach($path as $k=>$v){
					if($k<=3){
					if($v['grade_name']=="合伙人"){
						$least[]=$v;
					}
					}
				}
				//找到最接近的合伙人,增加0.5
				if($least[0]['id'] && $least[0]['grade_name']=="合伙人"){
					$this->makeMonth($least[0]['id'],$year,$month);
					$bfb=$this->getPercent(array(9,1));
					$person->where(array('member_id'=>$least[0]['id'],'year'=>$year,'month'=>$month))->setInc('hhr_hytd_buy',$money*$bfb);//
					$this->addMyMoney($least[0]['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($least[0]['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($least[0]['id'],array('Gross_income'=>$money*$bfb));
					if($parent['id']!=10000000) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
					}
				}
				if($least[1]&& $least[1]['grade_name']=="合伙人"){
					$this->makeMonth($least[1]['id'],$year,$month);
					$bfb=$this->getPercent(array(6,1));
					$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_money',$money);
					$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_pay',$money*$bfb);//
					$this->addMyMoney($least[1]['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($least[1]['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($least[1]['id'],array('Gross_income'=>$money*$bfb));
					if($least[1]['id']!=10000000) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
					}
				}
				if($least[2]&& $least[2]['grade_name']=="合伙人"){
					$this->makeMonth($least[2]['id'],$year,$month);
					$bfb=$this->getPercent(array(6,2));
					$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_money',$money);
					$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_money',$money*$bfb);
					$this->addMyMoney($least[2]['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($least[2]['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($least[2]['id'],array('Gross_income'=>$money*$bfb));
					if($least[2]['id']!=10000000) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
					}
				}
				if($least[3]&& $least[3]['grade_name']=="合伙人"){
					$this->makeMonth($least[3]['id'],$year,$month);
					$bfb=$this->getPercent(array(6,3));
					$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_money',$money);
					$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_money',$money*$bfb);
					$this->addMyMoney($least[3]['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($least[3]['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($least[3]['id'],array('Gross_income'=>$money*$bfb));
					if($least[3]['id']!=10000000) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
					}
				}
				//判断三级之内是否存在合伙人
				if($parent['grade_name']=="合伙人" || $pparent['grade_name']=="合伙人" || $ppparent['grade_name']=="合伙人"){
					if($parent['id'] && $parent['grade_name']=="合伙人"){
						$this->makeMonth($parent['id'],$year,$month);
						$bfb=$this->getPercent(array(12,1));
						$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_money',$money);
						$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_pay',$money*$bfb);//
						$this->addMyMoney($parent['id'],array('surplus_money'=>$money*$bfb));
						$this->addMyMoney($parent['id'],array('consume_profit'=>$money*$bfb));
						$this->addMyMoney($parent['id'],array('Gross_income'=>$money*$bfb));
						if($parent['id']!=10000000) {
							//公司支出部分
							$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
						}
					}
					if($pparent['id'] && $pparent['grade_name']=="合伙人" && $parent['grade_name']=="会员"){
						$this->makeMonth($pparent['id'],$year,$month);
						$bfb=$this->getPercent(array(12,2));
						$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_buy_money',$money);
						$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_buy_pay',$money*$bfb);//
						$this->addMyMoney($pparent['id'],array('surplus_money'=>$money*$bfb));
						$this->addMyMoney($pparent['id'],array('consume_profit'=>$money*$bfb));
						$this->addMyMoney($pparent['id'],array('Gross_income'=>$money*$bfb));
						if($pparent['id']!=10000000) {
							//公司支出部分
							$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
						}
					}
					if($ppparent['id'] && $ppparent['grade_name']=="合伙人" && $parent['grade_name']=="会员" && $pparent['grade_name']=="会员"){
						$this->makeMonth($ppparent['id'],$year,$month);
						$bfb=$this->getPercent(array(12,3));
						$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_buy_money',$money);
						$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_buy_pay',$money*$bfb);//
						$this->addMyMoney($ppparent['id'],array('surplus_money'=>$money*$bfb));
						$this->addMyMoney($ppparent['id'],array('consume_profit'=>$money*$bfb));
						$this->addMyMoney($ppparent['id'],array('Gross_income'=>$money*$bfb));
						if($ppparent['id']!=10000000) {
							//公司支出部分
							$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
						}
					}
				}
			}else{
				return false;//既不是会员,也不是合伙人的购物
			}
		}
	}

	/**
	 * @param $member_id   用户id
	 * @param array $field  需要增加的字段和增加值的字段 例如:array('jifenA'=>10,'jifenB'=>22)
	 */
	protected function addMyMoney($member_id,$field=array()){
		$member_profit=M('member_profit');
		if(!$member_profit->where(array('member_id'=>$member_id))->count()){
			$member_profit->add(array('member_id'=>$member_id));
		}
		foreach($field as $k=>$v){
			$member_profit->where(array('member_id'=>$member_id))->setInc($k,$v);
		}
	}
	/**
	 * 获取提成比例
	 * @param $a提成比例的坐标array(),或者说明文字
	 * @return mixed
	 */
	protected function getPercent($a){
		$model=M('member_proportion');
		//传入的是汉字说明
		if(is_string($a)){
			if($model->where(array('tcbl_nane'=>$a))->count()){
				return $model->where(array('tcbl_nane'=>$a))->find();
			}else{
				return $model->where(array('proportion_name'=>$a))->find();
			}
		}
		//传入坐标
		if(is_array($a)){
		if($a[1]==1){
			$level="first";
		}elseif($a[1]==2){
			$level="second";
		}else{
			$level="third";
		}
		return $model->where(array('id'=>$a[0]))->getField($level);
	}
	}
	protected function makeCompanyData($year,$month){
		$company=M('company_month');
		if(!$company->where(array('year'=>$year,'month'=>$month))->count()){
			$company->add(array('year'=>$year,'month'=>$month,'create_time'=>NOW_TIME));
		}
	}
	/**
	 * 旅游提成
	 */
	public function travel($id,$true_money,$money,$year,$month,$myjifen){
		$member=M('member');
		$person=M('person_month');
		$company=M('company_month');
		$my=$member->where(array('id'=>$id))->find();
		$pid=$my['pid'];
		$parent=$member->where(array('id'=>$pid))->find();
		$pparent=$member->where(array('id'=>$parent['pid']))->find();
		$ppparent=$member->where(array('id'=>$pparent['pid']))->find();
		$this->makeMonth($my['id'],$year,$month);
		//现增加公司旅游消费收入
		$this->makeCompanyData($year,$month);
		$company->where(array('year'=>$year,'month'=>$month))->setInc('tourism_add',$true_money);
		//保存自己的分月记录
		$this->makeMonth($my['id'],$year,$month);
		$person->where(array('member_id'=>$my['id'],'year'=>$year,'month'=>$month))->setInc('my_ly_jf',$myjifen);
		if($my['grade_name']=='合伙人'){
			//增加自身购物返利积分
			$this->addMyMoney($my['id'],array('add_jf_currency'=>$myjifen));
			if($parent['id']){
				$this->makeMonth($parent['id'],$year,$month);
				$bfb=$this->getPercent(array(7,1));
				$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_money',$money);
				$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_pay',$money*$bfb);
				if($parent['id']!=10000000){
					$company->where(array('year'=>$year,'month'=>$month))->setInc('tourism_jifen_pay',$money*$bfb);
				}
				$this->addMyMoney($parent['id'],array('tour_jifen_profit'=>$money*$bfb));
			}
			if($pparent['id']){
				$this->makeMonth($pparent['id'],$year,$month);
				$bfb=$this->getPercent(array(7,2));
				$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_money',$money);
				$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_pay',$money*$bfb);
				if($pparent['id']!=10000000) {
					$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
				}
				$this->addMyMoney($pparent['id'],array('tour_jifen_profit'=>$money*$bfb));
			}
			if($ppparent['id']){
				$this->makeMonth($ppparent['id'],$year,$month);
				$bfb=$this->getPercent(array(7,3));
				$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_money',$money);
				$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_pay',$money*$bfb);
				if($ppparent['id']!=10000000) {
					$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
				}
				$this->addMyMoney($ppparent['id'],array('tour_jifen_profit'=>$money*$bfb));
			}
		}elseif($my['grade_name']=='会员'){
			//增加自身购物返利积分
			$this->addMyMoney($my['id'],array('add_jf_currency'=>$myjifen/2));
			$this->addMyMoney($my['id'],array('add_jf_limit'=>$myjifen/2));
			//上级三级之内的会员增加收益
			if($parent['id'] && $parent['grade_name']=="会员" && $this->isVipEnd($parent['id'])){
				$this->makeMonth($parent['id'],$year,$month);
				$bfb=$this->getPercent(array(3,1));
				$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_money',$money);
				$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_pay',$money*$bfb);//
				//两个积分账户
				$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$money*$bfb/2);
				$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$money*$bfb/2);
				$this->addMyMoney($parent['id'],array('tour_jifen_profit'=>$money*$bfb));
				if($parent['id']!=10000000) {
					//公司支出部分
					$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
				}
			}
			if($pparent['id'] && $pparent['grade_name']=="会员" && $this->isVipEnd($pparent['id'])){
				$this->makeMonth($pparent['id'],$year,$month);
				$bfb=$this->getPercent(array(3,2));
				$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_money',$money);
				$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_pay',$money*$bfb);//
				//两个积分账户
				$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$money*$bfb/2);
				$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$money*$bfb/2);
				$this->addMyMoney($pparent['id'],array('tour_jifen_profit'=>$money*$bfb));
				if($pparent['id']!=10000000) {
					//公司支出部分
					$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
				}
			}
			if($ppparent['id'] && $ppparent['grade_name']=="会员" && $this->isVipEnd($ppparent['id'])){
				$this->makeMonth($ppparent['id'],$year,$month);
				$bfb=$this->getPercent(array(3,3));
				$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_money',$money);
				$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_pay',$money*$bfb);//
				//两个积分账户
				$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$money*$bfb/2);
				$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$money*$bfb/2);
				$this->addMyMoney($ppparent['id'],array('tour_jifen_profit'=>$money*$bfb));
				if($ppparent['id']!=10000000) {
					//公司支出部分
					$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
				}
			}
			//最接近的合伙人,向上数三级每个0.1
			$path=explode('-',$my['path']);
			array_pop($path);
			array_pop($path);
			array_reverse($path,false);
			$least=array();
			foreach($path as $k=>$v){
				if($k<=3){
					if($v['grade_name']=="合伙人"){
						$least[]=$v;
					}
				}
			}
			//找到最接近的合伙人,增加0.5
			if($least[0]['id'] && $least[0]['grade_name']=="合伙人"){
				$this->makeMonth($least[0]['id'],$year,$month);
				$bfb=$this->getPercent(array(10,1));
				$person->where(array('member_id'=>$least[0]['id'],'year'=>$year,'month'=>$month))->setInc('hhr_hytd_tra',$money*$bfb);//
				$this->addMyMoney($least[0]['id'],array('tour_jifen_profit'=>$money*$bfb));
				if($parent['id']!=10000000) {
					//公司支出部分
					$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
				}
			}
			if($least[1]&& $least[1]['grade_name']=="合伙人"){
				$this->makeMonth($least[1]['id'],$year,$month);
				$bfb=$this->getPercent(array(7,1));
				$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_hy_tra_money',$money);
				$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_hy_tra_pay',$money*$bfb);//
				$this->addMyMoney($least[1]['id'],array('tour_jifen_profit'=>$money*$bfb));
				if($least[1]['id']!=10000000) {
					$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
				}
			}
			if($least[2]&& $least[2]['grade_name']=="合伙人"){
				$this->makeMonth($least[2]['id'],$year,$month);
				$bfb=$this->getPercent(array(7,2));
				$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_tra_money',$money);
				$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_tra_pay',$money*$bfb);
				$this->addMyMoney($least[2]['id'],array('tour_jifen_profit'=>$money*$bfb));
				if($least[2]['id']!=10000000) {
					$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
				}
			}
			if($least[3]&& $least[3]['grade_name']=="合伙人"){
				$this->makeMonth($least[3]['id'],$year,$month);
				$bfb=$this->getPercent(array(7,3));
				$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_tra_money',$money);
				$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_tra_pay',$money*$bfb);
				$this->addMyMoney($least[3]['id'],array('tour_jifen_profit'=>$money*$bfb));
				if($least[3]['id']!=10000000) {
					$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
				}
			}
			//判断三级之内是否存在合伙人
			if($parent['grade_name']=="合伙人" || $pparent['grade_name']=="合伙人" || $ppparent['grade_name']=="合伙人"){
				if($parent['id'] && $parent['grade_name']=="合伙人"){
					$this->makeMonth($parent['id'],$year,$month);
					$bfb=$this->getPercent(array(13,1));
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_money',$money);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_pay',$money*$bfb);//
					$this->addMyMoney($parent['id'],array('tour_jifen_profit'=>$money*$bfb));
					if($parent['id']!=10000000) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
					}
				}
				if($pparent['id'] && $pparent['grade_name']=="合伙人" && $parent['grade_name']=="会员"){
					$this->makeMonth($pparent['id'],$year,$month);
					$bfb=$this->getPercent(array(13,2));
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_money',$money);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_pay',$money*$bfb);//
					$this->addMyMoney($pparent['id'],array('tour_jifen_profit'=>$money*$bfb));
					if($pparent['id']!=10000000) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
					}
				}
				if($ppparent['id'] && $ppparent['grade_name']=="合伙人" && $parent['grade_name']=="会员" && $pparent['grade_name']=="会员"){
					$this->makeMonth($ppparent['id'],$year,$month);
					$bfb=$this->getPercent(array(13,3));
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_money',$money);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_pay',$money*$bfb);//
					$this->addMyMoney($ppparent['id'],array('tour_jifen_profit'=>$money*$bfb));
					if($ppparent['id']!=10000000) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $money * $bfb);
					}
				}
			}
		}else{
			//游客不能旅游
		}


	}

	/**
	 * @param $member_id  用户id
	 * @return int  1未到期,0到期
	 * 如果是合伙人则不需要判断
	 */
	protected function isVipEnd($member_id){
		$endtime=M('member')->where(array('id'=>$member_id))->getField('vip_end');
		if($endtime-NOW_TIME>0){
			return 1;
		}else{
			return 0;
		}
	}
	/**
	 * @param int $id
	 * @param int $year
	 * @param int $month
	 * 判断用户月份记录是否存在,如果没有就新增一条
	 */
	protected function makeMonth($id,$year,$month) {
		if ($id) {
			$m = M('person_month');
			$arr=array('member_id' => $id, 'year' => $year, 'month' => $month,'create_time'=>NOW_TIME);
			for($i=1;$i<=8;++$i){
				for($k=1;$k<=3;++$k){
					$arr['per_'.$i.'_'.$k]=$this->getPercent(array($i,$k));
				}
			}
			$arr['per_9']=$this->getPercent(array(9,1));
			$arr['per_10']=$this->getPercent(array(10,1));
			$arr['per_11']=$this->getPercent(array(11,1));
			for($i=12;$i<=13;++$i){
				for($k=1;$k<=3;++$k){
					$arr['per_'.$i.'_'.$k]=$this->getPercent(array($i,$k));
				}
			}
			if (!$m -> where(array('member_id' => $id, 'year' => $year, 'month' => $month)) -> count()) {
				$m -> add($arr);
			}
		}
	}
}




