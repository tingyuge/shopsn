<?php
namespace Home\Controller;
use Think\Controller;
use Common\Tool\Tool;
use Home\Model\UserHeaderModel;
class FileUploadController extends Controller
{
    public function receiveFile()
    {
        Tool::checkPost($_FILES)? true : $this->ajaxReturnData(null, '400', '参数错误');
        Tool::checkPost($_POST, array('is_numeric' => array('user_id'),'user_header'), true, array('user_id'))? true : $this->ajaxReturnData(null, '400', '参数错误');
        Tool::connect('File');
        $_FILES = Tool::parseFile($_FILES);
        Tool::connect('Mosaic');
        $fileObj = UserHeaderModel::getInitation();
        $file    = $fileObj->UploadFile(C('USER_UPLOAD'));
        $insert  = $fileObj->updateOrAdd($file, $_POST);
        $status = empty($file) ? 0 : 1;
        $mssage = empty($file) ? '失败':'成功';
        $this->ajaxReturnData($file, $status, $mssage);
    }
    
    /**
     * ajax 返回数据
     */
    protected function ajaxReturnData($data, $status= 1, $message = '操作成功')
    {
        $this->ajaxReturn(array(
            'status'  => $status,
            'message' => $message,
            'data'    => $data
        ));
        die();
    }
}