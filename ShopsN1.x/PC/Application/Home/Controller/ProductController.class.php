<?php
namespace Home\Controller;

use Common\Tool\Tool;
class ProductController extends BaseController
{
    public function index()
    {
        //检测传值
        Tool::checkPost($_GET, array('is_numeric'=> array('sid')), true, array('sid')) === false ? $this->error('灌水机制已打开', U('Index/index')) : true;
        
        $this->display();
    }
}