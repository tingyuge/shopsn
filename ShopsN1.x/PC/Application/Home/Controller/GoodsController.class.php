<?php
namespace Home\Controller;

use Common\Tool\Tool;
use Home\Model\FootPrintModel;
use Home\Model\GoodsClassModel;
use Home\Model\GoodsModel;
/**
 * 商品控制器 
 */
class GoodsController extends BaseController
{
    private static $model;
   
    //商品列表
    public function goods(){
        
        //检测传值
        Tool::checkPost($_GET, array('is_numeric' => array('class_id'),'class_sub_id', 'price'), true) === false ? $this->error('灌水机制已打开') : true;
       
        //大分类及其当前分类的相同父级
        $data = GoodsClassModel::getInition()->classTop(array(
            'where' => array('fid' => 0, 'type', 'hide_status' => 0, 'type' => 1),
            'field' => array('id', 'class_name')
        ), $_GET['class_id']);
        
        $result = GoodsModel::getInitation()->screenData($_GET);
        if($_GET['class_id']){
            $this->assign('class_id',$_GET['class_id']);
        }
        $this->assign('result',$result);
        $this->assign('post',$_POST);
        
        $this->classData = $data;
        $this->display();
    }
    
    //商品详情
    public function goods_details()
    {
        //检测传值
        Tool::checkPost($_GET, array('is_numeric' => array('id')), true) === false ? $this->error('灌水机制已打开') : true;
        
        if (!isset(self::$model['childrenModel'] )) {
            self::$model['childrenModel'] = new \Home\Model\GoodsModel();
        }
        
        $result = self::$model['childrenModel']->find(array(
            'where' => array('id'=> $_GET['id']),
        ), $_GET, new \THink\Model('goods_orders_record'));
        
        
        if (!empty($_SESSION['user_id']) && !empty($result['goods']))
        {
            //添加我的足迹
            FootPrintModel::getInitation()->add(array(
                'uid'         => $_SESSION['user_id'],
                'gid'         => $_GET['id'],
                'goods_pic'   => $result['goods']['pic_url'],
                'goods_price' => $result['goods']['price_new'],
                'goods_name'  => $result['goods']['title'],
                'is_type'     => 1
            ));
        }
        
        //生成表单令牌
        
        
        $_SESSION['form'] = base64_encode(md5(C('form').$_GET['id']));
    
        $member = M('member','vip_');
        $where_m['id'] = $_SESSION['user_id'];
        $res_member = $member->where($where_m)->find();
        $this->assign('res_member',$res_member);
    
        //删除7天内的浏览记录
        $m = M('foot_print');
        $duibi_time = time() - (86400 * 7);
        $map['uid'] = $_SESSION['user_id'];
        $map['create_time'] = array('lt',$duibi_time);
        $m->where($map)->delete();
        $this->form = $_SESSION['form'];
        $this->result = $result;
        $this->display();
    }
    
    //限购商品列表
    public function xiangou()
    {
       
        
        //设置一下默认值防止报错
        Tool::isSetDefaultValue($_GET, array('class_sub_id' => 0, 'class_id' => 222, 'price' => 0) );
        //大分类及其当前分类的相同父级
        $data = GoodsClassModel::getInition()->classTop(array(
            'where' => array('fid' => 0, 'type', 'hide_status' => 0, 'type' => 1),
            'field' => array('id', 'class_name')
        ), $_GET['class_id']);
        
         $result = GoodsModel::getInitation()->screenData($_GET, 1);
       
         
        if($_GET['class_id']){
            $this->assign('class_id',$_GET['class_id']);
        }
        $this->assign('result',$result);
        $this->assign('post',$_POST);
        
        $this->classData = $data;
         
       
        $this->display('goods');
    }
    
}