<?php
namespace Common\Tool\Extend;

class ArrayChildren extends ArrayParse
{
    /**
     * 根据 标识 删除数组数据 
     */
    public function deleteByCondition(array $array, $condition = '_')
    {
        if (empty($array) || !is_array($array)) {
            return array();
        }
        
        foreach ($array as $key => $value)
        {
            if (false === strpos($key, $condition)) {
                continue;
            }
            
            unset($array[$key]);
        }
        return $array;
    }
    /**
     * 状态 改为键  value改为汉字提示； 
     * @param array $array 要处理的数组
     * @param array $prompt 提示的数组
     * @return array
     */
    public function changeKeyValueToPrompt(array $array , array $prompt)
    {
        $param = func_get_args();
        //检测类型
        foreach ($param as $value) {
            if (empty($value) || !is_array($value)) {
                return array();
            }
        }
        $flag = array();
        foreach ($array as $key => $value)
        {
            if (!array_key_exists($key, $prompt))
            {
                continue;
            }
            $flag[$value] = $prompt[$key];
        }
        unset($array, $prompt);
        return $flag;
    }
    
    /**
     * 组装 筛选控件
     * @param array $data post数据
     * @return array
     */
    public function buildActive(array  $data)
    {
        if (empty($data) || !is_array($data))
        {
            return array();
        }
        foreach ($data as $key => $value)
        {
            if (empty($value)) {
                unset($data[$key]);
            } 
        }
        return $data;
    }
    
    /**
     * 从数组中去除字段
     * @return array
     */
    public function getSplitUnset(array $array, $split='_d')
    {
        if (empty($array))
        {
            return array();
        }
    
        foreach ($array as $key => & $value)
        {
            if (false === strpos($key, $split))
            {
                unset($array[$key]);
            }
        }
    
        return $array;
    }
    
}