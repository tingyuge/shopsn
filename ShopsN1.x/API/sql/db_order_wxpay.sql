/*
Navicat MySQL Data Transfer

Source Server         : yisu
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : yisushop

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-12-22 13:46:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_order_wxpay`
-- ----------------------------
DROP TABLE IF EXISTS `db_order_wxpay`;
CREATE TABLE `db_order_wxpay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `wx_pay_id` varchar(50) DEFAULT NULL COMMENT '支付码',
  `status` tinyint(1) DEFAULT '0' COMMENT '0支付失败， 1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_order_wxpay
-- ----------------------------
