<?php
return array(
    //'配置项'=>'配置值'
    /* 数据库设置 */
    'IMG_ROOT_PATH'      => '/Uploads/goods/',
    'title'              => '亿速网络',
    'URL_MODEL'          => 2,
    'appId'              => '2016092801992020',
    'rsa'                => './Application/Common/AlipayMobile/rsa_private_key.pem',
    'DEFAULT_MODULE'     => 'API',
    /* 商品图片上传相关配置 */
    'GOODS_UPLOAD' => array(
        'mimes'    => '', //允许上传的文件MiMe类型
        'maxSize'  => 2*1024*1024, //上传的文件大小限制 (0-不做限制)
        'exts'     => 'jpg,gif,png,jpeg', //允许上传的文件后缀
        'autoSub'  => true, //自动子目录保存文件
        'subName'  => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
        'rootPath' => './Uploads/user/', //保存根路径
        'savePath' => '', //保存路径
        'saveName' => array('uniqid', ''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
        'saveExt'  => '', //文件保存后缀，空则使用原后缀
        'replace'  => false, //存在同名是否覆盖
        'hash'     => true, //是否生成hash编码
        'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
    ), //图片上传相关配置（文件上传类配置）
    'USER_HEADER'   => '/Uploads/user/',
    'DRIVER_CONFIG' => array( 
        'host'     => '', //服务器
        'port'     => 21, //端口
        'timeout'  => 90, //超时时间
        'username' => '', //用户名
        'password' => '', //密码
    ),
    'IMAGE_UPLOAD_SERVER' => 'http://fxcs.03.idchome.net/index.php/Home/FileUpload/receiveFile'
);