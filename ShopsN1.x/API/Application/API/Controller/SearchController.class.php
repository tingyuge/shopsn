<?php
namespace API\Controller;
use Common\Tool\Tool;
use API\Model\GoodsModel;

/**
 * 搜索
 */
class SearchController extends BaseController
{
    public function index()
    {   
        Tool::checkPost($_GET, (array)null, false , array('title')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        $data = GoodsModel::getInitation()->screenData($_GET);
        
        $this->ajaxReturnData($data);
    }
}