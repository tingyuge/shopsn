<?php
namespace API\Controller;
use API\Model\GoodsClassModel;
use API\Model\GoodsModel;
use Common\Tool\Tool;
use API\Model\FootPrintModel;

/**
 * 商品 控制器 
 */
class GoodsController extends BaseController
{
    //一级分类
    public function index()
    {
        $classData = GoodsClassModel::getInition()->getChildrens(array(
            'field' => 'id,class_name',
            'where' => array('fid' => 0, 'type' => 1, 'hide_status' => 0)
        ));
        
       $this->ajaxReturnData($classData);
    }
    
    //一级分类下所有商品
    
    public function classGoods()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('id')), true , array('id')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        GoodsClassModel::getInition()->where(array(
            'id' => $_POST['id']
        ))->getField('hide_status') != 0 ? $this->ajaxReturnData(array(), 400, '暂无数据') : true;
        
        $offset = ($_POST['page'] - 1) * $_POST['page_size'];
        
        $data = GoodsModel::getInitation()->getGoodsForClassId(array(
            'field' => 'id,title,pic_url,price_old,price_new,fanli_jifen,shoper_id',
            'where' => array('class_id' => $_POST['id'], 'type' => 1),
            'order' => 'sort_num DESC',
            'limit' => $offset.' , '.$_POST['page_size']
        ));
        $this->ajaxReturnData($data);
    }
    
    //二级分类
    public function twoClassData()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('id')), true , array('id')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        $data = GoodsClassModel::getInition()->getChildrens(array(
            'field' => 'id,class_name',
            'where' => array('fid' => $_POST['id'], 'type' =>1, 'hide_status' => 0)
        ));
        
        $this->updateClient($data, '操作');
    }
    
    //商品列表   
    public function goodsList()
    {
        $this->classGoods();      
    }
    
    //商品详情
    public function goodsDetail()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('id')), true , array('id')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        $isHave = GoodsModel::getInitation()->isHaveGoods($_POST['id']);
        
        $this->prompt($isHave, null, '没有该商品');
        
        $goods = GoodsModel::getInitation()->find(array(
            'field' => ' id,title,pic_url,pic_tuji,kucun,fanli_jifen,taocan,price_old,price_new,is_baoyou,shoper_id,shangjia,xianshi_status',
            'where' => array('id' => $_POST['id'], 'shangjia' => 1)
        ), new \THink\Model('goods_orders_record'));
       
        if (!empty($goods) && !empty($_SESSION['userId']))
        {
            //添加我的足迹
            FootPrintModel::getInitation()->add($goods);
        }
        $this->updateClient(array($goods), '操作');
    }
}