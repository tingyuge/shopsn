<?php
namespace API\Model;

use Think\Model;
use Think\Think;
use Common\Tool\Tool;

/**
 * 订单评论模型 
 */
class OrderCommentModel extends Model
{
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    /**
     * @see Think\Model\select 
     */
    public function select( array $options, Model $model)
    {
        if (empty($options) || !($model instanceof Model))
        {
            return array();
        }
    
        $data = parent::select($options);
        if (!empty($data))
        {
            $userId = null;
            foreach ($data as $key => $value)
            {
                if (!empty($value['user_id']))
                {
                    $userId .= ','.'"'.$value['user_id'].'"';
                }
            }
            $userId = substr($userId, 1);
            $userData = $model->field('username,id as user_id')->where('id in ('.$userId.')')->select();
            
            $data = Tool::lineArrayData($data, $userData, 'username', 'content');
            //排序
            usort( $data, function($goodsA, $goodsB){
                return  $goodsA['goods_id'] - $goodsB['goods_id'] < 0;
            });
        }
        return $data;
    }
    /**
     * 是否评论过该商品 
     */
    public function isCommentGoodsByUser($orderId)
    {
        if (!is_numeric($orderId))
        {
            return array();
        }
        return $this->where('goods_orders_id = "%s" and user_id = "%s"', array($orderId, $_SESSION['userId']))->getField($this->getPk());
    }
}