//
//  ZCommonGoodPointsCell.m
//  shopSN
//
//  Created by yisu on 16/9/13.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodPointsCell.h"

@implementation ZCommonGoodPointsCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        //点击cell不变色
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        [self initSubViews];
    }
    return self;
}




- (void)initSubViews {
    
    //bgView
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 44+10)];
    [self addSubview:bgView];
//    bgView.backgroundColor = [UIColor orangeColor];
    
    
    //pointLb
    _pointLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, 150, 15)];
    [bgView addSubview:_pointLb];
    //_pointLb.backgroundColor = __TestGColor;
    _pointLb.font = MFont(15);
    _pointLb.textColor = __MoneyColor;
    _pointLb.text = @"送积分（22积分）";
    
    
    //stockLb
    _stockLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_pointLb), 15, CGRectW(bgView)-160-10, 15)];
    
    [bgView addSubview:_stockLb];
    //_stockLb.backgroundColor = __TestOColor;
    _stockLb.font = MFont(15);
    _stockLb.textColor = HEXCOLOR(0x333333);
    _stockLb.textAlignment = NSTextAlignmentCenter;
    _stockLb.text = @"库存：40 件";
    
    
   
    
    
    
    
    //lineIV
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-10, CGRectW(bgView), 10)];
    [bgView addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xf0f0f0);
    
    
}


#pragma mark - reloadData
- (void)setGoodsIntrPoints:(ZGoodsDeal *)deal {
    
    //pointLb
    self.pointLb.text = [NSString stringWithFormat:@"送积分（%@积分）",deal.goodsPoints];
    
    //stockLb
    self.stockLb.text = [NSString stringWithFormat:@"库存：%@ 件",deal.goodsStocks];
    
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
