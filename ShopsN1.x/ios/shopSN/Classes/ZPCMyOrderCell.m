//
//  ZPCMyOrderCell.m
//  shopSN
//
//  Created by chang on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPCMyOrderCell.h"
#import "ZPCMyOrderSubView.h"

@interface ZPCMyOrderCell ()
{
    UIView *_titleView;
}

@property (nonatomic, strong)ZPCMyOrderSubView *orderSubView;

/** 订单行 按钮*/
@property (nonatomic, strong) UIButton *orderButton;

@end

@implementation ZPCMyOrderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = HEXCOLOR(0xdedede);
        
        [self initSubViews];
    }
    
    return self;
}



- (void)initSubViews {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 70)];
    [self addSubview:bgView];
    bgView.backgroundColor = HEXCOLOR(0xffffff);
    
    
    //标题 代付款 待收货 待评价 退货/换货  我的订单 >
    //图片 dfk.png dsh.png dpj.png th.png dd.png
    NSArray *titleLbArray = @[@"代付款", @"待收货", @"已完成", @"消息", @"我的订单>"];
    NSArray *titleIVArray = @[@"dfk.png", @"dsh.png", @"dpj.png", @"dpj.png", @"dd.png"];
    
    for (int i=0; i<5; i++) {
        
        //分配view
        //从左侧开始，每间隔5放一个view，最右侧第5个view右侧没有间隙
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(i*(CGRectW(bgView)-25)/5+5*(i+1), 0, (CGRectW(bgView)-25)/5, CGRectH(bgView))];
        [bgView addSubview:_titleView];
//        _titleView.backgroundColor = __TestGColor;
//        NSLog(@"w:%lf===h:%lf", _titleView.frame.size.width, _titleView.frame.size.height);
        
        
        //添加子视图
        _orderSubView = [[ZPCMyOrderSubView alloc] initWithFrame:CGRectMake((CGRectW(_titleView)-55)/2, 10, 55, 50)];
        //_orderSubView.center = _titleView.center;
        [_titleView addSubview:_orderSubView];
         //1 给图片和文本 赋值
        _orderSubView.titleIV.image = MImage(titleIVArray[i]);
        _orderSubView.titleLb.text = titleLbArray[i];
        
        //添加同尺寸按钮
        _orderButton = [[UIButton alloc] initWithFrame:_orderSubView.frame];
        [_titleView addSubview:_orderButton];
        _orderButton.tag = 2010+i;
        [_orderButton addTarget:self action:@selector(orderButtonAction:) forControlEvents:BtnTouchUpInside];
        
        
        
        if (i == 4) {//2 添加竖线
            UIImageView *leftLineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1, CGRectH(_titleView))];
            [_titleView addSubview:leftLineIV];
            leftLineIV.backgroundColor = HEXCOLOR(0xdedede);
        }
        
        
    }
    
    
    
}

- (void)orderButtonAction:(UIButton *)btn {
    [self.delegate didOrderButton:btn];
    

}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
