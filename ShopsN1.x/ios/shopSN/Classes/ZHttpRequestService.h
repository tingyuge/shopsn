//
//  ZHttpRequestService.h
//  shopSN
//
//  Created by yisu on 16/9/6.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

typedef void(^SuccessCallBack)(id responseObject,BOOL succe,NSDictionary *jsonDic);
typedef  void (^FailureCallBack)(NSError *error);

@interface ZHttpRequestService : NSObject

//单纯的获取数据
+(void)GET:(NSString *)path success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;


/** 提供 aip.php 请求方法*/
+ (void)POST:(NSString*)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;
/**上传图片*/
+ (void)POST:(NSString*)path Params:(NSDictionary *)params NSData:(NSData *)imageData key:(NSString *)name success:(SuccessCallBack)suuccess failure:(FailureCallBack)fail animated:(BOOL)animated;


/** 提供 goods.php 请求方法*/
+ (void)POSTGoods:(NSString*)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;


/** 提供 tour.php 请求方法*/
+ (void)POSTTour:(NSString*)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;


/** 提供 home.php 请求方法*/
+ (void)POSTHome:(NSString*)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;

/** 提供 GET home.php 请求方法 */
+ (void)GETHome:(NSString *)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;


/** 提供GET goods.php 请求方法*/
+ (void)GETGoods:(NSString*)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;


/** 提供GET tour.php 请求方法*/
+ (void)GETTour:(NSString*)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;

/** 提供GET aip.php 请求方法*/
+ (void)GETApi:(NSString*)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;

@end
