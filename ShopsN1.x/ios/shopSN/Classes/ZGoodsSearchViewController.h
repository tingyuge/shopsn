//
//  ZGoodsSearchViewController.h
//  shopSN
//
//  Created by yisu on 16/6/29.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品搜索页面
 *
 *  主控制器
 *
 */
#import "BaseViewController.h"

@interface ZGoodsSearchViewController : BaseViewController

/** 商品搜索页面 搜索文字 */
@property (copy, nonatomic) NSString *searchStr;

/** 商品搜索页面 是否是商品判断 */
@property (assign, nonatomic) BOOL isGoods;

@end
