//
//  ZCommonGoodsCountCell.h
//  shopSN
//
//  Created by yisu on 16/10/10.
//  Copyright © 2016年 yisu. All rights reserved.
//

/* 提供 普通商品 简介页面
 *
 *   商品数量 cell
 *
 */




#import "BaseTableViewCell.h"

@class ZCommonGoodsCountCell;
@protocol ZCommonGoodsCountCellDelegate <NSObject>

/** 普通商品 数量增减 */
- (void)changeGoodsCounts:(NSInteger)counts;

@end

@interface ZCommonGoodsCountCell : BaseTableViewCell

/** 普通商品 数量label */
@property (nonatomic, strong) UILabel *goodsCountsLb;

/** 普通商品 数量增减 代理方法 */
@property (weak, nonatomic) id<ZCommonGoodsCountCellDelegate>delegate;

@end
