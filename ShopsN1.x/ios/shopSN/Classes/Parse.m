//
//  Parse.m
//  shopSN
//
//  Created by yisu on 16/9/6.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "Parse.h"

@implementation Parse


+ (NSMutableArray *)parseCategory:(NSArray *)array {
    NSMutableArray *categorys =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZCategory *category = [[ZCategory alloc] init];
        category.categoryID = [Utils getTextStrByText:dic[@"id"]];
        category.categoryName = [Utils getTextStrByText:dic[@"class_name"]];
        [categorys addObject:category];
        
    }
    return categorys;
    
}

+ (NSMutableArray *)parseSubCategory:(NSArray *)array {
    NSMutableArray *subCategorys =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZSubCategory *sub = [[ZSubCategory alloc] init];
        sub.subCategoryID = [Utils getTextStrByText:dic[@"id"]];
        
        sub.subCategoryName = [Utils getTextStrByText:dic[@"class_name"]];
        
        [subCategorys addObject:sub];
        
    }
    return subCategorys;
    
}


+ (NSMutableArray *)parseTravelCategory:(NSArray *)array {
    NSMutableArray *categorys =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZCategory *category = [[ZCategory alloc] init];
        category.categoryID = [Utils getTextStrByText:dic[@"id"]];
        category.categoryName = [Utils getTextStrByText:dic[@"class_name"]];
        [categorys addObject:category];
        
    }
    return categorys;

}






+ (NSMutableArray *)parseTravelSubCategory:(NSArray *)array {
    NSMutableArray *subCategorys =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZSubCategory *sub = [[ZSubCategory alloc] init];
        sub.subCategoryID = [Utils getTextStrByText:dic[@"id"]];
        
        sub.subCategoryName = [Utils getTextStrByText:dic[@"class_name"]];
        
        [subCategorys addObject:sub];
        
    }
    return subCategorys;
}


+ (NSMutableArray *)parseGoodsList:(NSArray *)array {
    
    NSMutableArray *goodsLists =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZGoodsModel *goods = [[ZGoodsModel alloc] init];
        goods.goodsID = [Utils getTextStrByText:dic[@"id"]];
        goods.goodsTitle = [Utils getTextStrByText:dic[@"title"]];
        goods.goodsPicUrl = [Utils getTextStrByText:dic[@"pic_url"]];
        goods.goodsPoints = [Utils getTextStrByText:dic[@"fanli_jifen"]];
        goods.goodsNowPrice = [Utils getTextStrByText:dic[@"price_new"]];
        goods.goodsOriginalPrice = [Utils getTextStrByText:dic[@"price_old"]];
        
        
        [goodsLists addObject:goods];
        
    }
    return goodsLists;
}



+ (NSMutableArray *)parseGoodsDetail:(NSArray *)array {
    
    NSMutableArray *goodsDeals =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZGoodsDeal *deal = [[ZGoodsDeal alloc] init];
        deal.goodsID = [Utils getTextStrByText:dic[@"id"]];
        deal.goodsTitle = [Utils getTextStrByText:dic[@"title"]];
        deal.goodsPicUrl = [Utils getTextStrByText:dic[@"pic_url"]];
        deal.goodsPoints = [Utils getTextStrByText:dic[@"fanli_jifen"]];
        deal.goodsStocks = [Utils getTextStrByText:dic[@"kucun"]];//库存
        deal.goodsNowPrice = [Utils getTextStrByText:dic[@"price_new"]];
        deal.goodsOriginalPrice = [Utils getTextStrByText:dic[@"price_old"]];
        deal.goodsShipping = [Utils getTextStrByText:dic[@"is_baoyou"]];
        deal.goodsPackage = [Utils getTextStrByText:dic[@"taocan"]];//数据库有该内容，接口文件中没有
        deal.goodsShoperID = [Utils getTextStrByText:dic[@"shoper_id"]];//供应商id
        deal.goodsGoodsID = [Utils getTextStrByText:dic[@"goods_id"]];//商品goods_id
        
        //评价信息
        deal.goodsJudgeName = [Utils getTextStrByText:dic[@"username"]];
        deal.goodsJudgeGoodsID = [Utils getTextStrByText:dic[@"goods_id"]];
        deal.goodsJudgeTime = [Utils getTextStrByText:dic[@"pingjia_time"]];
        deal.goodsJudgeContent = [Utils getTextStrByText:dic[@"pingjia_content"]];
        
        //deal.goodsJudgePicUrl = [Utils getTextStrByText:dic[@"goods_image"]];//数据库暂时没有该内容
        
        //相关图集
        deal.goodsIntrPicArray = dic[@"pic_tuji"];  //简介图集
        deal.goodsDetailPicArray = dic[@"shuoming"];//详情图集
        
        
        [goodsDeals addObject:deal];
        
    }
    return goodsDeals;
}





+ (NSMutableArray *)parseHomeGoodsBannerData:(NSArray *)array {
    NSMutableArray *homeArray =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZHomePicModel *homePic = [[ZHomePicModel alloc] init];
        
        homePic.bannerUrl = dic[@"banner_url"];
        homePic.linkUrl   = dic[@"ad_link"];
        
        [homeArray addObject:homePic];
    }
    
    return homeArray;
}


+ (NSMutableArray *)parseHomeGoodsCenterData:(NSArray *)array {
    NSMutableArray *homeArray =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZHomePicModel *homePic = [[ZHomePicModel alloc] init];
        
        homePic.centerUrl = dic[@"center_url"];
        homePic.linkUrl   = dic[@"ad_link"];
        
        [homeArray addObject:homePic];
    }
    
    return homeArray;
}




+ (NSMutableArray *)parseHomeGoodsHotData:(NSArray *)array {
    NSMutableArray *homeArray =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZHomePicModel *homePic = [[ZHomePicModel alloc] init];
        
        homePic.hotUrl = dic[@"hot_url"];
        homePic.linkUrl   = dic[@"ad_link"];
        
        [homeArray addObject:homePic];
    }
    
    return homeArray;
}





+ (NSMutableArray *)parseHomeGoodsClassData:(NSArray *)array {
    NSMutableArray *homeArray =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZHomePicModel *homePic = [[ZHomePicModel alloc] init];
        
        homePic.classUrl  = dic[@"class_url"];
        homePic.className = dic[@"class_name"];
        homePic.classID   = dic[@"class_id"];
        
        [homeArray addObject:homePic];
    }
    
    return homeArray;
}






+ (NSMutableArray *)parseHomeTravelBannerData:(NSArray *)array {
    NSMutableArray *homeArray =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        //ZHomeTravelPicModel *homePic = [[ZHomeTravelPicModel alloc] init];
        ZHomePicModel *homePic = [[ZHomePicModel alloc] init];
        homePic.bannerUrl = dic[@"bannner_url"];
        homePic.linkUrl   = dic[@"ad_link"];
        
        [homeArray addObject:homePic];
    }
    
    return homeArray;
}


+ (NSMutableArray *)parseHomeTravelClassPageData:(NSArray *)array {
    NSMutableArray *homeArray =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        //ZHomeTravelPicModel *homePic = [[ZHomeTravelPicModel alloc] init];
        ZHomePicModel *homePic = [[ZHomePicModel alloc] init];
        homePic.classUrl = dic[@"class_page_url"];
        homePic.classID   = dic[@"class_id"];
        homePic.className   = dic[@"class_name"];
        [homeArray addObject:homePic];
    }
    
    return homeArray;
}

+ (NSMutableArray *)parseHomeTravelHotData:(NSArray *)array {
    NSMutableArray *homeArray =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        //ZHomeTravelPicModel *homePic = [[ZHomeTravelPicModel alloc] init];
        ZHomePicModel *homePic = [[ZHomePicModel alloc] init];
        homePic.hotUrl = dic[@"sige_url"];
        homePic.hotID   = dic[@"ad_link"];
        
        [homeArray addObject:homePic];
    }
    
    return homeArray;
}

+ (NSMutableArray *)parseHomeTravelCenterData:(NSArray *)array {
    NSMutableArray *homeArray =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        //ZHomeTravelPicModel *homePic = [[ZHomeTravelPicModel alloc] init];
        ZHomePicModel *homePic = [[ZHomePicModel alloc] init];
        homePic.centerUrl = dic[@"footer_url"];
        homePic.linkUrl   = dic[@"ad_link"];
        
        [homeArray addObject:homePic];
    }
    
    return homeArray;
}


+ (NSMutableArray *)parseHomeTravelClassData:(NSArray *)array {
    NSMutableArray *homeArray =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        //ZHomeTravelPicModel *homePic = [[ZHomeTravelPicModel alloc] init];
        ZHomePicModel *homePic = [[ZHomePicModel alloc] init];
        homePic.classUrl = dic[@"class_url"];
        homePic.classID   = dic[@"class_id"];
        homePic.className   = dic[@"class_name"];
        
        [homeArray addObject:homePic];
    }
    
    return homeArray;
}


//会员商城 搜索
+ (NSMutableArray *)parseGoodsSearchData:(NSArray *)array {
    NSMutableArray *goodsLists =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZGoodsModel *goods = [[ZGoodsModel alloc] init];
        goods.goodsID = [Utils getTextStrByText:dic[@"id"]];
        goods.goodsTitle = [Utils getTextStrByText:dic[@"title"]];
        goods.goodsPicUrl = [Utils getTextStrByText:dic[@"pic_url"]];
        goods.goodsNowPrice = [Utils getTextStrByText:dic[@"price_new"]];
        
        
        [goodsLists addObject:goods];
        
    }
    return goodsLists;
}



//商品 套餐内容解析
+ (NSMutableArray *)parseDealPakege:(NSArray *)array {
    NSMutableArray *pakeges =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        ZDealPakegeModel *pakege = [[ZDealPakegeModel alloc] init];
        pakege.tourismStartDate = [Utils getTextStrByText:dic[@"chufa_date"]];
        pakege.tourismDatePrice = [Utils getTextStrByText:dic[@"chufa_price"]];
        pakege.tourismDateChildPrice = [Utils getTextStrByText:dic[@"chufa_price_et"]];
        
        
        
        [pakeges addObject:pakege];
        
    }
    return pakeges;
}


//商品 订单内容解析
+ (NSMutableArray *)parseGoodsOrders:(NSArray *)array {
    NSMutableArray *orders =[NSMutableArray array];
    for (NSDictionary *dic in array) {
        
        ZOrderModel *order = [[ZOrderModel alloc] init];
        
        order.ID = [Utils getTextStrByText:dic[@"order_id"]];
        order.orderID = [Utils getTextStrByText:dic[@"order_sn_id"]];
        order.orderType = [Utils getTextStrByText:dic[@"order_type"]];
        order.orderAllPrice = [Utils getTextStrByText:dic[@"price_sum"]];
        order.orderStatus = [Utils getTextStrByText:dic[@"order_status"]];
        order.payStatus = [Utils getTextStrByText:dic[@"pay_status"]];
        order.orderCreateTime = [Utils getTextStrByText:dic[@"create_time"]];
        order.goodsArray = dic[@"goods"];
        
        [orders addObject:order];
    }
    
     return orders;
}




@end
