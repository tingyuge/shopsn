//
//  ZChangePasswordViewController.m
//  shopSN
//
//  Created by imac on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZChangePasswordViewController.h"

@interface ZChangePasswordViewController ()<UITextFieldDelegate>

@property (strong,nonatomic) UITextField *oldTF;

@property (strong,nonatomic) UITextField *futureTF;

@property (strong,nonatomic) UITextField *againTF;

@end

@implementation ZChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = __AccountBGColor;
    
//    self.titleLb.text = @"修改密码";
    self.title = @"修改密码";
    
    [self initView];
    
}

-(void)initView{
    //1 主视图
    //UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+8, __kWidth, 132)];
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+10, __kWidth, 132)];
    mainView.backgroundColor = HEXCOLOR(0xffffff);
    [self.view addSubview:mainView];
    
    for (int i =0; i<3; i++) {
        UIView *inputV = [[UIView alloc]initWithFrame:CGRectMake(0, 44*i, __kWidth, 44)];
        [mainView addSubview:inputV];
        inputV.backgroundColor = [UIColor clearColor];
        
        //标题
        UILabel *nameLb =[[UILabel alloc]initWithFrame:CGRectMake(15, 15, 60, 15)];
        [inputV addSubview:nameLb];
//        nameLb.backgroundColor = __TestOColor;
        nameLb.font = MFont(14);
        nameLb.textAlignment = NSTextAlignmentLeft;
        nameLb.textColor = HEXCOLOR(0x333333);
        
        //文本框
        UITextField *textTF = [[UITextField alloc]initWithFrame:CGRectMake(CGRectXW(nameLb)+5, 0, __kWidth-30-60, 44)];
        [inputV addSubview:textTF];
//        textTF.backgroundColor = __TestGColor;
        textTF.backgroundColor = [UIColor clearColor];
        textTF.delegate =self;
        textTF.secureTextEntry = YES;
        
        switch (i) {
            case 0:
            {
             nameLb.text= @"原密码";
                _oldTF = textTF;
            }
                break;
            case 1:
            {
             nameLb.text = @"新密码";
                _futureTF = textTF;
            }
                break;
            case 2:
            {
//             nameLb.frame = CGRectMake(15, 15, 60, 15);
//             textTF.frame = CGRectMake(CGRectXW(nameLb)+5, 0, __kWidth-30-nameLb.frame.size.width, 44);
                    nameLb.text = @"再次输入";
             _againTF =textTF;
            }
            default:
            
                break;
        }
        
    }
    //2 中间line
    for (int i=1; i<3; i++) {
        UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 44*i, CGRectW(mainView)-15, 1)];
        lineIV.backgroundColor = HEXCOLOR(0xd8d8d8);
        [mainView addSubview:lineIV];
    }
    
    //确定按钮
    UIButton *enterBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, CGRectYH(mainView)+20, __kWidth-30, 40)];
    [self.view addSubview:enterBtn];
    enterBtn.backgroundColor    = __DefaultColor;
    enterBtn.layer.cornerRadius = 4;
    enterBtn.titleLabel.font    = MFont(15);
    [enterBtn setTitle:@"确定" forState:BtnNormal];
    [enterBtn setTitleColor:[UIColor whiteColor] forState:BtnNormal];
    [enterBtn addTarget:self action:@selector(sureAction) forControlEvents:BtnTouchUpInside];
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/**
 *  确定
 */
-(void)sureAction{
    
    if ([_futureTF.text isEqualToString:_againTF.text]) {
        [self changePasswordRequest];
    }else{
        [SXLoadingView showAlertHUD:@"新密码与重复密码不同，请重新确认" duration:SXLoadingTime];
        return;
    }
}


- (void)changePasswordRequest {
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid],
                            @"password":_oldTF.text,
                            @"newpassword":_futureTF.text,
                            @"newpasswordtwo":_againTF.text};
    
    [ZHttpRequestService POST:@"User/changePassword" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
            [self.navigationController popViewControllerAnimated:true];
            
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];
}


#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
