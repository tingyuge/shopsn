//
//  ZCategoryView.h
//  亿速
//
//  Created by chang on 16/6/22.
//  Copyright © 2016年 wshan. All rights reserved.
//

/* 提供 分类页面 主视图控制器
 *
 *  中间 商品分类视图
 *
 */
#import "BaseView.h"

@class ZCategoryView;
@protocol ZCategoryViewDelegate <NSObject>

//- (void)pushToGoodsListVCWithGoodsName:(NSString *)goodsClassName withGoodsID:(NSIndexPath *)indexPath;

- (void)pushToGoodsListVCwithSubCategoryID:(NSString *)subCategoryID andSubCategoryName:(NSString *)subCategoryName;

@end


@interface ZCategoryView : BaseView

@property (nonatomic, weak) id<ZCategoryViewDelegate>delegate;

/** 分类页面 主类 数组 */
@property (nonatomic, strong) NSArray *categoryArr;

/** 分类页面 子类 数组 */
@property (nonatomic, strong) NSArray *subCategoryArr;

/** 分类页面 产品 数组 */
@property (nonatomic, strong)NSArray *titleArr;

/** 分类页面 判断是否是会员商品 */
@property (nonatomic, assign)BOOL isGoods;

- (void)getDataWithCategoryArray:(NSArray *)categoryArray andRowTitleArray:(NSArray *)rowTitleArray;

/**设置分类 默认选择 */
- (void)defaultChoose:(NSArray *)array;

@end
