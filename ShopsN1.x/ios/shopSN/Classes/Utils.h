//
//  Utils.h
//  shopSN
//
//  Created by yisu on 16/9/6.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

//string 转成富文本
+(NSMutableAttributedString*)changeText:(NSString*)text lineSpace:(CGFloat)lineSpace fontSize:(CGFloat)fontSize;

//计算富文本 尺寸
+(float)getAttributedStringHeightByText:(NSMutableAttributedString*)text andWidth:(float)width;


//计算宽度  是否加粗  是否+0.5
+(CGSize)sizeByText:(NSString*)text andFontSize:(CGFloat)fontSize andWidth:(float)width andIsBold:(BOOL)isBold andIsOriginal:(BOOL)isOriginal;

//计算高度 针对字体+0.5
+(float)heightForString:(NSString *)value FontSize:(CGFloat)fontSize andWidth:(float)width;



/*
 字符转换
 */
//获取有效字符串
+(NSString*)getTextStrByText:(id)text;
//汉子转拼音
+(NSString*)changeToPingyin:(NSString*)originalStr;

//时间戳 转成 时间NSDate
+(NSDate*)getDateForTimeStamp:(NSString*)timeStamp;
//NSDate 转成字符串
+(NSString*)getDateStr:(NSDate*)date type:(NSString*)type;

//获取当前系统的时间戳
+(NSInteger)getTimeSp;

//获取固定时间戳  到现在的  时间
+(NSInteger)getOverTime:(NSString*)timeStamp;

/**
 *  车牌号正则表达式
 *
 *  @param carNo 输入的车牌号
 *
 *  @return 是否正确
 */
//+ (BOOL)validateCarNo:(NSString *)carNo;

/**
 *  根据颜色生成Image
 *
 *  @param color
 *
 *  @return 由color生成的Image
 */
+ (UIImage *)buttonImageFromColor:(UIColor *)color;



@end
