//
//  ZCommonGoodsPackageCell.h
//  shopSN
//
//  Created by yisu on 16/9/13.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 普通商品 简介页面
 *
 *   套餐内容 cell
 *
 */

#import "BaseTableViewCell.h"

@interface ZCommonGoodsPackageCell : BaseTableViewCell

/** 商品简介页面 套餐标题label */
@property (nonatomic, strong) UILabel *packageTitleLb;



/** 商品简介页面 加载套餐数据 */
- (void)setGoodsIntrPackage:(ZGoodsDeal *)deal;


@end
