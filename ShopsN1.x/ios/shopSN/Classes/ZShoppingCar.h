//
//  ZShoppingCar.h
//  shopSN
//
//  Created by yisu on 16/7/1.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZShopGoods.h"

@interface ZShoppingCar : NSObject

/** 店铺下的商品 */
@property (nonatomic, strong) NSMutableArray<ZShopGoods *> *shopGoodsArr;

/** 店铺编辑button隐藏开关 */
@property (nonatomic, assign) BOOL shopEditButtonHidden;


/** 店铺下所有商品是否勾选 */
@property (nonatomic, assign) BOOL shopCheck;

/** 店铺下商品编辑开关 */
@property (nonatomic, assign) BOOL goodsDeleHidden;


@end
