//
//  ZCommonGoodsHeader.h
//  shopSN
//
//  Created by yisu on 16/9/17.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseView.h"

@interface ZCommonGoodsHeader : BaseView

/** 头部广告图片数组 */
@property (nonatomic, strong) NSArray *headerImageArray;

//获取数据
- (void)getDataWithImageArray:(NSArray *)headerImageArray;



@end
