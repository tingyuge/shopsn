//
//  ZCMyorderEndingFooter.h
//  shopSN
//
//  Created by yisu on 16/10/11.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的订单 订单管理
 *
 *  交易完成 footer
 *
 */


#import "BaseView.h"

@class ZCMyorderEndingFooter;
@protocol ZCMyorderEndingFooterDelegate <NSObject>

- (void)orderEndingOperating:(ZOrderModel *)order andButton:(UIButton *)button;

@end

@interface ZCMyorderEndingFooter : BaseView

/** 已完成订单  订单 */
@property (nonatomic, strong) ZOrderModel *order;



/** 已完成订单 商品小计价格 */
@property (nonatomic, strong) UILabel *settlePriceLb;

/** 已完成订单 商品小计数量 */
@property (nonatomic, strong) UILabel *settleCountLb;

/** 已完成订单 商品下单时间 */
@property (nonatomic, strong) UILabel *orderTimeLb;

/** 已完成订单footer 代理方法 */
@property (weak, nonatomic) id<ZCMyorderEndingFooterDelegate>delegate;

/** 添加旅游类商品信息 */
- (void)addTourismViewInfoDataWithAdultsNum:(NSString *)adultsNum andAdultsPrice:(NSString *)adultsPrice andMinorsNum:(NSString *)minorsNum andMinorsPrice:(NSString *)minorsPrice;


@end
