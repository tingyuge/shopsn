//
//  ZCMyOrderWaitingPayFooter.m
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCMyOrderWaitingPayFooter.h"

@interface ZCMyOrderWaitingPayFooter ()
{
    UIButton *payingBtn;//立即付款
    UIButton *cancelPayingBtn;//取消付款
    
    //旅游类商品 信息内容
    UIView *_tourismInfoView;
    UILabel *adultsLb;
    UILabel *minorsLb;
}

@end


@implementation ZCMyOrderWaitingPayFooter

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
        
        
        //1 背景
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:bgView];
        //bgView.backgroundColor = __TestOColor;
        
        //2 价格信息 h105
        UIView *priceInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 105)];
        //priceInfoView.backgroundColor = __TestGColor;
        [bgView addSubview:priceInfoView];
        [self addPriceInfoSubViews:priceInfoView];
        
        
        
        //3 订单操作 h45
//        UIView *orderOperatingView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(priceInfoView), CGRectW(bgView), 45)];
//        orderOperatingView.backgroundColor = __TestOColor;
//        [bgView addSubview:orderOperatingView];
//        [self addOrderOperatingSubViews:orderOperatingView];
        
        //立即付款按钮
        payingBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(self)-10-75, CGRectYH(priceInfoView)+10, 75, 25)];
        [self addSubview:payingBtn];
        payingBtn.titleLabel.font = MFont(11);
        payingBtn.backgroundColor = __DefaultColor;
        payingBtn.layer.cornerRadius = 3.0f;
        [payingBtn setTitle:@"立即付款" forState:BtnNormal];
        [payingBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
        payingBtn.tag = 1300;
        [payingBtn addTarget:self action:@selector(waitPayButtonAciton:) forControlEvents:BtnTouchUpInside];
        
        //取消订单按钮 边框color(169 169 169) (0xa9a9a9)
        cancelPayingBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(self)-10*2-75*2, CGRectYH(priceInfoView)+10, 75, 25)];
        [self addSubview:cancelPayingBtn];
        cancelPayingBtn.titleLabel.font = MFont(11);
        //cancelPayingBtn.backgroundColor = __TestOColor;
        cancelPayingBtn.layer.borderWidth = 1.0f;
        cancelPayingBtn.layer.borderColor = HEXCOLOR(0xa9a9a9).CGColor;
        cancelPayingBtn.layer.cornerRadius = 3.0f;
        [cancelPayingBtn setTitle:@"取消订单" forState:BtnNormal];
        [cancelPayingBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
        cancelPayingBtn.tag = 1301;
        [cancelPayingBtn addTarget:self action:@selector(waitPayButtonAciton:) forControlEvents:BtnTouchUpInside];
        
        
        //底部边条
//        UIImageView *bottomIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-10, CGRectW(bgView), 10)];
//        [bgView addSubview:bottomIV];
//        bottomIV.backgroundColor = HEXCOLOR(0xdedede);

        
        
    }
    return self;
}




#pragma mark - 数量和金额
- (void)addPriceInfoSubViews:(UIView *)view {
    
    //0 信息
    _tourismInfoView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, CGRectW(view)-20, 40)];
    [view addSubview:_tourismInfoView];
    //_tourismInfoView.backgroundColor = __TestOColor;
    _tourismInfoView.hidden = YES;
    
    adultsLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectW(_tourismInfoView), 20)];
    [_tourismInfoView addSubview:adultsLb];
    adultsLb.font = MFont(12);
    adultsLb.textColor = __DefaultColor;
    adultsLb.text = @"成人：人数_2 单价_￥2999.00  小计:￥5998.00";
    
    
    minorsLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectYH(adultsLb), CGRectW(_tourismInfoView), 20)];
    [_tourismInfoView addSubview:minorsLb];
    minorsLb.font = MFont(12);
    minorsLb.textColor = __DefaultColor;
    minorsLb.text = @"儿童：人数_2 单价_￥1999.00  小计:￥3998.00";
    
    //1 小计 价格 color (254 65 0) (fe4100)
    _settlePriceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)-130, CGRectH(view)-50, 120, 15)];
    [view addSubview:_settlePriceLb];
    //_settlePriceLb.backgroundColor = __TestGColor;
    _settlePriceLb.font = MFont(11);
    _settlePriceLb.textColor = __MoneyColor;
    _settlePriceLb.textAlignment = NSTextAlignmentRight;
    _settlePriceLb.text = @"应付金额:￥589.00";
    [self setSettlePriceLabeltextAttributes:_settlePriceLb];
    
    //2 小计 商品件数
    _settleCountLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(_settlePriceLb)-100, CGRectH(view)-50, 100, 15)];
    [view addSubview:_settleCountLb];
    //_settleCountLb.backgroundColor = __TestOColor;
    _settleCountLb.font = MFont(11);
    _settleCountLb.textColor = HEXCOLOR(0x333333);
    _settleCountLb.textAlignment = NSTextAlignmentRight;
    _settleCountLb.text = @"小计(共1件)：";
    
    //3 商品 下单时间
    _orderTimeLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectH(view)-20, CGRectW(view)-20, 15)];
    [view addSubview:_orderTimeLb];
    //_orderTimeLb.backgroundColor = __TestOColor;
    _orderTimeLb.font = MFont(11);
    _orderTimeLb.textColor = HEXCOLOR(0x333333);
    _orderTimeLb.text = @"下单时间：2016-09-16 14:00:00";
    
    //4 边线line3
    UIImageView *lineIV3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectH(view)-1, CGRectW(view)-10, 1)];
    [view addSubview:lineIV3];
    lineIV3.backgroundColor = HEXCOLOR(0xdedede);
}


#pragma mark - orderOperatingSubViews
- (void)addOrderOperatingSubViews:(UIView *)view {
    //立即付款按钮
    payingBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(view)-10-75, 10, 75, 25)];
    [view addSubview:payingBtn];
    payingBtn.titleLabel.font = MFont(11);
    payingBtn.backgroundColor = __DefaultColor;
    payingBtn.layer.cornerRadius = 3.0f;
    [payingBtn setTitle:@"立即付款" forState:BtnNormal];
    [payingBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    payingBtn.tag = 2300;
    [payingBtn addTarget:self action:@selector(waitPayButtonAciton:) forControlEvents:BtnTouchUpInside];
    
    //取消订单按钮 边框color(169 169 169) (0xa9a9a9)
    cancelPayingBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(view)-10*2-75*2, 10, 75, 25)];
    [view addSubview:cancelPayingBtn];
    cancelPayingBtn.titleLabel.font = MFont(11);
    //cancelPayingBtn.backgroundColor = __TestOColor;
    cancelPayingBtn.layer.borderWidth = 1.0f;
    cancelPayingBtn.layer.borderColor = HEXCOLOR(0xa9a9a9).CGColor;
    cancelPayingBtn.layer.cornerRadius = 3.0f;
    [cancelPayingBtn setTitle:@"取消订单" forState:BtnNormal];
    [cancelPayingBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    cancelPayingBtn.tag = 2301;
    [cancelPayingBtn addTarget:self action:@selector(waitPayButtonAciton:) forControlEvents:BtnTouchUpInside];
}

- (void)setSettlePriceLabeltextAttributes:(UILabel *)label {
    NSString *text = label.text;
    //NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    //修改前5为颜色
    [attri addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 5)];
    
    [label setAttributedText:attri];
}

- (void)waitPayButtonAciton:(UIButton *)btn {
    
    [self.delegate orderWaitPayOperating:self.order andButton:btn];
}


- (void)addTourismViewInfoDataWithAdultsNum:(NSString *)adultsNum andAdultsPrice:(NSString *)adultsPrice andMinorsNum:(NSString *)minorsNum andMinorsPrice:(NSString *)minorsPrice {
    
    
    
    //控件出来
    _tourismInfoView.hidden = NO;
    _settleCountLb.hidden = YES;
    
    
    float adultsPriceSum;
    float minorsPriceSum;
    adultsPriceSum = [adultsNum intValue] * [adultsPrice floatValue];
    minorsPriceSum = [minorsNum intValue] * [minorsPrice floatValue];
    
    adultsLb.text = [NSString stringWithFormat:@"成人：人数_%@ 单价_￥%@  小计：￥%.2f",adultsNum, adultsPrice, adultsPriceSum];
    minorsLb.text = [NSString stringWithFormat:@"儿童：人数_%@ 单价_￥%@  小计：￥%.2f",minorsNum, minorsPrice, minorsPriceSum];
    
    if (IsNilString(minorsNum) && IsNilString(minorsPrice)) {
        minorsLb.hidden = YES;
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
