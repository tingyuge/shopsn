//
//  ZCMyOrderWaitingReceiveFooter.h
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 我的订单页面
 *
 *  等待到货 footer
 *
 */
#import "BaseView.h"

@class ZCMyOrderWaitingReceiveFooter;
@protocol ZCMyOrderWaitingReceiveFooterDelegate <NSObject>

- (void)orderWaitReceiveOperating:(ZOrderModel *)order andButton:(UIButton *)button;

@end

@interface ZCMyOrderWaitingReceiveFooter : BaseView

/** 待收货订单  订单 */
@property (nonatomic, strong) ZOrderModel *order;



/** 待收货订单 商品小计价格 */
@property (nonatomic, strong) UILabel *settlePriceLb;

/** 待收货订单 商品小计数量 */
@property (nonatomic, strong) UILabel *settleCountLb;

/** 待收货订单 商品下单时间 */
@property (nonatomic, strong) UILabel *orderTimeLb;

/** 待收货订单footer 代理方法 */
@property (weak, nonatomic) id<ZCMyOrderWaitingReceiveFooterDelegate>delegate;

/** 添加旅游类商品信息 */
- (void)addTourismViewInfoDataWithAdultsNum:(NSString *)adultsNum andAdultsPrice:(NSString *)adultsPrice andMinorsNum:(NSString *)minorsNum andMinorsPrice:(NSString *)minorsPrice;

@end
