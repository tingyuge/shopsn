//
//  ZCenterPrivilegeCell.h
//  shopSN
//
//  Created by yisu on 16/7/15.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCenterPrivilegeCell : UICollectionViewCell

/** 特权列表 特权项目图片 */
@property (nonatomic, strong) UIImageView *privilegeIV;

/** 特权列表 特权项目标题 */
@property (nonatomic, strong) UILabel *privilegeTitleLb;

/** 特权列表 特权项目标题 */
@property (nonatomic, strong) UILabel *privilegeDetailLb;

@end
