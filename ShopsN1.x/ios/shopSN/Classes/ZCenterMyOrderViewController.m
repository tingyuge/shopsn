//
//  ZCenterMyOrderViewController.m
//  shopSN
//
//  Created by yisu on 16/7/13.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterMyOrderViewController.h"


//#import "ZCMyGoodsOrderWaitPayCell.h"
//#import "ZCMyGoodsOrderWaitReceiveCell.h"
//#import "ZCMyGoodsOrderWaitJudgeCell.h"
//#import "ZCMyGoodsOrderWaitCompleteCell.h"
//#import "ZCMyGoodsOrderEndCell.h"

#import "ZCMyOrderCell.h"

#import "ZCMyOrderHeaderView.h"       //header
#import "ZCMyOrderWaitingPayFooter.h" //待付款footer
#import "ZCMyOrderWaitingReceiveFooter.h"//等待收货footer
#import "ZCMyorderEndingFooter.h"       //订单完成footer
#import "ZCMyOrderClosePayFooter.h"   //交易关闭footer
#import "ZCMyOrderSuccessPayFooter.h"//交易成功footer

#import "ZCMyOrderChangeOrReturnFooter.h"//退货换货footer




#import "WXApi.h"
#import "ZPayMoney.h"

@interface ZCenterMyOrderViewController ()<UITableViewDataSource, UITableViewDelegate, UITextViewDelegate,ZCMyOrderWatingPayFooterDelegate, ZCMyOrderWaitingReceiveFooterDelegate, ZCMyorderEndingFooterDelegate, ZCMyorderChangeOrReturnFooterDelegate>
{
    UIView *chooseView;  //选择视图
    UITableView *_orderTableView;//订单列表
    UIView *bottomView; //底部视图
    UIView *footerView;
    UIView *messageBGView;//信息弹出框
    UIView *messageView;//文本信息视图
    
    UITextView *caseText;//退货原因text
    
    
    
    NSArray *_currentDataArray;//当前数据
    int _currentPage;//页数
    
    NSString *_actionStr;//请求名称
    NSString *_returnsCaseStr;//退货原因
    
    //BOOL _isPay;//默认支付宝支付
    UIButton *_currentButton;
    ZOrderModel *_currenOrder;
    
    
    
    
}

/** 选项按钮 */
@property (nonatomic, strong) NSMutableArray *btnArr;

/** 订单数据数组 */
@property (nonatomic, strong) NSMutableArray *ordersArray;


@end

@implementation ZCenterMyOrderViewController

#pragma mark - ==== 页面设置 =====
- (void)viewWillAppear:(BOOL)animated {
    
    //更新tabel footer
    [_orderTableView headerEndRefreshing];
    _orderTableView.footerHidden = NO;
    _orderTableView.tableFooterView.hidden = YES;
    
    //初始化数据
    _currentPage = 1;
    [_ordersArray removeAllObjects];
    if (_chooseNum) {
        
        [self fromCenterPageChooseNum:_chooseNum];
    }else{
        _actionStr = @"Order/orderList";//全部订单
        [self getData];

    }
    
    
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.tabBar.hidden = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLb.text = @"我的订单";
    
    //初始 数据
    _currentPage = 1;
    _ordersArray = [NSMutableArray array];
    
//    _actionStr = @"order_list";//全部订单
//    [self getData];
    
    //初始化 中间视图 相关子视图
    [self initSubViews:self.mainMiddleView];
    
}


#pragma mark -===== 获取数据 =====
- (void)getData {
    
    //订单数据请求
    [self myOrdersDataRequestWithPageNum:_currentPage];
}


- (void)myOrdersDataRequestWithPageNum:(int)pageNum {
    [_ordersArray removeAllObjects];
    NSString *page = [NSString stringWithFormat:@"%d",pageNum];
    NSDictionary *params = @{@"page":page,@"token":[UdStorage getObjectforKey:Userid]};
    [ZHttpRequestService POST:_actionStr withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [_ordersArray removeAllObjects];
            [SXLoadingView hideProgressHUD];
            NSArray *array = [Parse parseGoodsOrders:jsonDic[@"data"]];

            _currentDataArray = array;
            [_ordersArray addObjectsFromArray:array];
            [_orderTableView reloadData];
        }else{

            NSString *message = jsonDic[@"message"];
            if ([message isEqualToString:@"查询失败"]) {
                footerView.hidden = NO;
            }else {
                [SXLoadingView showAlertHUD:message duration:SXLoadingTime];
                _currentPage--;
            }
             [_orderTableView reloadData];
        }
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"请求失败" duration:SXLoadingTime];
        [_orderTableView reloadData]; 
        _currentPage--;
    } animated:YES withView:nil];
}

#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    
    //1 选择功能 视图
    chooseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), 40)];
    [view addSubview:chooseView];
    
//    //添加上下边线
//    for (int i=0; i<2; i++) {
//        UIImageView *linIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, i*39, CGRectW(chooseView), 1)];
//        linIV.backgroundColor = HEXCOLOR(0xdedede);
//        [chooseView addSubview:linIV];
//    }
    UIImageView *linIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 39, CGRectW(chooseView), 1)];
    [chooseView addSubview:linIV];
    linIV.backgroundColor = HEXCOLOR(0xdedede);
    _btnArr = [NSMutableArray array];
    
    //添加选择按钮
    ///全部 待付款 待收货 待评价  退货/换货
    //NSArray *titles = @[@"全部",@"待付款",@"待收货",@"待评价",@"退货/换货"];
    NSArray *titles = @[@"全部",@"待付款",@"待收货",@"已完成",@"退货中"];
    for (int i=0; i<titles.count; i++) {
        
        [self initChooseButtonWith:CGRectMake(i*(CGRectW(chooseView)-20)/5, 0, (CGRectW(chooseView)-20)/5, 40) withIndex:i withTitle:titles[i]];
    }
    
    
    //2 订单列表 表视图
    //_orderTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, CGRectW(view), CGRectH(view)-40-40)];
    CGRect frame = CGRectMake(0, 40, CGRectW(view), CGRectH(view)-40-40);
    _orderTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
    
    [view addSubview:_orderTableView];
    _orderTableView.backgroundColor = __AccountBGColor;
    _orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _orderTableView.dataSource = self;
    _orderTableView.delegate   = self;
    
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, CGRectW(view), 30)];
    UILabel *endNoteLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectH(footerView)-20, CGRectW(footerView)-20, 20)];
    [footerView addSubview:endNoteLb];
    endNoteLb.font = MFont(14);
    endNoteLb.textAlignment = NSTextAlignmentCenter;
    endNoteLb.text = @"没有更多数据了 ...";
    footerView.hidden = YES;
    _orderTableView.tableFooterView = footerView;
    
    //刷新控件
    //[_orderTableView addHeaderWithTarget:self action:@selector(refreshGoodsData)];//下拉刷新
    [_orderTableView addFooterWithTarget:self action:@selector(loadMoreGoodsData)];//上拉加载更多

    //底部视图
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectH(view)-40, CGRectW(view), 40)];
    [view addSubview:bottomView];
    bottomView.backgroundColor = HEXCOLOR(0xdedede);
    
}

//添加按钮
- (void)initChooseButtonWith:(CGRect)frame withIndex:(NSInteger)index withTitle:(NSString *)title {
    
    
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    [chooseView addSubview:btn];
    [_btnArr addObject:btn];
    btn.titleLabel.font = MFont(13);
    [btn setTitle:title forState:BtnNormal];
    [btn setTitleColor:HEXCOLOR(0x3e3e3e) forState:BtnNormal];
    [btn setTitleColor:__DefaultColor forState:BtnStateSelected];
    btn.tag = 130 + index;
    [btn addTarget:self action:@selector(chooseBtnAction:) forControlEvents:BtnTouchUpInside];
    
    if (!index) {
        btn.selected = YES;
    }
    
}


#pragma mark - ==== 按钮触发方法 =====
- (void)chooseBtnAction:(UIButton *)sender {
    sender.selected = YES;
//    BOOL flag;
//    flag = sender.selected;
    
    for (UIButton *button in _btnArr) {
        if (button == sender) {
            
            continue;
        } else
            
            button.selected = NO;
        
    }
    
    switch (sender.tag - 130) {
        case 0:
        {
            NSLog(@"全部");
            //重新初始化 数据
            _currentPage = 1;
            
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/orderList";//全部订单
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
        }
            break;
        case 1:
        {
            NSLog(@"待付款");
            //重新初始化 数据
            _currentPage = 1;
            
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/paymentList";//待支付订单
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
            
        }
            break;
        case 2:
        {
            NSLog(@"待收货");
            //重新初始化 数据
            _currentPage = 1;
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/receiptGoods"; //待收货订单
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
        }
            break;
            
        case 3:
        {
            NSLog(@"已完成");
            //重新初始化 数据
            _currentPage = 1;
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/paymentsOK"; //待评价商品
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
        }
            break;
        case 4:
        {
            NSLog(@"退货或换货");
            //重新初始化 数据
            _currentPage = 1;
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/tuiHuoStatus"; //待完成
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
        }
            break;

        default:
            break;
    }
    
}




#pragma mark- ===== tableView DataSource and Delegate =====


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return _ordersArray.count;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //return 5;
    //return _ordersArray.count;
    
    
    ZOrderModel *order = [[ZOrderModel alloc] init];
    order = _ordersArray[section];
    return order.goodsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZOrderModel *order = [[ZOrderModel alloc] init];
    order = _ordersArray[indexPath.section];
    NSArray *goodsArray = order.goodsArray;
    ZCMyOrderCell *cell = [[ZCMyOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    NSDictionary *dic = goodsArray[indexPath.row];
    
    //标题
    cell.descLb.text = dic[@"title"];
    //现价
    cell.nowPriceLb.text = [NSString stringWithFormat:@"￥%@",dic[@"goods_price"]];
    //数量
    cell.countLb.text = [NSString stringWithFormat:@"x%@",dic[@"goods_num"]];
    //图片
    [cell.goodsIV sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",GoodsImageUrl,dic[@"pic_url"]]] placeholderImage:MImage(@"zp_k.png")];
    cell.goodsIV.contentMode = UIViewContentModeScaleAspectFit;
    
    
    
    
    return cell;
    
    
}

//
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //return 290;
    
    return 60;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 160;
}




//header
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    ZOrderModel *order = [[ZOrderModel alloc] init];
    if (_ordersArray.count) {
        order = _ordersArray[section];
    }

    ZCMyOrderHeaderView *headerView = [[ZCMyOrderHeaderView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(tableView), 40)];
    
    
   headerView.orderDateLb.text = [NSString stringWithFormat:@"订单号：%@",order.orderID];
    switch ([order.orderStatus intValue]) {
        case 0://默认状态
//            if ([order.orderStatus isEqualToString:@"0"]) {
                headerView.orderTypeLb.text = @"待付款";
//            }else{
//                headerView.orderTypeLb.text = @"已付款";
//            }
//            
            break;
        case 1:
//            headerView.orderTypeLb.text = @"待收货";//已付款
//            break;
        case 2:

        case 3:
        case 10:
        case 11:
            headerView.orderTypeLb.text = @"待收货";
            break;
        case 4:
        case 9:
            headerView.orderTypeLb.text = @"已完成";//已签收
            break;
        case 5:
        case 6:
        case 7:
        case 8:
            headerView.orderTypeLb.text = @"退货中";//申请退货
            break;
        default:
            break;
    }
    
    
    return headerView;
}

//footer
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    ZOrderModel *order = [[ZOrderModel alloc] init];
    order = _ordersArray[section];
    NSString *flag = [NSString stringWithFormat:@"%@",order.orderStatus];
        switch ([flag integerValue]) {
            case 0://未知状态
            {
                ZCMyOrderWaitingPayFooter *footer = [[ZCMyOrderWaitingPayFooter alloc] initWithFrame:CGRectMake(0, 0, CGRectW(tableView), 160)];
                footer.order = order;
                footer.delegate = self;

                //应付总金额
                footer.settlePriceLb.text = [NSString stringWithFormat:@"￥%@.00",order.orderAllPrice];
                [self setSettlePriceLabeltextAttributes:footer.settlePriceLb];

                //下单时间
                NSDate *date = [Utils getDateForTimeStamp:order.orderCreateTime];
                footer.orderTimeLb.text = [Utils getDateStr:date type:@"yyyy-MM-dd HH:mm:ss"];

                //件数
                int goodsNum = 0;
                for (int i= 0; i < order.goodsArray.count; i++) {
                    NSDictionary *dic = order.goodsArray[i];
                    int tem = [dic[@"goods_num"] intValue];
                    goodsNum = goodsNum + tem;
                }
                //NSLog(@"goods_sum:%d",goodsNum);
                footer.settleCountLb.text = [NSString stringWithFormat:@"小计(共%d件)：",goodsNum];
                return footer;
            }
                break;
                
            case 1:
            case 2:

            case 3:
            case 10:
            case 11://已付款 发货中 已发货 待发货 待收货
            {
                
                ZCMyOrderWaitingReceiveFooter *footer = [[ZCMyOrderWaitingReceiveFooter alloc] initWithFrame:CGRectMake(0, 0, CGRectW(tableView), 160)];
                footer.order = order;
                footer.delegate = self;
                
                //应付总金额
                footer.settlePriceLb.text = [NSString stringWithFormat:@"￥%@.00",order.orderAllPrice];
                [self setSettlePriceLabeltextAttributes:footer.settlePriceLb];
                
                //下单时间
                NSDate *date = [Utils getDateForTimeStamp:order.orderCreateTime];
                footer.orderTimeLb.text = [Utils getDateStr:date type:@"yyyy-MM-dd HH:mm:ss"];
                
                //件数
                int goodsNum = 0;
                for (int i= 0; i < order.goodsArray.count; i++) {
                    NSDictionary *dic = order.goodsArray[i];
                    int tem = [dic[@"goods_num"] intValue];
                    goodsNum = goodsNum + tem;
                }
                //NSLog(@"goods_sum:%d",goodsNum);
                footer.settleCountLb.text = [NSString stringWithFormat:@"小计(共%d件)：",goodsNum];
                return footer;

            }
                break;
            case 5:
            case 6:
            case 7:
            case 8://退货中 类文件需修改
            {
                ZCMyOrderChangeOrReturnFooter *footer = [[ZCMyOrderChangeOrReturnFooter alloc]initWithFrame:CGRectMake(0, 0, CGRectW(tableView), 160)];
                footer.order = order;
                footer.delegate = self;
                //应付总金额
                footer.settlePriceLb.text = [NSString stringWithFormat:@"￥%@",order.orderAllPrice];
                [self setSettlePriceLabeltextAttributes:footer.settlePriceLb];
                //下单时间
                NSDate *date = [Utils getDateForTimeStamp:order.orderCreateTime];
                footer.orderTimeLb.text = [Utils getDateStr:date type:@"yyyy-MM-dd HH:mm:ss"];
                //件数
                int goodsNum = 0;
                for (int i= 0; i < order.goodsArray.count; i++) {
                    NSDictionary *dic = order.goodsArray[i];
                    int tem = [dic[@"goods_num"] intValue];
                    goodsNum = goodsNum + tem;
                }
                //NSLog(@"goods_sum:%d",goodsNum);
                footer.settleCountLb.text = [NSString stringWithFormat:@"小计(共%d件)：",goodsNum];
                return footer;
            }
                break;
            case 4:
            case 9://交易完成
            {
                ZCMyorderEndingFooter *footer = [[ZCMyorderEndingFooter alloc] initWithFrame:CGRectMake(0, 0, CGRectW(tableView), 160)];
                footer.order = order;
                footer.delegate = self;
                
                //应付总金额
                footer.settlePriceLb.text = [NSString stringWithFormat:@"￥%@",order.orderAllPrice];
                [self setSettlePriceLabeltextAttributes:footer.settlePriceLb];
                
                //下单时间
                NSDate *date = [Utils getDateForTimeStamp:order.orderCreateTime];
                footer.orderTimeLb.text = [Utils getDateStr:date type:@"yyyy-MM-dd HH:mm:ss"];
                
                //件数
                int goodsNum = 0;
                for (int i= 0; i < order.goodsArray.count; i++) {
                    NSDictionary *dic = order.goodsArray[i];
                    int tem = [dic[@"goods_num"] intValue];
                    goodsNum = goodsNum + tem;
                }
                //NSLog(@"goods_sum:%d",goodsNum);
                footer.settleCountLb.text = [NSString stringWithFormat:@"小计(共%d件)：",goodsNum];

                return footer;

            }
                break;
            default:
                return nil;
                break;
        }
//    }else{
//        return nil;
//    }
//
//    if ([order.orderStatus isEqualToString:@"0"]) {
//        //待付款
//
//    }else{
//        //已付款
//        ZCMyOrderWaitingReceiveFooter *footer = [[ZCMyOrderWaitingReceiveFooter alloc] initWithFrame:CGRectMake(0, 0, CGRectW(tableView), 160)];
//        footer.order = order;
//        footer.delegate = self;
//        
//        //应付总金额
//        footer.settlePriceLb.text = [NSString stringWithFormat:@"￥%@.00",order.orderAllPrice];
//        [self setSettlePriceLabeltextAttributes:footer.settlePriceLb];
//        
//        //下单时间
//        NSDate *date = [Utils getDateForTimeStamp:order.orderCreateTime];
//        footer.orderTimeLb.text = [Utils getDateStr:date type:@"yyyy-MM-dd HH:mm:ss"];
//        
//        //件数
//        int goodsNum = 0;
//        for (int i= 0; i < order.goodsArray.count; i++) {
//            NSDictionary *dic = order.goodsArray[i];
//            int tem = [dic[@"goods_num"] intValue];
//            goodsNum = goodsNum + tem;
//        }
//        //NSLog(@"goods_sum:%d",goodsNum);
//        footer.settleCountLb.text = [NSString stringWithFormat:@"小计(共%d件)：",goodsNum];
//        
//        //判断是否是旅游产品
//        if ([order.orderType isEqualToString:@"1"]) {
//            NSArray *goodsArray = order.goodsArray;
//            NSDictionary *dic = goodsArray[0];
//            
//            [footer addTourismViewInfoDataWithAdultsNum:dic[@"goods_num"] andAdultsPrice:dic[@"price_new"] andMinorsNum:dic[@"goods_num_et"] andMinorsPrice:dic[@"chufa_price_et"]];
//        }
//        
//        return footer;
//    }
//    

}



#pragma mark - ===== cell 文字设置
- (void)setSettlePriceLabeltextAttributes:(UILabel *)label {
    NSString *text = label.text;
    NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    //修改前6为颜色
    //[attri addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 6)];
    [attri addAttribute:NSFontAttributeName value:MFont(15) range:NSMakeRange(1, length-1)];
    
    [label setAttributedText:attri];
}


#pragma mark - ===== cell Delegate =====
#pragma mark - 待支付
//- (void)orderWaitPayOperating:(ZOrderModel *)order andButton:(UIButton *)button {
//    
//    if (button.tag == 1300) {
//        NSLog(@"立即支付");
//        _currenOrder = order;
//        _currentButton = button;
//        [self showChoosePayTypeMessageView];
//        
//        
//    }else {
//        NSLog(@"取消订单");
//        [self orderCancelActionRequest:order withButton:button];
//        
//    }
//    
//}

- (void)orderWaitPayOperating:(ZOrderModel *)order andButton:(UIButton *)button {
    if (button.tag == 1300) {
        NSLog(@"立即支付");
        _currenOrder = order;
        _currentButton = button;
        [self showChoosePayTypeMessageView];
        
        
    }else {
        NSLog(@"取消订单");
        [self orderCancelActionRequest:order withButton:button];
        
    }
}



#pragma mark - 待收货
//- (void)orderWaitReceiveOperating:(ZOrderModel *)order andButton:(UIButton *)button {
//    _currenOrder = order;
//    _currentButton = button;
//    if (button.tag == 1310) {
//        NSLog(@"确认收货");
//        [self orderSureReceiveActionRequest:order withButton:button];
//    }else {
//        NSLog(@"查看物流");
//        [self orderDeliveryInfoActionRequest:order withButton:button];
//        
//        //test
////        [self showGoodsReturnsMessageView];
////        NSString *urlStr = @"http://www.zzumall.com/kuaidi/index.php?num=2222222222";
////        [self showOrderDeliveryMessageViewWithUrl:urlStr];
//        
//        //[self showChoosePayTypeMessageView];
//    }
//}

- (void)orderWaitReceiveOperating:(ZOrderModel *)order andButton:(UIButton *)button {
    _currenOrder = order;
    _currentButton = button;
    if (button.tag == 1310) {
        NSLog(@"确认收货");
        [self orderSureReceiveActionRequest:order withButton:button];
    }else {
        NSLog(@"查看物流");
        [self orderDeliveryInfoActionRequest:order withButton:button];
    }
}

#pragma mark - 订单完成
- (void)orderEndingOperating:(ZOrderModel *)order andButton:(UIButton *)button {
    if (button.tag == 1350) {
        NSLog(@"未定义");
    }else{
        NSLog(@"查看物流");
        [self orderDeliveryInfoActionRequest:order withButton:button];
    }
}

#pragma mark - 订单退货
- (void)orderChangeOrReturnOperatiing:(ZOrderModel *)order andButton:(UIButton *)button {
    if (button.tag == 1330) {
        NSLog(@"申请退货");
        [self goodsReturnsActionRequest:order withButton:button];
    }else{
        NSLog(@"查看物流");
        [self orderDeliveryInfoActionRequest:order withButton:button];
    }
}


#pragma mark - 待评价cell
- (void)orderWaitJudgeOperating:(ZOrderModel *)order andButton:(UIButton *)button {
    if (button.tag == 1320) {
        NSLog(@"进行评价");
    }else {
        NSLog(@"查看物流");
        [self orderDeliveryInfoActionRequest:order withButton:button];
    }
}

#pragma mark - 待完成cell
//- (void)orderWaitCompleteOperating:(ZOrderModel *)order andButton:(UIButton *)button {
//    if (button.tag == 1330) {
//        NSLog(@"进行退货");
//        [self goodsReturnsActionRequest:order withButton:button];
//        
//    }else {
//        NSLog(@"查看物流");
//        [self orderDeliveryInfoActionRequest:order withButton:button];
//    }
//}




#pragma mark - ===== 订单类型 按钮请求方法 =====
#pragma mark - 取消订单请求
- (void)orderCancelActionRequest:(ZOrderModel *)order withButton:(UIButton *)button {
    
    //115.159.146.202/api/api.php?action=cancel_order&orders_num=
    NSDictionary *params = @{@"orders_num":order.ID,@"token":[UdStorage getObjectforKey:Userid]};
    
    [ZHttpRequestService POST:@"Order/cancelOrder" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [SXLoadingView showAlertHUD:@"订单取消成功" duration:2];
        }
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"请求失败" duration:SXLoadingTime];
    } animated:NO withView:nil];
    
}

#pragma mark - 确认支付订单请求
//支付宝支付
- (void)orderSurePayActionRequest:(ZOrderModel *)order withButton:(UIButton *)button {
    
    [ZPayMoney payWithOrderNumber:order.ID title:[NSString stringWithFormat:@"%@商品结算",simpleTitle] price:order.orderAllPrice complete:^{
        
        //清除遮罩视图
        [messageBGView removeFromSuperview];
        
        //调用后推到首页
        [self.navigationController popToRootViewControllerAnimated:false];
        self.tabBarController.selectedIndex = 0;
        
    }];

    
    
    
}

//银联支付
- (void)orderSureUnionPayActionRequest:(ZOrderModel *)order withButton:(UIButton *)button {
    
    //清除遮罩视图
    [messageBGView removeFromSuperview];
    
    [ZPayMoney UnionPayWithOrderNumber:order.ID price:order.orderAllPrice withVc:self complete:^{
        //调用后推到首页
//        [self.navigationController popToRootViewControllerAnimated:false];
//        self.tabBarController.selectedIndex = 0;
        
    }];
}

//微信支付
- (void)orderSureWXPayActionRequest:(ZOrderModel *)order withButton:(UIButton *)button {
   
    [ZPayMoney weiXinPayWithOrderNumber:order.ID title:[NSString stringWithFormat:@"%@商品结算",simpleTitle] price:order.orderAllPrice complete:^{
        //
        //清除遮罩视图
        [messageBGView removeFromSuperview];
        
        //调用后推到首页
        [self.navigationController popToRootViewControllerAnimated:false];
        self.tabBarController.selectedIndex = 0;
    }];
    
    
    
    
}




#pragma mark - 确认收货请求
- (void)orderSureReceiveActionRequest:(ZOrderModel *)order withButton:(UIButton *)button {
    if (order == nil) {
        [SXLoadingView showAlertHUD:@"该订单数据异常" duration:SXLoadingTime];
        return;
    }
    //115.159.146.202/api/api.php?action=confirm&orders_num=
    NSDictionary *params = @{@"orders_num":order.ID,@"token":[UdStorage getObjectforKey:Userid]};
    
    [ZHttpRequestService POST:@"Order/confirm" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [SXLoadingView showAlertHUD:@"订单确认收货" duration:2];
            [_orderTableView reloadData];
        }else{
            [SXLoadingView showAlertHUD:jsonDic[@"message"] duration:1.5];
        }
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"请求失败" duration:SXLoadingTime];
    } animated:NO withView:nil];

    
    
}


#pragma mark - 查看物流请求
- (void)orderDeliveryInfoActionRequest:(ZOrderModel *)order withButton:(UIButton *)button {

    [SXLoadingView showAlertHUD:@"开发中！" duration:1.5];
    return;
    
    //115.159.146.202/api/api.php?action=kuaidi&orders_num=147412496110000232
     NSDictionary *params = @{@"orders_num":order.orderID};
    [ZHttpRequestService POST:@"kuaidi" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        NSString *deliveryUrlStr = nil;
        
        if (succe) {
            if ([jsonDic[@"data"] isKindOfClass:[NSNull class]]) {
                NSLog(@"无数据情况");
            }else{
                NSDictionary *dic = jsonDic[@"data"][0];
                NSString *str = [Utils getTextStrByText:dic[@"kuaidi_num"]];
                deliveryUrlStr = [NSString stringWithFormat:@"%@%@",OrderDeliveryUrl,str] ;
                
                NSLog(@"%@",deliveryUrlStr);
            }
            
            //跳转web浏览页面
            //NSString *urlStr = @"http://www.zzumall.com/kuaidi/index.php?num=2222222222";
            [self showOrderDeliveryMessageViewWithUrl:deliveryUrlStr];
         
        }
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"请求失败" duration:SXLoadingTime];
    } animated:NO withView:nil];
}



#pragma mark - 申请退货请求
- (void)goodsReturnsActionRequest:(ZOrderModel *)order withButton:(UIButton *)button {
    
    //弹出框 并判断退货原因不能为空
    [self showGoodsReturnsMessageView];
    
    //115.159.146.202/api/api.php?action=tuihuo&orders_num=147412496110000232&tuihuo_case=
    NSDictionary *params = @{@"token":[UdStorage getObjectforKey:Userid],
                             @"order_id":order.ID,
                             @"tuihuo_case":_returnsCaseStr};
    [ZHttpRequestService POST:@"Order/tuihuo" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [SXLoadingView showAlertHUD:@"退货成功" duration:2];
            [_orderTableView reloadData];
          }
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"请求失败" duration:SXLoadingTime];
    } animated:NO withView:nil];

}


#pragma mark - 选择支付方式提示信息框
- (void)showChoosePayTypeMessageView {
    //1 弹出框背景
    messageBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-64)];
    messageBGView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
    [self.view addSubview:messageBGView];
    
    //2 菜单
    UIView *message = [[UIView alloc] initWithFrame:CGRectMake((CGRectW(messageBGView)-260)/2, (CGRectH(messageBGView)-180)/2, 260, 180)];
    [messageBGView addSubview:message];
    message.backgroundColor = HEXCOLOR(0xffffff);
    
    
    //titleLb
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(message)-100)/2, 10, 100, 30)];
    [message addSubview:titleLb];
    titleLb.font = MFont(15);
    titleLb.textColor = HEXCOLOR(0x333333);
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.text = @"选择支付方式";
    
    //closeBtn
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(message)-30, 10, 20, 20)];
    [message addSubview:closeBtn];
    [closeBtn setImage:MImage(@"close") forState:BtnNormal];
    [closeBtn addTarget:self action:@selector(orderDeliveryMessageViewCloseAction) forControlEvents:BtnTouchUpInside];
    
    //line
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectYH(titleLb), CGRectW(message), 1)];
    [message addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    
    //银联支付
    UIButton *unionPayButton = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectH(message)-60, 70, 30)];
    [message addSubview:unionPayButton];
    unionPayButton.backgroundColor = __TestOColor;
    unionPayButton.titleLabel.font = MFont(12);
    [unionPayButton setTitle:@"银联支付" forState:BtnNormal];
    [unionPayButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [unionPayButton addTarget:self action:@selector(chooseUnionPayAction) forControlEvents:BtnTouchUpInside];
    
    UIImageView *unionIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectX(unionPayButton)+15, CGRectH(message)-105, 40, 40)];
    [message addSubview:unionIV];
    unionIV.image = MImage(@"icon_uppay.png");
    

    
    
    
    //微信支付
    UIButton *wxPayButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(unionPayButton)+15, CGRectH(message)-60, 70, 30)];
    [message addSubview:wxPayButton];
    wxPayButton.backgroundColor = __DefaultColor;
    wxPayButton.titleLabel.font = MFont(12);
    [wxPayButton setTitle:@"微信支付" forState:BtnNormal];
    [wxPayButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [wxPayButton addTarget:self action:@selector(chooseWXPayAction) forControlEvents:BtnTouchUpInside];
    
    UIImageView *wxIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectX(wxPayButton)+20, CGRectH(message)-100, 30, 30)];
    [message addSubview:wxIV];
    wxIV.image = MImage(@"weixin.png");
    
    
    
    //支付宝支付
    UIButton *payButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(message)-80), CGRectH(message)-60, 70, 30)];
    [message addSubview:payButton];
    payButton.backgroundColor = HEXCOLOR(0x55bce1);
    payButton.titleLabel.font = MFont(12);
    [payButton setTitle:@"支付宝" forState:BtnNormal];
    [payButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [payButton addTarget:self action:@selector(choosePayAction) forControlEvents:BtnTouchUpInside];
    
    UIImageView *aliIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectX(payButton)+20, CGRectH(message)-100, 30, 30)];
    [message addSubview:aliIV];
    aliIV.image = MImage(@"zhifubao.png");
    

}

- (void)choosePayAction {
    NSLog(@"支付宝支付");
    
    [self orderSurePayActionRequest:_currenOrder withButton:_currentButton];
    
}

- (void)chooseUnionPayAction {
    NSLog(@"银联支付");
    [self orderSureUnionPayActionRequest:_currenOrder withButton:_currentButton];
    
}

- (void)chooseWXPayAction {
    NSLog(@"微信支付");
    [self orderSureWXPayActionRequest:_currenOrder withButton:_currentButton];
    
}

#pragma mark - 退货申请提示信息框
- (void)showGoodsReturnsMessageView {
    //1 弹出框背景
    messageBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-64)];
    messageBGView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
    [self.view addSubview:messageBGView];
    
    //2 菜单
    messageView = [[UIView alloc] initWithFrame:CGRectMake((CGRectW(messageBGView)-260)/2, 66, 260, 280)];
    [messageBGView addSubview:messageView];
    messageView.backgroundColor = HEXCOLOR(0xf0f0f0);
    
    
    //titleLb
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(messageView)-100)/2, 10, 100, 30)];
    [messageView addSubview:titleLb];
    titleLb.font = MFont(15);
    titleLb.textColor = HEXCOLOR(0x333333);
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.text = @"退货原因";
    
    caseText = [[UITextView alloc] initWithFrame:CGRectMake(10, CGRectYH(titleLb)+5, CGRectW(messageView)-20, 190)];
    [messageView addSubview:caseText];
    caseText.backgroundColor = HEXCOLOR(0xffffff);
    caseText.textColor = HEXCOLOR(0x333333);
    caseText.font = MFont(14);
    caseText.delegate = self;
    
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(30, CGRectH(messageView)-40, 80, 30)];
    [messageView addSubview:cancelButton];
    cancelButton.backgroundColor = HEXCOLOR(0x55bce1);
    cancelButton.titleLabel.font = MFont(12);
    [cancelButton setTitle:@"取消" forState:BtnNormal];
    [cancelButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [cancelButton addTarget:self action:@selector(goodsReturnMessageCancelAction) forControlEvents:BtnTouchUpInside];
    
    UIButton *sureButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(messageView)-110), CGRectH(messageView)-40, 80, 30)];
    [messageView addSubview:sureButton];
    sureButton.backgroundColor = __DefaultColor;
    sureButton.titleLabel.font = MFont(12);
    [sureButton setTitle:@"提交" forState:BtnNormal];
    [sureButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [sureButton addTarget:self action:@selector(goodsReturnMessageSureAction) forControlEvents:BtnTouchUpInside];
}

- (void)goodsReturnMessageCancelAction {
    NSLog(@"退货提示信息取消");
    //清除遮罩视图
    [messageBGView removeFromSuperview];
}

- (void)goodsReturnMessageSureAction {
    NSLog(@"退货提示信息确认");
    _returnsCaseStr = caseText.text;
    if (IsNilString(_returnsCaseStr)) {
        [SXLoadingView showAlertHUD:@"请输入退货原因" duration:SXLoadingTime];
        return;
    }
    //清除遮罩视图
    [messageBGView removeFromSuperview];
}



#pragma mark - 退货申请提示信息框
- (void)showOrderDeliveryMessageViewWithUrl:(NSString *)urlStr {
    
    //1 弹出框背景
    messageBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, __kHeight)];
    messageBGView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
    [self.view addSubview:messageBGView];
    
    //2 菜单
    UIView *message = [[UIView alloc] initWithFrame:CGRectMake(10, (CGRectH(messageBGView)-280)/2, CGRectW(messageBGView)-20, 280)];
    [messageBGView addSubview:message];
    message.backgroundColor = HEXCOLOR(0xffffff);
    //message.layer.cornerRadius = 5.0f;
    
    //titleView
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(message), 30)];
    [message addSubview:titleView];
    titleView.backgroundColor = HEXCOLOR(0xf0f0f0);
    
    //titleLb
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 100, 30)];
    [titleView addSubview:titleLb];
    titleLb.font = MFont(14);
    titleLb.textColor = HEXCOLOR(0x333333);
    titleLb.text = @"查看物流信息";
    
    //closeBtn
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(titleView)-30, 5, 20, 20)];
    [titleView addSubview:closeBtn];
    [closeBtn setImage:MImage(@"close") forState:BtnNormal];
    [closeBtn addTarget:self action:@selector(orderDeliveryMessageViewCloseAction) forControlEvents:BtnTouchUpInside];
    
    if (IsNilString(urlStr)) {
        UILabel *deliveryInfoLb = [[UILabel alloc] initWithFrame:CGRectMake(20, 60, 150, 30)];
        [message addSubview:deliveryInfoLb];
        deliveryInfoLb.font = MFont(17);
        deliveryInfoLb.textColor = HEXCOLOR(0x333333);
        //deliveryInfoLb.textAlignment = NSTextAlignmentCenter;
        deliveryInfoLb.text = @"目前暂无相关信息";
    }else{
        UIWebView *web = [[UIWebView alloc] initWithFrame:CGRectMake(0, 30, CGRectW(message), CGRectH(message)-30)];
        [message addSubview:web];
        //web.backgroundColor = __TestGColor;
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        [web loadRequest:request];
    }
    
    
    
    
}

- (void)orderDeliveryMessageViewCloseAction {
    NSLog(@"关闭提示框");
    
    //清除遮罩视图
    [messageBGView removeFromSuperview];
}

#pragma mark - ===== 刷新控件设置内容 =====
#pragma mark - 上拉加载更多
- (void)collectionViewFootLoadAction {
    
    [self loadMoreGoodsData];
}


//加载更多商品
- (void)loadMoreGoodsData {
    NSLog(@"商品 评价页面 加载更多");
    [_orderTableView footerEndRefreshing];
    if (_currentDataArray.count == 10) {
        _currentPage++;
        [self myOrdersDataRequestWithPageNum:_currentPage];
    }else{

        _orderTableView.footerHidden = YES;
        _orderTableView.tableFooterView.hidden = NO;
    }
}


#pragma mark - 下拉刷新数据
- (void)refreshGoodsData {
    NSLog(@"商品 简介页面 下拉刷新");
    [_orderTableView headerEndRefreshing];
    _orderTableView.footerHidden = NO;
    _orderTableView.tableFooterView.hidden = YES;
    //初始化 数据
    _currentPage = 1;
    [_ordersArray removeAllObjects];
    [self myOrdersDataRequestWithPageNum:_currentPage];
}


#pragma mark - textView Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (textView) {
        if (__kHeight<__k6Height) {
            
            [UIView animateWithDuration:0.4 animations:^{
                messageBGView.frame = CGRectMake(0, -76, __kWidth, __kHeight);
            }];
        }
    }
    
    return YES;
}


- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if (__kHeight<__k6Height) {
        
        [UIView animateWithDuration:0.4 animations:^{
            messageBGView.frame = CGRectMake(0, 66, __kWidth, __kHeight);
        }];
    }
    
    
    return YES;

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (caseText) {
        [self.view endEditing:YES];
    }
}


#pragma mark - ===== 个人中心选择项目 ====
- (void)fromCenterPageChooseNum:(NSInteger)chooseNum {
    NSLog(@"选择当前按钮tag:%d",chooseNum);
    
    for (UIButton *button in _btnArr) {
        if (button.tag == chooseNum) {
            
            button.selected = YES;
            continue;
        } else
            
            button.selected = NO;
        
    }
    
    switch (chooseNum - 130) {
        case 0:
        {
            NSLog(@"全部");
            //重新初始化 数据
            _currentPage = 1;
            
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/orderList";//全部订单
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
        }
            break;
        case 1:
        {
            NSLog(@"待付款");
            //重新初始化 数据
            _currentPage = 1;
            
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/payment_list";//待支付订单
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
            
        }
            break;
        case 2:
        {
            NSLog(@"待收货");
            //重新初始化 数据
            _currentPage = 1;
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/receipt_goods"; //待收货订单
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
        }
            break;
            
        case 3:
        {
            NSLog(@"待评价");
            //重新初始化 数据
            _currentPage = 1;
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/payments_waite"; //待评价商品
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
        }
            break;
        case 4:
        {
            NSLog(@"退货或换货");
            //重新初始化 数据
            _currentPage = 1;
            [_ordersArray removeAllObjects];
            
            _actionStr = @"Order/payments_ok"; //待完成
            //订单数据请求
            [self myOrdersDataRequestWithPageNum:_currentPage];
        }
            break;

        default:
            break;
    }

    
}

#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark *** 支付 ***
//支付宝支付
-(void)alipayWithDic:(NSString *)orderStr{
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = AlipayScheme;
    
    NSLog(@"orderstr---%@",orderStr);
    //获取的orderstr直接调用支付宝
    [[AlipaySDK defaultService] payOrder:orderStr fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
        
        //调用后推到首页
        [self.navigationController popToRootViewControllerAnimated:false];
        self.tabBarController.selectedIndex = 0;
        
    }];
    
}

//微信支付
- (NSString *)jumpToBizPay {
    
    NSLog(@"weixingxin");
    
    if (![WXApi isWXAppInstalled]) {
        [SXLoadingView showAlertHUD:@"未安装微信" duration:0.5];
        return @"未安装微信";
    }
    
    if(![WXApi isWXAppSupportApi]){
        [SXLoadingView showAlertHUD:@"该版本不支持微信支付" duration:0.5];
        return @"该版本不支持微信支付";
    }
    
    //============================================================
    // V3&V4支付流程实现
    // 注意:参数配置请查看服务器端Demo
    // 更新时间：2015年11月20日
    //============================================================
    NSString *urlString   = @"http://wxpay.weixin.qq.com/pub_v2/app/app_pay.php?plat=ios";
    //解析服务端返回json数据
    NSError *error;
    //加载一个NSURL对象
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    //将请求的url数据放到NSData对象中
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    if ( response != nil) {
        NSMutableDictionary *dict = NULL;
        //IOS5自带解析类NSJSONSerialization从response中解析出数据放到字典中
        dict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:&error];
        
        NSLog(@"url:%@",urlString);
        if(dict != nil){
            NSMutableString *retcode = [dict objectForKey:@"retcode"];
            if (retcode.intValue == 0){
                NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
                
                //调起微信支付
                PayReq* req             = [[PayReq alloc] init];
                req.partnerId           = [dict objectForKey:@"partnerid"];
                req.prepayId            = [dict objectForKey:@"prepayid"];
                req.nonceStr            = [dict objectForKey:@"noncestr"];
                req.timeStamp           = stamp.intValue;
                req.package             = [dict objectForKey:@"package"];
                req.sign                = [dict objectForKey:@"sign"];
                [WXApi sendReq:req];
                //日志输出
                NSLog(@"appid=%@\npartid=%@\nprepayid=%@\nnoncestr=%@\ntimestamp=%ld\npackage=%@\nsign=%@",[dict objectForKey:@"appid"],req.partnerId,req.prepayId,req.nonceStr,(long)req.timeStamp,req.package,req.sign );
                return @"";
            }else{
                return [dict objectForKey:@"retmsg"];
            }
        }else{
            return @"服务器返回错误，未获取到json对象";
        }
    }else{
        return @"服务器返回错误";
    }
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
