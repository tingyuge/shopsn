//
//  ZSearchHeader.m
//  shopSN
//
//  Created by yisu on 16/6/29.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZSearchHeader.h"

@interface ZSearchHeader ()



/** 搜索异常 顶部提示内容 */
@property (nonatomic, strong) UILabel *bottomLb;

@end

@implementation ZSearchHeader

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //self.backgroundColor = __TestGColor;
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        [self addSubview:bgView];
        
        
        
        [self initSubViews:bgView];
    }
    
    return self;
}

- (void)initSubViews:(UIView *)view {
    //上边线
    UIImageView *topIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), 1)];
    topIV.backgroundColor = HEXCOLOR(0xdedede);
    [view addSubview:topIV];
    
    
    
    //topLb
    _topLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, CGRectW(view)-20, 40)];
    //_topLb.backgroundColor = __TestOColor;
    _topLb.font = MFont(15);
    _topLb.textColor = HEXCOLOR(0x999999);
    _topLb.numberOfLines = 0;
    [view addSubview:_topLb];
    
    
    //下边线 110改为100
    UIImageView *bottomIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, CGRectH(view)-1, CGRectW(view)-10, 1)];
    bottomIV.backgroundColor = HEXCOLOR(0xdedede);
    [view addSubview:bottomIV];
    
    //middleView
//    UIView *middleView = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectYH(_topLb), CGRectW(view)-16, 70)];
//    //middleView.backgroundColor = __TestOColor;
//    [view addSubview:middleView];
//    //[self middleViewAddSubView:middleView];
    
    
    //bottomLb
//    _bottomLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(bottomIV)+5, CGRectW(view)-20, 20)];
//    //_bottomLb.backgroundColor = __TestOColor;
//    _bottomLb.font = MFont(12);
//    _bottomLb.textColor = HEXCOLOR(0x999999);
//    [view addSubview:_bottomLb];
    
    
    //测试文字
    _topLb.text    = @"亲，“雷logo过…”暂无搜索结果。您是不是要搜：";
    _bottomLb.text = @"我们为您推荐了“抗过敏”：";
    
    
}

//- (void)middleViewAddSubView:(UIView *)view {
//    //测试 抗过敏 过敏 防过敏 耳钉防过敏 高跟过膝长靴
//    NSArray *btnArray = @[@"抗过敏", @"过敏", @"防过敏", @"耳钉防过敏", @"高跟过膝长靴"];
//    
//    for (int i=0; i<btnArray.count; i++) {
//        
//        NSString *name = btnArray[i];
//        static UIButton *recordBtn =  nil;
//        UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
//        CGRect rect = [name boundingRectWithSize:CGSizeMake(CGRectW(view)-20, 25) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:btn.titleLabel.font} context:nil];
//        if (i == 0)
//        {//第一行 第一个按钮
//            btn.frame =CGRectMake(0, 5, rect.size.width+16, rect.size.height+5);//调整宽度 +16  高度+5
//        }
//        else{//其他按钮
//            CGFloat yuWidth = CGRectW(view) - 25-recordBtn.frame.origin.x -recordBtn.frame.size.width;
//            if (yuWidth >= rect.size.width) {//第一排 后面按钮
//                btn.frame =CGRectMake(recordBtn.frame.origin.x +recordBtn.frame.size.width + 10, recordBtn.frame.origin.y, rect.size.width+16, rect.size.height+5);
//            }else{//第二排高度调整 +5
//                btn.frame =CGRectMake(0, recordBtn.frame.origin.y+recordBtn.frame.size.height+10, rect.size.width, rect.size.height+5);
//            }
//            
//        }
//        //btn.backgroundColor = [UIColor yellowColor];
//        btn.layer.borderWidth = 1.0;
//        btn.layer.borderColor = __DefaultColor.CGColor;
//        btn.titleLabel.font = MFont(12);
//        [btn setTitle:name forState:BtnNormal];
//        [btn setTitleColor:__DefaultColor forState:BtnNormal];
//        [view addSubview:btn];
//        
//        recordBtn = btn;
//        
//    }
//}


@end
