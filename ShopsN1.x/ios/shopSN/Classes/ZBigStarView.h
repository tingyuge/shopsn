//
//  ZBigStarView.h
//  shopSN
//
//  Created by yisu on 16/8/3.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品评价页面
 *
 *  星级(大尺寸) 视图
 *
 */
#import "BaseView.h"

@interface ZBigStarView : BaseView

/** 星星(大尺寸) imageView */
@property (nonatomic, strong) UIImageView *starIV;

/** 星星(大尺寸) image */
@property (nonatomic, strong) UIImage *starImage;

/** 设置评价星级 图片 */
-(void)setStarIV:(UIImageView *)starIV;

/** 星级视图 设置星 数量*/
-(void)setStar:(NSInteger)star;



@end
