//
//  ZCAccountBankCardViewController.m
//  shopSN
//
//  Created by chang on 16/7/10.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCAccountBankCardViewController.h"
#import "ZCAccountBundleBankCardViewController.h" //绑定银行卡 页面

@interface BankCell:UITableViewCell
/**所属银行*/
@property (nonatomic,strong) UILabel *cellBank;
/**卡类型*/
@property (nonatomic,strong) UILabel *cellType;
/**银行卡号*/
@property (nonatomic,strong) UILabel *cellCardnum;

@end

@implementation BankCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = __AccountBGColor;
        
        UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(30, 10, __kWidth-60, 70)];
        whiteView.backgroundColor = [UIColor whiteColor];
        whiteView.layer.cornerRadius = 10;
        
        [self.contentView addSubview:whiteView];
        [self.contentView addSubview:self.cellBank];
        [self.contentView addSubview:self.cellType];
        [self.contentView addSubview:self.cellCardnum];
    }
    return self;
}
#pragma mark *** getters ***
-(UILabel *)cellBank{
    if (!_cellBank) {
        _cellBank = [[UILabel alloc] initWithFrame:CGRectMake(50, 16, self.bounds.size.width, 20)];
        _cellBank.text = @"111";
        _cellBank.font = MFont(14);
    }
    return _cellBank;
}
-(UILabel *)cellType{
    if (!_cellType) {
        _cellType = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(self.cellBank), CGRectYH(self.cellBank), self.cellBank.bounds.size.width, self.cellBank.bounds.size.height)];
        _cellType.text = @"222";
        _cellType.font = MFont(14);
    }
    return _cellType;
}
-(UILabel *)cellCardnum{
    if (!_cellCardnum) {
        _cellCardnum = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(self.cellBank), CGRectYH(self.cellType), self.cellBank.bounds.size.width, self.cellBank.bounds.size.height)];
        _cellCardnum.text = @"333";
        _cellCardnum.font = MFont(14);
    }
    return _cellCardnum;
}
@end

@interface ZCAccountBankCardViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *_dataSoruce;
}
/**表格*/
@property (nonatomic,strong) UITableView *tableView;
/**添加银行卡*/
@property (nonatomic,strong) UIButton *addBtn;

@end

@implementation ZCAccountBankCardViewController

#pragma mark - ==== 页面设置 =====
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLb.text = @"我的银行卡";
    _dataSoruce = @[];
    //初始化 中间视图 相关子视图
    self.mainMiddleView.backgroundColor = __AccountBGColor;
    [self initSubViews:self.mainMiddleView];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getCardData];
}
-(void)getCardData{
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid]};
    WK(weakSelf);
    [ZHttpRequestService POST:@"User/personBankcard" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
             NSLog(@"jsondic---%@",jsonDic[@"data"]);
            _dataSoruce = jsonDic[@"data"];
            [weakSelf.tableView reloadData];
            NSInteger heightCount = _dataSoruce.count>7?7:_dataSoruce.count;
            
            weakSelf.tableView.frame = CGRectMake(0, 64, __kWidth, heightCount*80);
            weakSelf.addBtn.frame = CGRectMake(10, CGRectYH(self.tableView)+30, __kWidth-20, 56);
            
            
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:0.5];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:0.5];
        
    } animated:true withView:nil];
}

-(void)reloadAllView{
    
}

#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    
    //背景
//    UIView *subBGView = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, CGRectW(view), 180)];
//    [view addSubview:subBGView];
//    subBGView.backgroundColor = HEXCOLOR(0xffffff);
    
    //添加银行卡 按钮视图
//    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectYH(self.tableView)+30, CGRectW(view)-20, 56)];
//    [subBGView addSubview:btnView];
    //等待背景图
//    btnView.backgroundColor = __TestOColor;
//    btnView.layer.borderWidth = 1.0f;
//    btnView.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    // label color(0 143 255) (0x008fff)  文字 + 添加银行卡
//    UILabel *btnLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, CGRectW(btnView)-20, 50)];
//    [btnView addSubview:btnLabel];
//    btnLabel.font = MFont(<#font#>)
//    btnLabel.textColor = HEXCOLOR(0x008fff);
//    btnLabel.textAlignment = NSTextAlignmentCenter;
//    btnLabel.text = @"+ 添加银行卡";
    
    UIButton *addBankCardBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectYH(self.tableView)+30, CGRectW(view)-20, 56)];
    [addBankCardBtn setTitle:@"+ 添加银行卡" forState:0];
    [addBankCardBtn setTitleColor:HEXCOLOR(0x008fff) forState:0];
    addBankCardBtn.layer.borderWidth = 1.0f;
    addBankCardBtn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;

    [addBankCardBtn addTarget:self action:@selector(addBankCardBtnAction) forControlEvents:BtnTouchUpInside];
    self.addBtn = addBankCardBtn;
    [self.view addSubview:self.tableView];
    [self.view addSubview:addBankCardBtn];
}
#pragma mark *** tableviewdelegate ***
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataSoruce.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BankCell *cell = [tableView dequeueReusableCellWithIdentifier:@"bankCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[BankCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"bankCell"];
    }
    NSDictionary *dic = _dataSoruce[indexPath.row];
    cell.cellBank.text = dic[@"belong"];
    cell.cellType.text = dic[@"type"];
    cell.cellCardnum.text = dic[@"card_num"];
    return cell;
}
#pragma mark - ==== 按钮触发方法 =====
- (void)addBankCardBtnAction {
    NSLog(@"进入 绑定银行卡页面");
    ZCAccountBundleBankCardViewController *vc = [[ZCAccountBundleBankCardViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UITableView *)tableView{
    if (!_tableView) {
        
        NSInteger heightCount = _dataSoruce.count>7?7:_dataSoruce.count;
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, heightCount*80)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[BankCell class] forCellReuseIdentifier:@"bankCell"];
        _tableView.rowHeight = 80;
        _tableView.backgroundColor = self.view.backgroundColor;
    }
    return _tableView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
