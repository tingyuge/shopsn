//
//  UIImage+imageTool.m
//  shopSN
//
//  Created by yisu on 16/6/12.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "UIImage+imageTool.h"

@implementation UIImage (imageTool)

+ (instancetype)imageWithOriginalName:(NSString *)imageName {
    
    UIImage *image = [UIImage imageNamed:imageName];
    return [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
}

@end
