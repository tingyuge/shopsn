//
//  ZRetrievePasswordViewController.h
//  shopSN
//
//  Created by yisu on 16/6/13.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseViewController.h"

@interface ZRetrievePasswordViewController : BaseViewController

/** 手机号 Str */
@property (copy, nonatomic) NSString *phoneStr;

///** 用户id Str */
//@property (copy, nonatomic) NSString *userIDStr;

@end
