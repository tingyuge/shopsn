//
//  ZHttpRequestService.m
//  shopSN
//
//  Created by yisu on 16/9/6.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHttpRequestService.h"

@implementation ZHttpRequestService

+(void)GET:(NSString *)actionParameter success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi;
{
    
    NSString *path = [TestRootURL stringByAppendingString:[NSString stringWithFormat:@"/api.php?action=%@",actionParameter]];


    
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    //    NSLog(@"url---%@",path);
    [manager GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        NSError *error=nil;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        //        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        
        success(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];
}


+ (void)GETHome:(NSString *)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi {
    
    NSString *path = [RootURL stringByAppendingString:actionParameter];
    

    
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
        NSLog(@"url---%@     %@",path,params);
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        NSError *error=nil;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        
        success(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];
}



+ (void)POST:(NSString*)actionParameter withParameters:(NSDictionary*)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi
{
    
    NSString *path = [RootURL stringByAppendingString:actionParameter];
    
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    NSLog(@"url---%@     %@",path,params);
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        NSError *error;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        success(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];
}


+ (void)POSTGoods:(NSString *)actionParameter withParameters:(NSDictionary *)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi {
    
    NSString *path = [RootURL stringByAppendingString:actionParameter];
    
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    NSLog(@"url---%@     %@",path,params);
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        NSError *error;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        success(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];
    
}


+ (void)POSTTour:(NSString *)actionParameter withParameters:(NSDictionary *)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi {
    
    NSString *path = [TestRootURL stringByAppendingString:[NSString stringWithFormat:@"/tour.php?action=%@",actionParameter]];
    
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    NSLog(@"url---%@     %@",path,params);
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        NSError *error;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        success(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];
}


+ (void)POSTHome:(NSString *)actionParameter withParameters:(NSDictionary *)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi {
    NSString *path = [TestRootURL stringByAppendingString:[NSString stringWithFormat:@"/home.php?action=%@",actionParameter]];
    
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    NSLog(@"url---%@     %@",path,params);
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        NSError *error;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        success(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];

}

//GET goods.php请求
+ (void)GETGoods:(NSString *)actionParameter withParameters:(NSDictionary *)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi {
    
    NSString *path = [TestRootURL stringByAppendingString:[NSString stringWithFormat:@"/goods.php?action=%@",actionParameter]];
    
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    NSLog(@"url---%@",path);
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        NSError *error=nil;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        
        success(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];
    
}

//GET tour.php请求
+ (void)GETTour:(NSString *)actionParameter withParameters:(NSDictionary *)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi {
    
    NSString *path = [TestRootURL stringByAppendingString:[NSString stringWithFormat:@"/tour.php?action=%@",actionParameter]];
    
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    NSLog(@"url---%@",path);
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        NSError *error=nil;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        
        success(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];
}

//GET aip.php 请求
+ (void)GETApi:(NSString *)actionParameter withParameters:(NSDictionary *)params success:(SuccessCallBack)success failure:(FailureCallBack)fail animated:(BOOL)animated withView:(UIView *)vi {
    
    NSString *path = [TestRootURL stringByAppendingString:[NSString stringWithFormat:@"/api.php?action=%@",actionParameter]];
    
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    //manager.requestSerializer = [AFHTTPRequestSerializer serializer];//方式2
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    NSLog(@"url---%@",path);
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        NSError *error=nil;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        success(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
            [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];
    
}
//上传图片
+(void)POST:(NSString *)path Params:(NSDictionary *)params NSData:(NSData *)imageData key:(NSString *)name success:(SuccessCallBack)suuccess failure:(FailureCallBack)fail animated:(BOOL)animated{
    if (animated) {
        [SXLoadingView showProgressHUD:@""];
    }
    NSString *path1 = [NSString stringWithFormat:@"%@%@",RootURL,path];
    AFHTTPRequestOperationManager *manager =[AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSLog(@"url---%@   %@",path,params);
    [manager POST:path1 parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSString *file=[NSString stringWithFormat:@"%@.jpg",imageData];
        [formData appendPartWithFileData:imageData name:name fileName:file mimeType:@"image/png"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",dic);
        BOOL succe=NO;
        NSString *code=[NSString stringWithFormat:@"%@",dic[@"code"]];
        if (IsEquallString(code, @"200")) {
            succe=YES;
        }
        suuccess(responseObject,succe,dic);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (animated) {
           [SXLoadingView hideProgressHUD];
        }
        fail(error);
    }];

}

@end
