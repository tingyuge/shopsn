//
//  ZChangePersonIDViewController.m
//  shopSN
//
//  Created by imac on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZChangePersonIDViewController.h"

@interface ZChangePersonIDViewController ()<UITextFieldDelegate>

@property (strong,nonatomic) UITextField *personIdTF;

@end

@implementation ZChangePersonIDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.titleLb.text = @"修改身份证号";
    self.title = @"修改身份证号";
    [self initView];
}
-(void)initView{
    
    //背景视图
    UIView *backV = [[UIView alloc]initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-64)];
    [self.view addSubview:backV];
    backV.backgroundColor = __AccountBGColor;
    
    UIView *mainV = [[UIView alloc]initWithFrame:CGRectMake(0, 8, __kWidth, 44)];
    [backV addSubview:mainV];
    mainV.backgroundColor = [UIColor whiteColor];
    
    //文本框
    _personIdTF = [[UITextField alloc]initWithFrame:CGRectMake(15, 0, __kWidth-30, 44)];
    [mainV addSubview:_personIdTF];
    _personIdTF.delegate = self;
    _personIdTF.backgroundColor = [UIColor clearColor];
    if (!IsNilString(_personID)) {
        _personIdTF.text =[NSString stringWithFormat:@"%@",_personID];
    }else{
        _personIdTF.text =@"暂无";
    }
    _personIdTF.font = MFont(12);
    _personIdTF.textColor = HEXCOLOR(0x333333);
    _personIdTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    //说明 label
    UILabel *noteLb = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectYH(mainV), __kWidth-30, 30)];
    [backV addSubview:noteLb];
    noteLb.textAlignment = NSTextAlignmentLeft;
    noteLb.backgroundColor = [UIColor clearColor];
    noteLb.textColor= HEXCOLOR(0x999999);
    noteLb.font = MFont(12);
    noteLb.text = @"15或18位数字组成";
    
    //确定 按钮
    UIButton *saveBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, CGRectYH(noteLb)+10, __kWidth-30, 40)];
    [backV addSubview:saveBtn];
    saveBtn.backgroundColor    = __DefaultColor;
    saveBtn.layer.cornerRadius = 4.0f;
    saveBtn.titleLabel.font    = MFont(15);
    [saveBtn setTitle:@"确定" forState:BtnNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:BtnNormal];
    [saveBtn addTarget:self action:@selector(saveAction) forControlEvents:BtnTouchUpInside];
}

-(void)saveAction{
    NSLog(@"保存修改身份证号");
    if (IsNilString(_personIdTF.text)) {
        [SXLoadingView showAlertHUD:@"身份证不可为空" duration:1.5];
    }
    NSDictionary *params =@{@"idcard":_personIdTF.text,@"token":[UdStorage getObjectforKey:Userid]};
    
    [ZHttpRequestService POST:@"User/changeIdCard" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            NSLog(@"xiugai--%@",jsonDic);
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
            
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];
}
#pragma mark -UITextFiled Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
