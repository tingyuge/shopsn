//
//  ZCenterShareViewController.m
//  shopSN
//
//  Created by chang on 16/7/9.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterShareViewController.h"
#import "ZCenterMyQRCodeViewController.h"//我的二维码名片页面

#import "ZCenterShareCell.h"        //会员id 推广三维码 cell
#import "ZCenterShareDomainCell.h"  //推广域名 cell
#import "ZCenterShareQRCoderCell.h" //推广二维码 cell


#import "UMSocial.h"

@interface ZCenterShareViewController ()<UITableViewDataSource, UITableViewDelegate>

{
    UITableView *_tableView;
    NSString *_shareUrlStr;
}

/**
 *  图片数组
 */
@property (nonatomic ,strong)NSArray *iconArray;

/**
 *  标题数组
 */
@property (nonatomic ,strong)NSArray *titleArray;
/**
 *  详情数组
 */
@property (nonatomic ,strong)NSArray *detailArray;


/**二维码图*/
@property (nonatomic,strong) UIImage *myImage;

@end

@implementation ZCenterShareViewController
#pragma mark - ==== 懒加载 =====
/*
 会员ID 推广域名  推广二维码  推广三维码
 89565646  www.zzy.com
 */
- (NSArray *)iconArray {
    if (!_iconArray) {
        _iconArray = [NSArray array];
        _iconArray = @[@"ID.png", @"tg.png", @"ewm.png", @"swm.png"];
    }
    return _iconArray;
}

- (NSArray *)titleArray {
    if (!_titleArray) {
        _titleArray = [NSArray array];
        _titleArray = @[@"会员ID", @"推广域名", @"推广二维码"];
    }
    return _titleArray;
}

- (NSArray *)detailArray {
    if (!_detailArray) {
        _detailArray = [NSArray array];
        _detailArray = @[_memberId, @"", @"", @""];
    }
    return _detailArray;
}


#pragma mark - ==== 页面设置 =====
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"分享%@",simpleTitle];
#warning 分享链接
//    _shareUrlStr = [NSString stringWithFormat:@"http://www.zzumall.com/index.php/Mobile/Myzzy/web/id/%@",[UdStorage getObjectforKey:Userid]];
    _shareUrlStr = ShareURL;
    
    
    //视图 初始化
    [self initViews];
    
    
}



//视图 初始化
- (void)initViews {
    
    
    //顶部边线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, 1)];
    [self.view addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 65, __kWidth, __kHeight-65)];
    [self.view addSubview:_tableView];
    _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
    
}

#pragma mark- ===== tableView DataSource and Delegate =====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     //*** 重写cell
    if ((indexPath.row == 0)||(indexPath.row == 3)) {
        ZCenterShareCell *cell = [[ZCenterShareCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
//        
//        //推广三维码
//        if (indexPath.row == 3) {
//            //隐藏 4 行内容 替换为 右箭头
//            cell.cs_detailLb.hidden = YES;
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        }

        
        cell.cs_iconIV.image = MImage(self.iconArray[indexPath.row]);
        cell.cs_titleLb.text = self.titleArray[indexPath.row];
        cell.cs_detailLb.text = self.detailArray[indexPath.row];
        return cell;
    }
    
    else {
        //推广域名
        if (indexPath.row == 1) {
            ZCenterShareDomainCell *cell = [[ZCenterShareDomainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.cs_iconIV.image = MImage(self.iconArray[indexPath.row]);
            cell.cs_titleLb.text = self.titleArray[indexPath.row];
            cell.cs_shareUrlLabel.text = _shareUrlStr;
            [cell.cs_shareButton addTarget:self action:@selector(shareUrlButtonAction) forControlEvents:BtnTouchUpInside];
            
            return cell;
        }
        //推广二维码
        else {
            ZCenterShareQRCoderCell *cell = [[ZCenterShareQRCoderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.cs_iconIV.image = MImage(self.iconArray[indexPath.row]);
            cell.cs_titleLb.text = self.titleArray[indexPath.row];
            self.myImage = cell.cs_ewIV.image;
            return cell;
        }

    }
   
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1 ) {
        return 154;
    }
    else if (indexPath.row == 2) {
        return 174;
    }
    return 44;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 2) {
        NSLog(@"进入 我的二维码页面");
//        ZCenterMyQRCodeViewController *vc = [[ZCenterMyQRCodeViewController alloc] init];
//        //test
//        //vc.qrCodeStr = @"http://blog.yourtion.com";
//        vc.qrCodeStr = @"https://www.baidu.com";
//        vc.nickNameStr =[NSString stringWithFormat:@"%@昵称",simpleTitle] ;
//        vc.qrCodeView = [[UIImageView alloc] init];
//        vc.qrCodeView.image = self.myImage;
//        [self.navigationController pushViewController:vc animated:YES];
    }
    

    
}

- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}

#pragma mark - 分享域名
- (void)shareUrlButtonAction {
    NSLog(@"分享域名");
    
    [UMSocialData defaultData].extConfig.title =[NSString stringWithFormat:@"%@-域名分享",simpleTitle] ;
    
    //微信好友 朋友圈 分享
    [UMSocialData defaultData].extConfig.wechatSessionData.url = _shareUrlStr;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = _shareUrlStr;
    
    //qq好友 qq空间 分享
    [UMSocialData defaultData].extConfig.qqData.url = _shareUrlStr;
    [UMSocialData defaultData].extConfig.qzoneData.url = _shareUrlStr;
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:_shareUrlStr];
    
    
    
    [UMSocialSnsService presentSnsIconSheetView:self appKey:UMKey shareText:[NSString stringWithFormat:@"%@-域名分享",simpleTitle] shareImage:nil shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQQ,UMShareToQzone] delegate:nil];
    
    
}

#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
