//
//  ZGoodsListSetionView.m
//  shopSN
//
//  Created by chang on 16/6/26.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZGoodsListSetionView.h"

@interface ZGoodsListSetionView ()
{
    NSMutableArray *_btnArr;
    
    
    BOOL isLowPrices;// 价格排序 YES（低到高）  NO（高到低）
    
   
    
}


@end



@implementation ZGoodsListSetionView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self) {
        self = [super initWithFrame:frame];
        self.backgroundColor = HEXCOLOR(0xffffff);
        
        _btnArr = [NSMutableArray array];
        
        //添加上下边线
        for (int i=0; i<2; i++) {
            UIImageView *linIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, i*39, self.frame.size.width, 1)];
            linIV.backgroundColor = HEXCOLOR(0xdedede);
            [self addSubview:linIV];
        }
        
        //添加功能选择按钮
        NSArray *titles = @[@"综合",@"价格",@"销量",@"筛选"];
        for (int i=0; i<4; i++) {
            [self initScreenButtonWith:CGRectMake(i*CGRectW(self)/4, 0, CGRectW(self)/4, 40) withIndex:i withTitle:titles[i]];
            
        }
        
    }
    return self;
}


- (void)initScreenButtonWith:(CGRect)frame withIndex:(NSInteger)index withTitle:(NSString *)title {
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    [self addSubview:button];
    [_btnArr addObject:button];
    //button.backgroundColor = HEXCOLOR(0xdedede);
    //button.titleLabel.backgroundColor = __TestGColor;
    //button.imageView.backgroundColor = __TestOColor;
    
    //*** 为后面布局效果 必须放置一个图片上去 否则在后面在加图片会有布局问题

    if (index == 1) {//价格  按钮
        [button setImage:MImage(@"jiantou_d") forState:BtnNormal];
        button.layer.masksToBounds = YES;
    }

    
    if (index == 3) {//筛选 按钮
        [button setImage:MImage(@"jiantou03") forState:BtnNormal];
        [button setImage:MImage(@"jiantou04") forState:BtnStateSelected];
        button.layer.masksToBounds = YES;
    }
    
    if (!index) {
        button.selected = YES;
    }
    
    
    [button setTitle:title forState:BtnNormal];
    [button setTitleColor:HEXCOLOR(0x3e3e3e) forState:BtnNormal];
    [button setTitleColor:__DefaultColor forState:BtnStateSelected];
    
    button.titleLabel.font = MFont(14);
    
    //标题和图片布局 *** i5距离稍宽 i6距离合适
    [button setTitleEdgeInsets:UIEdgeInsetsMake ( 0 , -button. imageView . frame . size . width , 0 , button. imageView . frame . size . width )];
    [button setImageEdgeInsets:UIEdgeInsetsMake ( 0 , button. titleLabel . frame . size . width + 30 , 0 , 0 )];

    
    button.tag = 40+index;
    [button addTarget:self action:@selector(screenBtnAciton:) forControlEvents:BtnTouchUpInside];
}


-(void)screenBtnAciton:(UIButton *)sender {
    sender.selected = YES;
    BOOL flag;
    flag = sender.selected;
    
    for (UIButton *button in _btnArr) {
        if (button == sender) {
            
            continue;
        } else
            
            button.selected = NO;
        
    }
    
    if (sender.tag == 41) {//价格
        flag = isLowPrices;
        if (isLowPrices) {
            
            [sender setImage:MImage(@"jiantou02") forState:BtnStateSelected];
            [sender setImage:MImage(@"jiantou_d") forState:BtnNormal];
            
            NSLog(@"按价格从高到低排列");
            isLowPrices = NO;
            
        }else {
            [sender setImage:MImage(@"jiantou01") forState:BtnStateSelected];
            [sender setImage:MImage(@"jiantou_d") forState:BtnNormal];
            
            NSLog(@"按价格从低到高排列");
            
            isLowPrices = YES;
        }
    }else if (sender.tag == 42) {//销量
        NSLog(@"按销量从高到低 排列");
 
        
    }else { //筛选功能
        if (sender.tag == 43) {
            NSLog(@"展开筛选视图");
            [self.delegate showSiftView:sender];
        }
        
        
        
        isLowPrices = NO;
    }
    

}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
