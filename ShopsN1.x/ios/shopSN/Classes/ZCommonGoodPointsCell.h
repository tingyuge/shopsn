//
//  ZCommonGoodPointsCell.h
//  shopSN
//
//  Created by yisu on 16/9/13.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 普通商品 简介页面
 *
 *   积分 库存内容 cell
 *
 */

#import "BaseTableViewCell.h"

@interface ZCommonGoodPointsCell : BaseTableViewCell

/** 商品简介页面 积分label */
@property (nonatomic, strong) UILabel *pointLb;

/** 商品简介页面 库存label */
@property (nonatomic, strong) UILabel *stockLb;

/** 商品简介页面 加载积分和库存数据 */
- (void)setGoodsIntrPoints:(ZGoodsDeal *)deal;




@end
