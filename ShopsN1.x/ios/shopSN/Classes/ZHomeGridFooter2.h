//
//  ZHomeGridFooter2.h
//  shopSN
//
//  Created by yisu on 16/8/11.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 collectionView
 *
 *  会员旅游展示 footer
 *
 */

#import <UIKit/UIKit.h>


@interface ZHomeGridFooter2 : UICollectionReusableView


/** 商品展示 footer 视图 */
@property (nonatomic, strong) UIView *commonGoodsFooterView;


/** 商品展示 footer center图片 */
@property (nonatomic, strong) UIImageView *commonGoodsFooterCenterIV;

@end
