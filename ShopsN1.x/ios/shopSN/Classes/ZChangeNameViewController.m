//
//  ZChangeNameViewController.m
//  shopSN
//
//  Created by imac on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZChangeNameViewController.h"

@interface ZChangeNameViewController ()<UITextFieldDelegate>

@property (strong,nonatomic) UITextField *nameTF;

@end

@implementation ZChangeNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.titleLb.text = @"修改姓名";
    self.title = @"修改姓名";
    [self initView];
}
-(void)initView{
    
    //背景视图
    UIView *backV = [[UIView alloc]initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-64)];
    [self.view addSubview:backV];
//    backV.backgroundColor = LH_RGBCOLOR(230, 230, 230);
    backV.backgroundColor = __AccountBGColor;
    
    
    UIView *mainV = [[UIView alloc]initWithFrame:CGRectMake(0, 8, __kWidth, 44)];
    [backV addSubview:mainV];
    mainV.backgroundColor = [UIColor whiteColor];
    
    //文本框
    _nameTF = [[UITextField alloc]initWithFrame:CGRectMake(15, 0, __kWidth-30, 44)];
    [mainV addSubview:_nameTF];
    _nameTF.delegate = self;
    _nameTF.backgroundColor = [UIColor clearColor];
    if (!IsNilString(_realName)) {
        _nameTF.text =[NSString stringWithFormat:@"%@",_realName];
    }else{
    _nameTF.text =@"暂无";
    }
    _nameTF.font = MFont(12);
    _nameTF.textColor = HEXCOLOR(0x333333);
    _nameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    //说明 label
    UILabel *noteLb = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectYH(mainV), __kWidth-30, 30)];
    [backV addSubview:noteLb];
    noteLb.textAlignment = NSTextAlignmentLeft;
    noteLb.backgroundColor = [UIColor clearColor];
    noteLb.textColor= HEXCOLOR(0x999999);
    noteLb.font = MFont(12);
    noteLb.text = @"4-20个字符，可由中英文、“_”、“-”组成";
    
    //确定 按钮
    UIButton *saveBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, CGRectYH(noteLb)+10, __kWidth-30, 40)];
    [backV addSubview:saveBtn];
    saveBtn.backgroundColor    = __DefaultColor;
    saveBtn.layer.cornerRadius = 4.0f;
    saveBtn.titleLabel.font    = MFont(15);
    [saveBtn setTitle:@"确定" forState:BtnNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:BtnNormal];
    [saveBtn addTarget:self action:@selector(saveAction) forControlEvents:BtnTouchUpInside];
}

-(void)saveAction{
    NSLog(@"保存修改姓名");
    NSDictionary *params =@{@"realname":_nameTF.text,@"token":[UdStorage getObjectforKey:Userid]};
    
    [ZHttpRequestService POST:@"User/changeName" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            NSLog(@"xiugai--%@",jsonDic);
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
            
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];
    
}

#pragma mark -UITextFiled Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
