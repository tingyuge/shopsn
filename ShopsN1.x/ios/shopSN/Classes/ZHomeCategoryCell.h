//
//  ZHomeCategoryCell.h
//  shopSN
//
//  Created by yisu on 16/8/13.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 collectionView
 *
 *  顶部 分类选择 cell
 *
 */
#import <UIKit/UIKit.h>

@interface ZHomeCategoryCell : UICollectionViewCell

/** 分类选择中的 imageView */
@property (nonatomic, strong) UIImageView *categoryIV;
/** 分类选择中的 Label */
@property (nonatomic, strong) UILabel *categoryLb;

/** 设置类别选择参数 */
//- (void)setCategoryInfo:(ZCategory *)category;



@end
