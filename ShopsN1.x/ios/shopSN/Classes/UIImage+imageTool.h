//
//  UIImage+imageTool.h
//  shopSN
//
//  Created by yisu on 16/6/12.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (imageTool)

+ (instancetype)imageWithOriginalName:(NSString *)imageName;

@end
