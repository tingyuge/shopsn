//
//  ZHomeGridHeader3.m
//  shopSN
//
//  Created by yisu on 16/8/19.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeGridHeader3.h"

@implementation ZHomeGridHeader3

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:bgView];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        
        //标题
        //_recommendTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 100, 20)];
        _recommendTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, (CGRectH(bgView)-20)/2, 100, 20)];
        [bgView addSubview:_recommendTitleLb];
//    _recommendTitleLb.backgroundColor = __TestGColor;
        _recommendTitleLb.font = MFont(14);
        _recommendTitleLb.textColor = HEXCOLOR(0x333333);

        //更多
        _moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(__kWidth-70, (CGRectH(bgView)-20)/2, 70, 20)];
        [bgView addSubview:_moreBtn];
        _moreBtn.titleLabel.font = MFont(14);
        [_moreBtn setTitle:@"更多>" forState:BtnNormal];
        [_moreBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
        [_moreBtn addTarget:self action:@selector(find:) forControlEvents:BtnTouchUpInside];
        
        //底线
        UIImageView *linIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-1, CGRectW(bgView), 1)];
        linIV.backgroundColor = HEXCOLOR(0xdedede);
        [bgView addSubview:linIV];
        
        
    }
    return self;
    
}

-(void)find:(UIButton*)sender{
    [self.delegate lookmore:sender];
}


@end
