//
//  ZCategoryViewController.m
//  shopSN
//
//  Created by yisu on 16/6/12.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCategoryViewController.h"
#import "ZGoodsSearchViewController.h"//商品搜索
#import "ZGoodsListViewController.h" //商品列表

#import "ZListForGoodsViewController.h"//会员商品列表


//subViews
#import "ZCategoryView.h"//页面 分类视图




@interface ZCategoryViewController ()<ZCategoryViewDelegate, UITextFieldDelegate>
{
    UIView *topMenuView;   //顶部交互视图
    UIView *menuBtnView;   //按钮视图
    UITextField *searchTF;
    //UIView *bottomView;    //底部视图
    //ZCategoryView *categoryView;//分类视图
}


//
/** 分类页面 中间主视图 */
@property (nonatomic ,strong) ZCategoryView *categoryView;
//

@end

@implementation ZCategoryViewController


#pragma mark - ==== 页面设置 =====

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.tabBar.hidden = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //test
    //self.view.backgroundColor = [UIColor greenColor];
    
    
    //[self initNavi];// 自定义导航栏
    
    //[self initMainView];//初始化主页面视图
    
    [self initSubViews:self.mainMiddleView];
    
    
    
}



#pragma mark- 自定义导航栏中间内容
- (void)addNaviSubControllers:(UIView *)middleView {
    
    //middleView.backgroundColor = __TestGColor;
    
    //中间搜索框
    UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(middleView), CGRectH(middleView))];
    searchView.layer.borderWidth = 1;
    searchView.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    searchView.backgroundColor = HEXCOLOR(0xffffff);
    searchView.layer.cornerRadius = 5;
    [middleView addSubview:searchView];
    [self createSearchSubView:searchView];
    
    
}


//自定义 搜索视图 内部控件
- (void)createSearchSubView:(UIView *)btnView {
    
    
    //searchIV
    UIImageView *searchIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 7.5, 15, 15)];
    searchIV.image = MImage(@"sousuo");
    //searchIV.backgroundColor = __TestOColor;
    [btnView addSubview:searchIV];
    
    
    //searchTF
    searchTF = [[UITextField alloc] initWithFrame:CGRectMake(CGRectXW(searchIV)+5, 5, CGRectW(btnView)-CGRectW(searchIV)-25, CGRectH(btnView)-10)];
    [btnView addSubview:searchTF];
    //searchTF.backgroundColor = __TestGColor;
    searchTF.delegate = self;
    searchTF.font = MFont(14);
    searchTF.textColor = HEXCOLOR(0x333333);
    searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;//小叉子
    searchTF.keyboardType = UIKeyboardTypeWebSearch;//web搜索模式
    searchTF.placeholder = @"搜索内容";
    
}

- (void)getDataWithCategoryArray:(NSArray *)categoryArray andRowTitleArray:(NSArray *)rowTitleArray {
    
    _categoryArr = categoryArray;
    _rowTitleArr = rowTitleArray;
    
    [_categoryView removeFromSuperview];
    
    [self initSubViews:self.mainMiddleView];
}

//中间主视图内子控件初始化
- (void)initSubViews:(UIView *)view {
    
    //分类视图
    _categoryView = [[ZCategoryView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), CGRectH(view)-50)];
    [view addSubview:_categoryView];
    
    _categoryView.delegate = self;
    
    _categoryView.isGoods = _isGoods;
    
    [_categoryView getDataWithCategoryArray:_categoryArr andRowTitleArray:_rowTitleArr];
    
    [_categoryView defaultChoose:_categoryView.categoryArr];
    
}





- (void)pushToGoodsListVCwithSubCategoryID:(NSString *)subCategoryID andSubCategoryName:(NSString *)subCategoryName {
    
    
    
    
    if (_isGoods) {
        //会员商品
        NSLog(@"跳转会员商品列表信息: id=%@ name=%@",subCategoryID, subCategoryName);
        ZListForGoodsViewController *goodsVC = [[ZListForGoodsViewController alloc] init];
        goodsVC.subCategoryID = subCategoryID;
        goodsVC.subCategoryName = subCategoryName;
        [self.navigationController pushViewController:goodsVC animated:YES];
        
    }    
    
}






#pragma mark - ===== TextFiled Delegate =====
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (IsNilString(textField.text)) {
        [SXLoadingView showAlertHUD:@"请输入搜索内容" duration:SXLoadingTime];
    }else {
        ZGoodsSearchViewController *vc = [[ZGoodsSearchViewController alloc] init];
        vc.searchStr = textField.text;
        vc.isGoods = _isGoods;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
    [textField resignFirstResponder];
    return YES;
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (searchTF) {
        [self.view endEditing:YES];
    }
}






#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
