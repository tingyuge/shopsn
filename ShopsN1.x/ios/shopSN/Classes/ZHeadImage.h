//
//  ZHeadImage.h
//  shopSN
//
//  Created by yisu on 16/6/15.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 Header视图控制器
 *
 *  图片模型
 *
 */

#import <Foundation/Foundation.h>

@interface ZHeadImage : NSObject
/** headerScrollView 图片名称 */
@property (nonatomic, copy) NSString *imageName;
/** headerScrollView 图片url */
@property (nonatomic, copy) NSString *imageUrl;


@end
