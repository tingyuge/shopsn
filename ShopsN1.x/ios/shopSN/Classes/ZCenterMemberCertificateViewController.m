//
//  ZCenterMemberCertificateViewController.m
//  shopSN
//
//  Created by yisu on 16/7/15.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterMemberCertificateViewController.h"
//#import "ZCMemberCertificateSubViewController.h"//内页 页面
//
//#import "ZCenterPrivilegeCell.h" //cell
//#import "ZCMemberCertificateButtonSubView.h" //顶部视图中的按钮视图

#import "ZYSLoginViewController.h"//亿速登录
#import "WXApi.h"
#import "ZPayMoney.h"

@interface ZCenterMemberCertificateViewController ()
{
    NSInteger userCertificate;        //用户等级
    NSInteger privilegeNum;           //特权数量
    UICollectionView *_privilegeListCollectionView;//特权项目列表
    
//    ZCMemberCertificateButtonSubView *btnViewV1; //游客
//    ZCMemberCertificateButtonSubView *btnViewV2; //会员
//    ZCMemberCertificateButtonSubView *btnViewV3; //事业合伙人
    
    
    UIView *messageBGView;//弹出框背景
    NSString *_payOrderName;//支付订单名称
    NSString *_payOrderID;  //支付订单单号
    NSString *_payOrderType; //支付订单方式
    NSString *_payOrderPrice;//支付订单金额
    
}

@property (nonatomic ,copy) NSString *chooseTitle;
@property (nonatomic ,copy) NSString *chooseDetail;

@end

@implementation ZCenterMemberCertificateViewController
#pragma mark - ==== 页面设置 =====
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.titleLb.text = @"shopSN互联网信息股份有限公司";
    
    //test
    privilegeNum = 12;
    
    //判断账户资格
    if ([[UdStorage getObjectforKey:UserType] isEqualToString:@"游客"]) {
        [self initVisitorUI];//游客身份
    }else{
        [self getMemberData];//会员、合伙人身份
    }
    
    
    //test
    //[self initVisitorUI];//游客身份
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.tabBar.hidden = NO;
}

-(void)getMemberData{
    WK(weakSelf)
    NSDictionary *params =@{@"id":[UdStorage getObjectforKey:Userid]};
    
    [ZHttpRequestService POST:@"vip" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
            NSLog(@"jsondic---%@",jsonDic[@"data"]);
            [weakSelf initUIWith:jsonDic[@"data"]];
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];

}

#pragma mark - 会员资格 视图初始化
-(void)initUIWith:(id)dic{
    
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, __kWidth, 75)];
    headLabel.text = MainTitle;
    headLabel.textColor = [UIColor whiteColor];
    headLabel.textAlignment = 1;
    headLabel.backgroundColor = LH_RGBCOLOR(20, 175, 15);
    headLabel.font = MFont(20);
    [self.view addSubview:headLabel];
    
    
    UIImageView *logoImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, __kHeight/4, __kWidth, 75)];
    logoImage.image = MImage(@"logo");
    logoImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:logoImage];
    
    
    NSArray *arr=@[dic[0][@"grade_name"],dic[0][@"vip_end"]];
    
    for (int idx = 0; idx<arr.count; idx++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, __kHeight/2+20*idx, __kWidth, 20)];
        label.font = MFont(18);
        label.textAlignment = 1;
        [self.view addSubview:label];
        label.text = [NSString stringWithFormat:@"您已成为 %@",arr[0]];
        if (idx==1) {
            NSInteger  timeValue = [arr[1] integerValue];
            NSDate *data = [NSDate dateWithTimeIntervalSince1970:timeValue];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            
            label.text = [NSString stringWithFormat:@"会员到期时间：%@",[NSString stringWithFormat:@"%@",[formatter stringFromDate:data]]];
            label.font = BFont(15);
            
        }
    }
   
}


#pragma mark - 游客资格 视图初始化
////初始化相关子视图
- (void)initVisitorUI {
    
    UILabel *headLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, __kWidth, 64)];

    headLabel.text = [NSString stringWithFormat:@"%@会员",simpleTitle];
    headLabel.textColor = [UIColor whiteColor];
    headLabel.textAlignment = 1;
    headLabel.backgroundColor = LH_RGBCOLOR(254, 194, 49);
    headLabel.font = MFont(20);
    [self.view addSubview:headLabel];
    
    
    //消费说明
    UIView *saleView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(headLabel), __kWidth, (CGRectH(self.view)-64)/5)];
    [self.view addSubview:saleView];
    [self addSaleSubViews:saleView];//addSubviews;
    
    
    //积分奖励说明
    UIView *pointView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(saleView), __kWidth, (CGRectH(self.view)-64)/5)];
    [self.view addSubview:pointView];
    [self addPointSubViews:pointView];//addSubviews;
    //pointView.backgroundColor = __TestOColor;
    
    
    //注释词
    UIView *noteView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(pointView), __kWidth, (CGRectH(self.view)-64)/5)];
    [self.view addSubview:noteView];
    [self addNoteViewSubViews:noteView];//addSubviews;
    //noteView.backgroundColor = __TestGColor;
    
    //按钮视图
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(noteView), __kWidth, (CGRectH(self.view)-64)/5)];
    [self.view addSubview:buttonView];
    [self addButtonViewSubViews:buttonView];//addSubviews;
    //buttonView.backgroundColor = __TestOColor;
    
    
    
}



- (void)addSaleSubViews:(UIView *)view {
    UILabel *saleLb1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)/5, 10, CGRectW(view)-20, 20)];
    [view addSubview:saleLb1];
    saleLb1.font = MFont(15);
    //saleLb1.textAlignment = NSTextAlignmentCenter;
    saleLb1.textColor = HEXCOLOR(0x333333);
    saleLb1.text = @"1、会员个人消费送积分:";
    
    
    UILabel *saleLb2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)/5+10, CGRectYH(saleLb1), CGRectW(view)-20, 20)];
    [view addSubview:saleLb2];
    saleLb2.font = MFont(15);
    //saleLb2.textAlignment = NSTextAlignmentCenter;
    saleLb2.textColor = HEXCOLOR(0x333333);
    saleLb2.text = @"a.会员个人网上购物送积分";
    
    
    UILabel *saleLb3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)/5+10, CGRectYH(saleLb2), CGRectW(view)-20, 20)];
    [view addSubview:saleLb3];
    saleLb3.font = MFont(15);
    //saleLb3.textAlignment = NSTextAlignmentCenter;
    saleLb3.textColor = HEXCOLOR(0x333333);
    saleLb3.text = @"b.会员个人旅游送积分";
    
    
    
    
    
}

- (void)addPointSubViews:(UIView *)view {
    UILabel *pointLb1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)/5, 10, CGRectW(view)-20, 20)];
    [view addSubview:pointLb1];
    pointLb1.font = MFont(15);
    pointLb1.textColor = HEXCOLOR(0x333333);
    pointLb1.text = [NSString stringWithFormat:@"2、会员分享%@积分奖励:",simpleTitle];
    
    
    UILabel *pointLb2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)/5+10, CGRectYH(pointLb1), CGRectW(view)-20, 20)];
    [view addSubview:pointLb2];
    pointLb2.font = MFont(15);
    pointLb2.textColor = HEXCOLOR(0x333333);
    pointLb2.text = [NSString stringWithFormat:@"a.会员%@推荐会员积分奖励",simpleTitle];
    
    
    UILabel *pointLb3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)/5+10, CGRectYH(pointLb2), 210, 40)];
    [view addSubview:pointLb3];
    pointLb3.font = MFont(15);
    //pointLb3.textAlignment = NSTextAlignmentCenter;
    pointLb3.numberOfLines = 0;
    pointLb3.textColor = HEXCOLOR(0x333333);
    pointLb3.text = [NSString stringWithFormat:@"b.会员%@推荐会员购物和旅游积分奖励",simpleTitle];
    
    
    
}


- (void)addNoteViewSubViews:(UIView *)view {
    UILabel *noteLb1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, CGRectW(view)-20, 40)];
    [view addSubview:noteLb1];
    noteLb1.font = MFont(18);
    noteLb1.textAlignment = NSTextAlignmentCenter;
    noteLb1.textColor = __DefaultColor;
    noteLb1.text = @"分享创业，分享积分，分享快乐！";
    
    
    UILabel *noteLb2 = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(noteLb1)+20, CGRectW(view)-20, 20)];
    [view addSubview:noteLb2];
    noteLb2.font = MFont(18);
    noteLb2.textAlignment = NSTextAlignmentCenter;
    noteLb2.textColor = __DefaultColor;
    noteLb2.text = @"365元/年 1天一块钱";
}


- (void)addButtonViewSubViews:(UIView *)view {
    //
    UIButton *joinButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(view)-200)/2, 5, 200, 30)];
    [view addSubview:joinButton];
    joinButton.titleLabel.font = MFont(13);
    joinButton.backgroundColor = LH_RGBCOLOR(0, 176, 18);
    joinButton.layer.cornerRadius = 5;
    [joinButton setTitle:@"立即加入" forState:BtnNormal];
    [joinButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [joinButton addTarget:self action:@selector(joinButtonAction) forControlEvents:BtnTouchUpInside];
    
    
    UIButton *logoutButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(view)-200)/2, CGRectYH(joinButton)+40, 200, 30)];
    [view addSubview:logoutButton];
    logoutButton.titleLabel.font = MFont(13);
    logoutButton.backgroundColor = LH_RGBCOLOR(0, 176, 18);
    logoutButton.layer.cornerRadius = 2;
    [logoutButton setTitle:@"注销登录" forState:BtnNormal];
    [logoutButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [logoutButton addTarget:self action:@selector(logoutButtonAction) forControlEvents:BtnTouchUpInside];
    
}

- (void)joinButtonAction {
    NSLog(@"立即加入");
    
    //判断购买资格
    [self checkUserTypeRequest];
    
}

- (void)logoutButtonAction {
    NSLog(@"注销登录");
    
    //清除记录
    NSString *nilStr = nil;
    [UdStorage storageObject:nilStr forKey:Userid];
    [UdStorage storageObject:nilStr forKey:UserType];
    [UdStorage storageObject:nilStr forKey:UserPhone];
    
    
    
    //暂时注销
//    ZLoginViewController *vc = [[ZLoginViewController alloc] init];
    ZYSLoginViewController *vc = [[ZYSLoginViewController alloc]init];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}



#pragma mark - 购买资格判断请求
- (void)checkUserTypeRequest {
    NSLog(@"判断用户是否具备购买会员资格");
    
    //115.159.146.202/api/api.php?action=can_buy_vip&id=10000285
    //NSString *userID = @"10000285";//test
    NSString *userID = [UdStorage getObjectforKey:Userid];//loadData
    
    
    [ZHttpRequestService POST:@"can_buy_vip" withParameters:@{@"id":userID} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        NSString *code= [NSString stringWithFormat:@"%@",jsonDic[@"code"]];
        NSString *message= [NSString stringWithFormat:@"%@",jsonDic[@"message"]];
        NSLog(@"code:%@  message:%@",code, message);
        
       //具备购买资格
        if ([code isEqualToString:@"200"] ||[code isEqualToString:@"201"]) {
            [self showMessageViewWithCode:code];
        }
        //无法购买
        if ([code isEqualToString:@"41"] ||[code isEqualToString:@"42"]) {
            [SXLoadingView showAlertHUD:message duration:SXLoadingTime];
        }
        
        //[SXLoadingView showAlertHUD:message duration:SXLoadingTime];
        
    } failure:^(NSError *error) {
        //
    } animated:NO withView:nil];
    
    
    
    //测试
//    NSString *codeStr = @"200";//或201
//    [self showMessageViewWithCode:codeStr];
}



#pragma mark - 信息提示框
//具备购买资格 弹出改提示框
- (void)showMessageViewWithCode:(NSString *)code {
    
    //115.159.146.202/api/api.php?action=can_buy_Vip&id=10000285
    
    NSLog(@"显示信息提示");
    
    
    //1 弹出框背景
    messageBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, __kHeight)];
    messageBGView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
    [self.view addSubview:messageBGView];
    
    
    //2 菜单
    UIView *messageView = [[UIView alloc] initWithFrame:CGRectMake((CGRectW(messageBGView)-260)/2, (CGRectH(messageBGView)-120)/2, 260, 120)];
    [messageBGView addSubview:messageView];
    messageView.backgroundColor = HEXCOLOR(0xffffff);
    //messageView.layer.cornerRadius = 5.0f;
    
    //titleView
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(messageView), 30)];
    [messageView addSubview:titleView];
    titleView.backgroundColor = HEXCOLOR(0xf1f1f1);
    
    //titleLb
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
    [titleView addSubview:titleLb];
    titleLb.font = MFont(12);
    titleLb.textColor = HEXCOLOR(0x333333);
    titleLb.text = @"信息";
    
    //cancelBtn
    UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(titleView)-30, 5, 20, 20)];
    [titleView addSubview:cancelBtn];
    [cancelBtn setImage:MImage(@"close") forState:BtnNormal];
    [cancelBtn addTarget:self action:@selector(payMessageViewCloseAction) forControlEvents:BtnTouchUpInside];
    
    //descView
    UIView *descView = [[UIView alloc] initWithFrame:CGRectMake((CGRectW(messageView)-220)/2, CGRectYH(titleView)+5, 220, 20)];
    [messageView addSubview:descView];
    //descView.backgroundColor = __TestOColor;
    
    
    //iconIV
//    UIImageView *iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
//    [descView addSubview:iconIV];
//    iconIV.contentMode = UIViewContentModeScaleAspectFit;
//    iconIV.image = MImage(@"jz.png");
    
    //descLb
    //UILabel *descLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(iconIV)+10, 5, 182, 15)];
    UILabel *descLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(descView)-160)/2, 10, 160, 15)];
    [descView addSubview:descLb];
    //descLb.backgroundColor = __TestGColor;
    descLb.font = MFont(13);
    descLb.textColor = HEXCOLOR(0x333333);
    descLb.textAlignment = NSTextAlignmentCenter;
    descLb.text = @"请选择您购买的项目内容";
    
    //messageBtn color(85 188 225)(0x55bce1)
    
    
    if ([code isEqualToString:@"201"]) {
        UIButton *vipButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(messageView)-80)/2, CGRectH(messageView)-40, 80, 30)];
        [messageView addSubview:vipButton];
        vipButton.backgroundColor = HEXCOLOR(0x55bce1);
        vipButton.titleLabel.font = MFont(12);
        [vipButton setTitle:@"会员" forState:BtnNormal];
        [vipButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
        [vipButton addTarget:self action:@selector(sureBuyVipAciton) forControlEvents:BtnTouchUpInside];
    }else {
        UIButton *vipButton1 = [[UIButton alloc] initWithFrame:CGRectMake(30, CGRectH(messageView)-40, 80, 30)];
        [messageView addSubview:vipButton1];
        vipButton1.backgroundColor = HEXCOLOR(0x55bce1);
        vipButton1.titleLabel.font = MFont(12);
        [vipButton1 setTitle:@"会员" forState:BtnNormal];
        [vipButton1 setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
        [vipButton1 addTarget:self action:@selector(sureBuyVipAciton) forControlEvents:BtnTouchUpInside];
        
        UIButton *vipButton2 = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(messageView)-110), CGRectH(messageView)-40, 80, 30)];
        [messageView addSubview:vipButton2];
        vipButton2.backgroundColor = __DefaultColor;
        vipButton2.titleLabel.font = MFont(12);
        [vipButton2 setTitle:@"合伙人" forState:BtnNormal];
        [vipButton2 setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
        [vipButton2 addTarget:self action:@selector(sureBuyVip2Aciton) forControlEvents:BtnTouchUpInside];
    }
    
   
    
    
}


//信息提示框取消按钮触发方法
- (void)messageCancelAction {
    NSLog(@"取消信息提示");
    //清除遮罩视图
    [messageBGView removeFromSuperview];
}

#pragma mark - ===== 购买会员 =====
- (void)sureBuyVipAciton {
    
    NSLog(@"选择 购买会员");
    _payOrderName = @"购买会员";

    
    //购买请求
    _payOrderPrice = @"365";
    [self toBuyVipRequestWithPrice:_payOrderPrice];
    
    
    //测试
//    //清除遮罩视图
//    [messageBGView removeFromSuperview];
//    
//    _payOrderID = @"hui1475775665876431";
//    //选择支付方式
//    [self showChoosePayTypeMessageView];
    
}




#pragma mark - ===== 购买事业合伙人 =====
- (void)sureBuyVip2Aciton {
    
    
    NSLog(@"选择 购买事业合伙人");
    _payOrderName = @"购买事业合伙人";
    
    _payOrderPrice = @"30000";
    [self toBuyVipRequestWithPrice:_payOrderPrice];
}



#pragma mark - 创建购买会员或合伙人订单
- (void)toBuyVipRequestWithPrice:(NSString *)price {
    NSLog(@"购买会员资格费用:%@",price);
    
    //清除遮罩视图
    [messageBGView removeFromSuperview];
    
    //创建购买会员订单
    //115.159.146.202/api/api.php?action=create_buy_vip_order&id=10000285&money=365
    //NSString *userID = @"10000285";//test
    NSString *userID = [UdStorage getObjectforKey:Userid];//loadData
    NSDictionary *params =@{@"id":userID,
                            @"money":price};
    
    [ZHttpRequestService POST:@"create_buy_vip_order" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
         
        //订单创建成功
        if (succe) {
            
            //进行支付
            _payOrderID = [Utils getTextStrByText:jsonDic[@"data"][@"orders_num"]];
            NSLog(@"orderID:%@",_payOrderID);
            
            
            
            //选择支付方式
            [self showChoosePayTypeMessageView];
            
            
        }else {
            NSString *code= [NSString stringWithFormat:@"%@",jsonDic[@"code"]];
            NSString *message= [NSString stringWithFormat:@"%@",jsonDic[@"message"]];
            NSLog(@"code:%@  message:%@",code, message);
            [SXLoadingView showAlertHUD:message duration:SXLoadingTime];
        }
        
       
        
    } failure:^(NSError *error) {
        //
    } animated:NO withView:nil];

    
    
}


#pragma mark - 选择支付方式提示信息框
- (void)showChoosePayTypeMessageView {
    
    
    
    //1 弹出框背景
    messageBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-64)];
    messageBGView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
    [self.view addSubview:messageBGView];
    
    //2 菜单
    UIView *message = [[UIView alloc] initWithFrame:CGRectMake((CGRectW(messageBGView)-260)/2, (CGRectH(messageBGView)-180)/2, 260, 180)];
    [messageBGView addSubview:message];
    message.backgroundColor = HEXCOLOR(0xffffff);
    
    
    //titleLb
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(message)-100)/2, 5, 100, 30)];
    [message addSubview:titleLb];
    titleLb.font = MFont(15);
    titleLb.textColor = HEXCOLOR(0x333333);
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.text = @"选择支付方式";
    
    //closeBtn
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(message)-30, 10, 20, 20)];
    [message addSubview:closeBtn];
    [closeBtn setImage:MImage(@"close") forState:BtnNormal];
    [closeBtn addTarget:self action:@selector(payMessageViewCloseAction) forControlEvents:BtnTouchUpInside];
    
    //line
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectYH(titleLb), CGRectW(message), 1)];
    [message addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    
    //支付信息
    UILabel *orderNameLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(lineIV)+5, CGRectW(message)-20, 20)];
    [message addSubview:orderNameLb];
    //orderNameLb.backgroundColor = __TestGColor;
    orderNameLb.font = MFont(13);
    orderNameLb.text = [NSString stringWithFormat:@"订单名称：%@",_payOrderName];
    
    UILabel *orderIDLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(orderNameLb), CGRectW(message)-20, 20)];
    [message addSubview:orderIDLb];
    //orderIDLb.backgroundColor = __TestOColor;
    orderIDLb.font = MFont(13);
    orderIDLb.text = [NSString stringWithFormat:@"订单编号：%@",_payOrderID];
    
    UILabel *orderPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(orderIDLb), CGRectW(message)-20, 20)];
    [message addSubview:orderPriceLb];
    //orderPriceLb.backgroundColor = __TestGColor;
    orderPriceLb.font = MFont(13);
    orderPriceLb.text = [NSString stringWithFormat:@"订单金额：￥%@.00",_payOrderPrice];
    [self setLabeltextAttributes:orderPriceLb];
  
    
    
    
    /**
     *支付按钮
     */
    
    //银联支付
    UIButton *unionPayButton = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectH(message)-40, 70, 30)];
    [message addSubview:unionPayButton];
    unionPayButton.backgroundColor = __TestOColor;
    unionPayButton.titleLabel.font = MFont(12);
    [unionPayButton setTitle:@"银联支付" forState:BtnNormal];
    [unionPayButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [unionPayButton addTarget:self action:@selector(chooseUnoinPayAction) forControlEvents:BtnTouchUpInside];
    
    UIImageView *unionIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectX(unionPayButton)+20, CGRectH(message)-68, 30, 30)];
    [message addSubview:unionIV];
    unionIV.image = MImage(@"icon_uppay.png");
    

    
    //微信支付
    UIButton *wxPayButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(unionPayButton)+15, CGRectH(message)-40, 70, 30)];
    [message addSubview:wxPayButton];
    wxPayButton.backgroundColor = __DefaultColor;
    wxPayButton.titleLabel.font = MFont(12);
    [wxPayButton setTitle:@"微信支付" forState:BtnNormal];
    [wxPayButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [wxPayButton addTarget:self action:@selector(chooseWXPayAction) forControlEvents:BtnTouchUpInside];
    
    UIImageView *wxIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectX(wxPayButton)+25, CGRectH(message)-65, 20, 20)];
    [message addSubview:wxIV];
    wxIV.image = MImage(@"weixin.png");
    

    
    //支付宝支付
    UIButton *payButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(message)-80), CGRectH(message)-40, 70, 30)];
    [message addSubview:payButton];
    payButton.backgroundColor = HEXCOLOR(0x55bce1);
    payButton.titleLabel.font = MFont(12);
    [payButton setTitle:@"支付宝支付" forState:BtnNormal];
    [payButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [payButton addTarget:self action:@selector(choosePayAction) forControlEvents:BtnTouchUpInside];
    
    UIImageView *aliIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectX(payButton)+25, CGRectH(message)-65, 20, 20)];
    [message addSubview:aliIV];
    aliIV.image = MImage(@"zhifubao.png");
    

}




//支付宝支付
- (void)choosePayAction {
    NSLog(@"支付宝支付");
    
    [ZPayMoney payWithOrderNumber:_payOrderID title:_payOrderName price:_payOrderPrice complete:^{
        
        //清除遮罩视图
        [messageBGView removeFromSuperview];
        
    }];
    
}

//银联支付
- (void)chooseUnoinPayAction {
     NSLog(@"银联支付");
    //清除遮罩视图
    [messageBGView removeFromSuperview];
    [ZPayMoney UnionPayWithOrderNumber:_payOrderID price:_payOrderName withVc:self complete:^{
        //
    }];
}


//微信支付
- (void)chooseWXPayAction {
    NSLog(@"微信支付");
    [ZPayMoney weiXinPayWithOrderNumber:_payOrderID title:_payOrderName price:_payOrderPrice complete:^{
        //清除遮罩视图
        [messageBGView removeFromSuperview];
    }];
    
    
}

- (void)payMessageViewCloseAction {
    NSLog(@"关闭提示框");
    
    //清除遮罩视图
    [messageBGView removeFromSuperview];
}



//设置Label文字指定位置大小 颜色
- (void)setLabeltextAttributes:(UILabel *)label {
    NSString *text = label.text;
    NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    
    //电话号码 字符颜色
    [attri addAttribute:NSForegroundColorAttributeName value:__MoneyColor range:NSMakeRange(5, length-5)];
    [label setAttributedText:attri];
    
}



#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
