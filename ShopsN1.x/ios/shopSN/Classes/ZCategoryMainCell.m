//
//  ZCategoryMainCell.m
//  亿速
//
//  Created by chang on 16/6/23.
//  Copyright © 2016年 wshan. All rights reserved.
//

#import "ZCategoryMainCell.h"

@implementation ZCategoryMainCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = HEXCOLOR(0xf0f0f0);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self initMainCellSubViews];
        
    }
    return self;
}




- (void)initMainCellSubViews {
  
    //bgView
    _categoryMainCellBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _categoryMainCellBGView.backgroundColor = HEXCOLOR(0xf0f0f0);
    [self.contentView addSubview:_categoryMainCellBGView];
    
    
    //1 侧边
    _categoryMainCellSideIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 5, CGRectH(_categoryMainCellBGView))];
    _categoryMainCellSideIV.backgroundColor = __DefaultColor;
    [self.contentView addSubview:_categoryMainCellSideIV];
    
    
    //2 名称
//    _categoryMainCellTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_categoryMainCellSideIV)+15, 0, CGRectW(_categoryMainCellBGView)-CGRectW(_categoryMainCellSideIV)-15, CGRectH(_categoryMainCellBGView)-1)];
    _categoryMainCellTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, CGRectH(self.contentView)-1)];
    //_categoryMainCellTitleLb.backgroundColor = __TestOColor;
    _categoryMainCellTitleLb.font = MFont(13);
    _categoryMainCellTitleLb.textColor = HEXCOLOR(0x333333);
    _categoryMainCellTitleLb.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_categoryMainCellTitleLb];
    
    
    //3 边线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(self.contentView), CGRectW(_categoryMainCellBGView), 1)];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    [self.contentView addSubview:lineIV];
    
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
