//
//  ZCAccountDiscountModel.h
//  shopSN
//
//  Created by chang on 16/7/10.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 账户管理 子页面
 *
 *   优惠券页面 模型类
 *
 */
#import <Foundation/Foundation.h>

@interface ZCAccountDiscountModel : NSObject

/** 优惠券 金额 */
@property (nonatomic, copy) NSString *discountMoney;

/** 优惠券 使用范围 */
@property (nonatomic, copy) NSString *discountReference;

/** 优惠券 有效期 */
@property (nonatomic, copy) NSString *discountTime;

@end
