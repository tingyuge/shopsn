//
//  ZCommonGoodsJudgeCell.m
//  shopSN
//
//  Created by yisu on 16/8/3.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodsJudgeCell.h"


@implementation ZCommonGoodsJudgeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        //点击cell不变色
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    
    
    //背景
    //UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, __kWidth-20, 192+20)];
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, __kWidth-20, 110)];
    [self addSubview:bgView];
    //bgView.backgroundColor = [UIColor orangeColor];
    
    
    //头像
    UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 15, 24, 24)];
    [bgView addSubview:iconView];
    iconView.layer.cornerRadius = 12;
    iconView.image = MImage(@"wdzzy");
    self.iconView = iconView;
    
    //标题
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(iconView)+5, 15, 24, 24)];
    [bgView addSubview:titleLb];
    //    titleLb.backgroundColor = __TestOColor;
    titleLb.font = MFont(11);
    titleLb.textColor = HEXCOLOR(0x999999);
    titleLb.text = @"用户";
    
    //用户id
    UILabel *userIdLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(titleLb), 15, 36, 24)];
    [bgView addSubview:userIdLb];
    self.userIdLb = userIdLb;
    //    userIdLb.backgroundColor = __TestOColor;
    userIdLb.font = MFont(11);
    userIdLb.textColor = HEXCOLOR(0x999999);
    userIdLb.text = @"Z***6";
    
    
    //评价星级
    _starView = [[ZSmallStarView alloc] initWithFrame:CGRectMake(CGRectXW(userIdLb)+5, 21, 80, 15)];
    _starView.starImage = MImage(@"xx_l_s.png");
    [bgView addSubview:_starView];
    //test
    //_starView.backgroundColor = __TestGColor;
    [_starView setStar:5];
    _starView.hidden = YES;
    
    
    //时间
    UILabel *timeLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-130, 0, 130, 24)];
    [bgView addSubview:timeLb];
    //    timeLb.backgroundColor = __TestOColor;
    self.timeLb = timeLb;
    timeLb.font = MFont(11);
    timeLb.textColor = HEXCOLOR(0x999999);
    timeLb.textAlignment = NSTextAlignmentRight;
    timeLb.text = @"2016-7-29";
    
    
    //评价内容
    // 包装完整，没有异味，全部都是U开头的纸尿裤，每包都有防伪标识，让人用的很放心
    UILabel *contentLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectYH(iconView)+10, CGRectW(bgView), 34)];
    [bgView addSubview:contentLb];
    self.contentLb = contentLb;
    contentLb.numberOfLines = 0;
    //    contentLb.backgroundColor = __TestOColor;
    contentLb.font = MFont(13);
    contentLb.textColor = HEXCOLOR(0x333333);
    contentLb.text = @"包装完整，没有异味，全部都是U开头的纸尿裤，每包都有防伪标识，让人用的很放心";
    
//    
//    //颜色标题
//    UILabel *colorTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectYH(contentLb)+5, 33, 20)];
//    [bgView addSubview:colorTitleLb];
////    colorTitleLb.backgroundColor = __TestGColor;
//    colorTitleLb.font = MFont(11);
//    colorTitleLb.textColor = HEXCOLOR(0x999999);
//    colorTitleLb.text = @"颜色：";
//    
//    //颜色内容
//    UILabel *colorLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(colorTitleLb), CGRectYH(contentLb)+5, 36, 20)];
//    [bgView addSubview:colorLb];
//    self.colorLb = colorLb;
////    colorLb.backgroundColor = __TestGColor;
//    colorLb.font = MFont(11);
//    colorLb.textColor = HEXCOLOR(0x333333);
//    colorLb.text = @"通用";
//    
//    //规格标题
//    UILabel *rankTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(colorLb), CGRectYH(contentLb)+5, 33, 20)];
//    [bgView addSubview:rankTitleLb];
////    rankTitleLb.backgroundColor = __TestGColor;
//    rankTitleLb.font = MFont(11);
//    rankTitleLb.textColor = HEXCOLOR(0x999999);
//    rankTitleLb.text = @"规格：";
//    
//    
//    //规格内容
//    UILabel *rankLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(rankTitleLb), CGRectYH(contentLb)+5, 110, 20)];
//    [bgView addSubview:rankLb];
//    self.rankLb = rankLb;
////    colorLb.backgroundColor = __TestGColor;
//    rankLb.font = MFont(11);
//    rankLb.textColor = HEXCOLOR(0x333333);
//    rankLb.text = @"L 宝贝1岁三个月";
//    
//    
//    //评价配图
//    UIView *photoView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(colorTitleLb)+10, 77, 77)];
//    [bgView addSubview:photoView];
//    self.photoView = photoView;
//    photoView.layer.borderWidth = 1.0f;
//    photoView.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
//    
//    UIImageView *photoIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 77, 77)];
//    [photoView addSubview:photoIV];
//    photoIV.image = MImage(@"pic11.png");
    
    
    //底线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(-10, CGRectH(bgView)-10, __kWidth, 10)];
    [bgView addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    
    
}


- (void)loadJudeCellData:(ZGoodsDeal *)deal {
    //userId
    if (deal.goodsJudgeName.length > 4) {
        //简写评价人 userID
        NSString *insertStr = [NSString stringWithFormat:@"%@****%@",[deal.goodsJudgeName substringToIndex:1],[deal.goodsJudgeName substringWithRange:NSMakeRange(7,1)]];
        _userIdLb.text = insertStr;
        
    }else{
        _userIdLb.text = deal.goodsJudgeName;
    }
    
    
    //timeLb
//    if (IsNilString(deal.goodsJudgeTime)) {
//        _timeLb.text = @"数据暂为空";
//    }else{
//        _timeLb.text   = deal.goodsJudgeTime;
//    }
    _timeLb.text   = deal.goodsJudgeTime;
    
    
    
    //contenLb
//    if (IsNilString(deal.goodsJudgeTime)) {
//        _contentLb.text = @"数据暂为空";
//    }else{
//        _contentLb.text = deal.goodsJudgeContent;
//    }
    NSDate *date = [Utils getDateForTimeStamp:deal.goodsJudgeContent];
    _contentLb.text = [Utils getDateStr:date type:@"yyyy-MM-dd HH:mm:ss"];
    //_contentLb.text = deal.goodsJudgeContent;
    
    
    
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
