//
//  ZShoppingCarBottomView.m
//  shopSN
//
//  Created by yisu on 16/6/30.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZShoppingCarBottomView.h"

@interface ZShoppingCarBottomView ()

/** 全部勾选 按钮 */
@property (nonatomic, strong) UIButton *checkBtn;





@end

@implementation ZShoppingCarBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        //bgView.backgroundColor = __TestGColor;
        [self addSubview:bgView];
        
        //添加子视图
        [self initSubView:bgView];
    }
    return self;
    
}


- (void)initSubView:(UIView *)view {
    
    //顶部边线
    UIImageView *topLineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), 1)];
    [view addSubview:topLineIV];
    topLineIV.backgroundColor= HEXCOLOR(0xdedede);
    
    
    //选择按钮
    _checkBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, CGRectH(view))];
    //_checkBtn.backgroundColor = __TestGColor;
    _checkBtn.titleLabel.font = MFont(15);
    [_checkBtn setTitle:@"全选" forState:BtnNormal];
    [_checkBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    [_checkBtn setImage:MImage(@"yuang01") forState:BtnNormal];
    [_checkBtn setImage:MImage(@"yuang02") forState:BtnStateSelected];
//    [_checkBtn setImage:MImage(@"yuang01") forState:BtnNormal];
//    [_checkBtn setImage:MImage(@"yuang02") forState:BtnStateSelected];
    [_checkBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [_checkBtn addTarget:self action:@selector(checkBtnAction:) forControlEvents:BtnTouchUpInside];
//    [view addSubview:_checkBtn];
    
    //结算按钮
//    _payNumStr = @"2";
//    NSString *payBtnStr = [NSString stringWithFormat:@"结算(%@)",_payNumStr];
    _payBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(view)-90, 0, 90, CGRectH(view))];
    _payBtn.backgroundColor = __DefaultColor;
    _payBtn.titleLabel.font = MFont(15);
    [_payBtn setTitle:@"结算" forState:BtnNormal];
    [view addSubview:_payBtn];
    [_payBtn addTarget:self action:@selector(payBtnAction) forControlEvents:BtnTouchUpInside];
    
    //合计现价 color (255 71 0) (ff4700)
    _totalNowMoneyLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_checkBtn)+5, 10, CGRectW(view)-CGRectW(_checkBtn)-CGRectW(_payBtn)-10, 15)];
    //_totalNowMoneyLb.backgroundColor = __TestGColor;
    _totalNowMoneyLb.font = MFont(14);
    _totalNowMoneyLb.textColor = HEXCOLOR(0xff4700);
    _totalNowMoneyLb.textAlignment = NSTextAlignmentRight;
    _totalNowMoneyLb.text = @"合计:￥0.00";
    [view addSubview:_totalNowMoneyLb];

    //节省金额
    _totalSaveMoneyLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_checkBtn)+5, CGRectYH(_totalNowMoneyLb), CGRectW(view)-CGRectW(_checkBtn)-CGRectW(_payBtn)-10, 15)];
    //_totalSaveMoneyLb.backgroundColor = __TestOColor;
    _totalSaveMoneyLb.font = MFont(10);
    _totalSaveMoneyLb.textColor = HEXCOLOR(0x999999);
    _totalSaveMoneyLb.textAlignment = NSTextAlignmentRight;
    _totalSaveMoneyLb.text = @"为您节省:￥140.00";
//    [view addSubview:_totalSaveMoneyLb];
    
    
    
}


#pragma mark - ===== 按钮触发方法 =====
//全选按钮
- (void)checkBtnAction:(UIButton *)btn {
    btn.selected = !btn.selected;
    [self.delegate didAllCheckButton:btn.selected];
}


//结算按钮
- (void)payBtnAction {
    //NSLog(@"跳转至 结算页面");
    [self.delegate didSelectPayButton];
}

- (void)setMoney:(NSString *)money number:(NSString *)number {
    _money = money;
    _number = number;
    _totalNowMoneyLb.text = [NSString stringWithFormat:@"合计:¥%@",_money];
    [_payBtn setTitle:[NSString stringWithFormat:@"结算(%@)",_number] forState:BtnNormal];
    if (money.intValue) {
        _payBtn.enabled = NO;
        _payBtn.backgroundColor = HEXCOLOR(0xacacac);
    } else {
        _payBtn.enabled = YES;
        _payBtn.backgroundColor = HEXCOLOR(0xff8c1e);
    }
}

- (void)setAllCheck:(BOOL)allCheck {
    _allCheck = allCheck;
    _checkBtn.selected = _allCheck;
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
