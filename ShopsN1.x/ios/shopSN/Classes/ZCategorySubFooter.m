//
//  ZCategorySubFooter.m
//  亿速
//
//  Created by chang on 16/6/24.
//  Copyright © 2016年 wshan. All rights reserved.
//

#import "ZCategorySubFooter.h"

@implementation ZCategorySubFooter

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        //直接作为底部线条使用
        BaseView *bgView = [[BaseView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xeeeeee);
        [self addSubview:bgView];
    }
    return self;
}


@end
