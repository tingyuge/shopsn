//
//  ZTabBarViewController.m
//  shopSN
//
//  Created by yisu on 16/6/12.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZTabBarViewController.h"
#import "ZHomeViewController.h"
#import "ZCategoryViewController.h"
#import "ZShoppingCarViewController.h"
#import "ZPersonalCenterViewController.h"
#import "ZYSLoginViewController.h"//登录

@interface ZTabBarViewController ()<ZHomeViewControllerDelegate, UITabBarControllerDelegate>
{
    NSMutableArray *_categorySectionArray;
    NSMutableArray *_rowTitleArray;
    //判断是否为会员商品
    BOOL _isGoods;
}

/** 主控制器 items*/
@property (nonatomic, strong) NSMutableArray *items;
/** 首页 */
@property (nonatomic, weak) ZHomeViewController *homeVc;
/** 分类 */
@property (nonatomic, weak) ZCategoryViewController *categoryVc;
/** 购物车 */
@property (nonatomic, weak) ZShoppingCarViewController *shoppingCarVc;
/** 个人中心 */
@property (nonatomic, weak) ZPersonalCenterViewController *personalCenterVc;


@end

@implementation ZTabBarViewController

#pragma mark - 懒加载
- (NSMutableArray *)items {
    if (!_items) {
        _items = [NSMutableArray array];
    }
    return _items;
}

#pragma mark - ===== 页面部分 =====
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabBar.backgroundColor = [UIColor whiteColor];
    
    _categorySectionArray = [NSMutableArray array];
    _rowTitleArray = [NSMutableArray array];
    
    _isGoods = YES;
    
    //添加所有的子控制器
    [self setUpAllChildViewController];
    
    

    self.delegate =self;
    
    
    
}


#pragma mark - 添加子控制器
//添加所有的子控制器
- (void)setUpAllChildViewController {
    
    //首页 red
    ZHomeViewController *homeVc = [[ZHomeViewController alloc] init];
    [self setUpOneChildViewController:homeVc image:OMImage(@"home_b") selectedImage:OMImage(@"home_hove") title:@"首页"];
    
    _homeVc = homeVc;
    _homeVc.delegate = self;
    
    
    
    
    //分类 green
    ZCategoryViewController *categoryVc = [[ZCategoryViewController alloc] init];
    [self setUpOneChildViewController:categoryVc image:OMImage(@"fbxs_b.png") selectedImage:OMImage(@"fbxs_hove") title:@"分类"];
    categoryVc.isGoods = _isGoods;
    _categoryVc = categoryVc;
    
    
    
    
    //购物车 blue
    ZShoppingCarViewController *shoppingCarVc = [[ZShoppingCarViewController alloc] init];
    [self setUpOneChildViewController:shoppingCarVc image:OMImage(@"buy_b") selectedImage:OMImage(@"buy_hove") title:@"购物车"];
    _shoppingCarVc = shoppingCarVc;
    
    
    
    
    //我的 orange
    ZPersonalCenterViewController *personalCenterVc = [[ZPersonalCenterViewController alloc] init];
    //[self setUpOneChildViewController:personalCenterVc image:MImage(@"me_b") selectedImage:OMImage(@"me_hove") title:@"个人中心"];
    [self setUpOneChildViewController:personalCenterVc image:OMImage(@"me_b") selectedImage:OMImage(@"me_hove") title:@"个人中心"];
    _personalCenterVc = personalCenterVc;
    
}


//添加一个子控制器
- (void)setUpOneChildViewController:(UIViewController *)vc image:(UIImage *)image selectedImage:(UIImage *)selectedImage title:(NSString *)title {
    
    vc.title                    = title;
    vc.tabBarItem.image         = image;
    vc.tabBarItem.selectedImage = selectedImage;
    
//    NSMutableDictionary *atrributes = [NSMutableDictionary dictionary];
//    atrributes[NSForegroundColorAttributeName] = __DefaultColor;
//    [vc.tabBarItem setTitleTextAttributes:atrributes forState:UIControlStateSelected];
    NSMutableDictionary *titleSelAttr = [NSMutableDictionary dictionary];
    titleSelAttr[NSForegroundColorAttributeName] = __DefaultColor;
    NSMutableDictionary *titleNorAttr = [NSMutableDictionary dictionary];
    titleNorAttr[NSForegroundColorAttributeName] = HEXCOLOR(0x010101);
    [vc.tabBarItem setTitleTextAttributes:titleSelAttr forState:UIControlStateSelected];
    [vc.tabBarItem setTitleTextAttributes:titleNorAttr forState:UIControlStateNormal];
    
    [self.items addObject:vc.tabBarItem];
    
    BaseNavigationController *navi = [[BaseNavigationController alloc] initWithRootViewController:vc];
    
    [self addChildViewController:navi];
    
}


#pragma mark ==== TabBarViewControllerDelegate =====
//购物车和个人中心页面 进行登录判断
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    //NSLog(@"tabBarVC_title:%@", tabBarController.title);
  
    //购物车和个人中心页面 进行登录判断
    if ([viewController.tabBarItem.title isEqualToString:@"购物车"] || [viewController.tabBarItem.title isEqualToString:@"个人中心"]  ) {

        if ([UdStorage getObjectforKey:Userid]) {
            //NSLog(@"%@",[UdStorage getObjectforKey:Userid]);
            return YES;
        }
        else{
            //跳转到登陆页面
            ZYSLoginViewController *login =[[ZYSLoginViewController alloc]init];
//            ZLoginViewController *login = [[ZLoginViewController alloc] init];
            login.pageName = viewController.tabBarItem.title;
            [((UINavigationController *)tabBarController.selectedViewController) pushViewController:login animated:YES];
            return NO;
        }
    
    }else{
        
         return YES;
    }
        
   
    
}



- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
   
    
    
    
    
    if ([viewController.title isEqualToString:@"分类"]) {
        //NSLog(@"即将进入分类页面");
        
        
        [_categoryVc getDataWithCategoryArray:[_categorySectionArray lastObject] andRowTitleArray:[_rowTitleArray lastObject]];
        
    }
    
   
    
}


#pragma mark ===== ZHomeViewControllerDelegate =====
- (void)sendValueToMainVCWithMainSectionArray:(NSArray *)mainSectionArray andRowArray:(NSArray *)rowArray andIsGoods:(BOOL)isGoods{
    
    [_categorySectionArray removeAllObjects];
    [_rowTitleArray removeAllObjects];
    
    [_categorySectionArray addObject:mainSectionArray];
    [_rowTitleArray addObject:rowArray];
    
    _isGoods = isGoods;
    _categoryVc.isGoods = isGoods;
    
}


#pragma mark - ===== 登录 =====

-(void)loginAction{
    
    
    //使用新设计图 登陆页面
//    ZLoginViewController *vc = [[ZLoginViewController alloc] init];

    ZYSLoginViewController *vc= [[ZYSLoginViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}




#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
