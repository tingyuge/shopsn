//
//  ZWelcomeViewController.m
//  shopSN(内测版)
//
//  Created by yisu on 16/7/25.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZWelcomeViewController.h"
#import "ZTabBarViewController.h"
#import "AppDelegate.h"

@interface ZWelcomeViewController ()<UIScrollViewDelegate>
{
    UIScrollView *_scrollView;
    UIPageControl *_pageControl;
    UIButton *homeBtn;
}

/** 视图定时器 */
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ZWelcomeViewController

- (void)viewWillAppear:(BOOL)animated {
    //隐藏导航栏
    self.navigationController.navigationBarHidden = YES;
    
    //隐藏状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    //取消隐藏导航栏
    self.navigationController.navigationBarHidden = NO;
    
    //取消隐藏状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    
    //关闭定时器
    [_timer setFireDate:[NSDate distantFuture]];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //开启定时器
    [_timer setFireDate:[NSDate distantPast]];
    
    self.view.backgroundColor = HEXCOLOR(0xffffff);

    
    
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, __kHeight)];
    
    //_scrollView.frame = self.view.bounds;
    [self.view addSubview:_scrollView];
//    _scrollView.backgroundColor = __TestOColor;
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator   = NO;
    _scrollView.delegate = self;
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(20, self.view.frame.size.height-50, self.view.frame.size.width-30, 20)];
    [self.view addSubview:_pageControl];
    
    //************ 加载图片 *************
    
    NSArray *imageNames = @[@"01", @"02", @"03"];
    
    for (int i = 0; i < imageNames.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageNames[i]]];
//        imageView.frame = CGRectMake(CGRectW(_scrollView) *i, 0, CGRectW(_scrollView), CGRectH(_scrollView));
//        imageView.frame = CGRectMake(__kWidth *i, 0, __kWidth, __kHeight);
        
        CGRect frame = _scrollView.frame;
        frame.origin.x = i * frame.size.width;
        frame.origin.y = 0;
        imageView.frame = frame;
        
        [_scrollView addSubview:imageView];
        
        //点击最后一张图片时，跳转
        if (i == imageNames.count - 1) {
            UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
            [imageView addGestureRecognizer:tapGR];
            imageView.userInteractionEnabled = YES;
            
            
            
        }
    }
    

    
    
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width *imageNames.count, 0);
    
    //pagecontrol config
    _pageControl.numberOfPages = imageNames.count;
    _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    _pageControl.currentPageIndicatorTintColor = [UIColor greenColor];
    _pageControl.userInteractionEnabled = NO;
    
    //scrollview delegate
    _scrollView.delegate = self;
    
    
    //scrollView动画播放
    _timer = [NSTimer scheduledTimerWithTimeInterval:4.0f target:self selector:@selector(playingWelcomeImage) userInfo:nil repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    
    
    //进入主页面按钮
    homeBtn = [[UIButton alloc] initWithFrame:CGRectMake(__kWidth-90, 55, 70, 30)];
    [self.view addSubview:homeBtn];
    homeBtn.layer.borderWidth = 1.0f;
    homeBtn.layer.borderColor = HEXCOLOR(0xffffff).CGColor;
    homeBtn.layer.cornerRadius = 5.0f;
    homeBtn.titleLabel.font = MFont(12);
    [homeBtn setTitle:@"进入主页面" forState:BtnNormal];
    [homeBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [homeBtn addTarget:self action:@selector(homeBtnAction) forControlEvents:BtnTouchUpInside];
    
}





//************ 跳转至TabBarController *************
- (void)tap:(UITapGestureRecognizer *)gr{
    NSLog(@"进入主界面");
    
//    MainViewController *vc = [[MainViewController alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
}




#pragma mark - 主页面跳转按钮
- (void)homeBtnAction {
    NSLog(@"进入主界面");
    
    ZTabBarViewController *tabVC = [[ZTabBarViewController alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    app.window.rootViewController = tabVC;
    
    
    //第一次浏览完欢迎界面之后就不再显示了
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:@"isVisibled"];
    [ud synchronize];
    
}



#pragma mark --- scrollView delegate
//************ 设置pageControl *************
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    NSInteger pageIndex = lrintf(scrollView.contentOffset.x /scrollView.frame.size.width);
    _pageControl.currentPage = pageIndex;
    
//    if (_pageControl.currentPage == 2) {
//        NSLog(@"等待2秒自动跳转主页面");
//        
//        _timer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(playingWelcomeImage) userInfo:nil repeats:NO];
//    }
    
}


#pragma mark - 滑动自动跳转


#pragma mark - scrollView 动画播放
- (void)playingWelcomeImage {
    int i = 1;
    if (_pageControl.currentPage == 2) {
        
        //判断当前页是最后一页，即改为第一页 继续循环
//        _pageControl.currentPage = 0;
//        i = 0;
        
        //改为自动跳转至主页面
        ZTabBarViewController *tabVC = [[ZTabBarViewController alloc] init];
        AppDelegate *app = [UIApplication sharedApplication].delegate;
        app.window.rootViewController = tabVC;
        
        
        //第一次浏览完欢迎界面之后就不再显示了
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setBool:YES forKey:@"isVisibled"];
        [ud synchronize];

    }
    _scrollView.contentOffset = CGPointMake((_pageControl.currentPage + i) * self.view.frame.size.width, 0);
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
