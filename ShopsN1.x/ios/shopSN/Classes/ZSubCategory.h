//
//  ZSubCategory.h
//  shopSN
//
//  Created by yisu on 16/9/9.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 分类页面
 *
 *  二级分类模型
 *
 */

#import <Foundation/Foundation.h>

@interface ZSubCategory : NSObject
/** 二级分类 id */
@property (copy, nonatomic) NSString *subCategoryID;

/** 二级分类 名称 */
@property (copy, nonatomic) NSString *subCategoryName;

@end
