//
//  ZCopyrightViewController.m
//  shopSN
//
//  Created by imac on 2016/12/22.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCopyrightViewController.h"

@interface ZCopyrightViewController ()

@property (strong,nonatomic) UILabel *textLb;

@end

@implementation ZCopyrightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLb.text = @"版权声明";
    self.view.backgroundColor = [UIColor whiteColor];
    [self initView];
}

-(void)initView{
    _textLb = [[UILabel alloc]initWithFrame:CGRectMake(10, 40, __kWidth-20, 200)];
    [self.view addSubview:_textLb];
    _textLb.textAlignment = NSTextAlignmentLeft;
    _textLb.textColor = [UIColor blackColor];
    _textLb.font = MFont(15);
    _textLb.numberOfLines = 0;
    _textLb.text = @"1、本APP版权归shopSN(上海亿速网络科技有限公司)所有；\n2、本APP源码可以免费用于商业用途（但必须保留此申明，如未经商业授权删除此声明，视为侵权盗版。）；\n3、如需删除声明购买授权或升级维护请联系本公司www.shopsn.net进行咨询";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
