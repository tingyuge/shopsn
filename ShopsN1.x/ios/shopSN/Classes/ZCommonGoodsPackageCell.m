//
//  ZCommonGoodsPackageCell.m
//  shopSN
//
//  Created by yisu on 16/9/13.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodsPackageCell.h"

@implementation ZCommonGoodsPackageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        //点击cell不变色
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        [self initSubViews];
    }
    return self;
}




- (void)initSubViews {
    
    //bgView
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 44+10)];
    [self addSubview:bgView];
    //    bgView.backgroundColor = [UIColor orangeColor];
    
    
    //packageTitleLb
    _packageTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, CGRectW(bgView)-20, 15)];
    [bgView addSubview:_packageTitleLb];
    //_packageTitleLb.backgroundColor = __TestGColor;
    _packageTitleLb.font = MFont(15);
    _packageTitleLb.textColor = HEXCOLOR(0x333333);
    _packageTitleLb.text = @"套餐选择：有";
    
    
    
    
    
    
    //lineIV
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-10, CGRectW(bgView), 10)];
    [bgView addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xf0f0f0);
    
    
}


#pragma mark - reloadData
- (void)setGoodsIntrPackage:(ZGoodsDeal *)deal {
    
    //title
    NSString *str;
    if (IsNilString(deal.goodsPackage)) {
        str = @"无";
    }else{
        str = deal.goodsPackage;
    }
    
    self.packageTitleLb.text = [NSString stringWithFormat:@"套餐选择：%@",str];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
