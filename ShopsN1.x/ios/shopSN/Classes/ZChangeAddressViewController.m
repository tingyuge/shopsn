//
//  ZChangeAddressViewController.m
//  shopSN
//
//  Created by imac on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZChangeAddressViewController.h"
#import "ZChangeAddressTableViewCell.h"
#import "ZChangeAddressTableViewOtherCell.h"//详细地址 cell
#import "ZChooseAreaView.h"
//#import "ZPersonConnectViewController.h"

@interface ZChangeAddressViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate>


/**
 *  地区选择
 */
@property (strong,nonatomic) ZChooseAreaView *pickerView;

@end

@implementation ZChangeAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   self.titleLb.text = @"修改收货地址";
    [self initView];
    _isDefault = self.addressModel.isDefaultAddress;
}

- (void)initView{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-108)];
    [self.view addSubview:_tableView];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = LH_RGBCOLOR(230, 230, 230);
    
    UIButton *saveBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, CGRectYH(_tableView)+1, __kWidth, 43)];
    [self.view addSubview:saveBtn];
    saveBtn.backgroundColor = [UIColor whiteColor];
    saveBtn.titleLabel.font = MFont(15);
    [saveBtn setTitle:@"保存地址" forState:BtnNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:BtnNormal];
    saveBtn.backgroundColor = __DefaultColor;
    [saveBtn addTarget:self action:@selector(saveAddress) forControlEvents:BtnTouchUpInside];
}

/**
 *  选择视图
 */
- (void)showPick{
    WK(weakSelf)
    _pickerView = [[ZChooseAreaView alloc]initWithAreaFrame:CGRectMake(0, __kHeight-200, __kWidth, 200)];
    [self.view addSubview:_pickerView];
    _pickerView.backgroundColor = LH_RGBCOLOR(200, 200, 210);
    
    _pickerView.returntextfileBlock=^(NSString *data){
        
        _addStr =data;
        
        NSArray *arr = [data componentsSeparatedByString:@" "];
        weakSelf.addressModel.Province =arr[0];
        weakSelf.addressModel.city = arr[1];
        weakSelf.addressModel.area = arr[2];
        
        
        NSMutableArray *paArr = [@[] mutableCopy];
        
        for (int idx = 0; idx<6; idx++) {
            if (idx!=3) {
                ZChangeAddressTableViewCell *cell = [weakSelf.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                [paArr addObject:cell.detailTV.text];
                
            }
            if (idx==3) {
                ZChangeAddressTableViewOtherCell *cell = [weakSelf.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                [paArr addObject:cell.addressTF.text];
            }
        }
        NSLog(@"112--%@", paArr);
        weakSelf.addressModel.realname = paArr[0];
        weakSelf.addressModel.mobile = paArr[1];
        weakSelf.addressModel.address = paArr[3];
        weakSelf.addressModel.zipcode = paArr[5];
        
        [ weakSelf.tableView reloadData];
    };
    
}
//#pragma mark -ZPersonConnectViewControllerDelegate
//-(void)didselectedPerson:(ReceiveAddressModel *)sender{
//    _addressModel.realname =sender.realname;
//    _addressModel.mobile = sender.mobile;
//    [_tableView reloadData];
//}

#pragma mark ==保存地址==
-(void)saveAddress{
    
    NSMutableArray *paArr = [@[] mutableCopy];
    
    for (int idx = 0; idx<6; idx++) {
        if (idx!=3) {
            ZChangeAddressTableViewCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
            [paArr addObject:cell.detailTV.text];
        }
        if (idx==3) {
            ZChangeAddressTableViewOtherCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
            [paArr addObject:cell.addressTF.text];
        }
    }
    
//    NSLog(@"%@", paArr);
    
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid],
                            @"realname":paArr[0],
                            @"id":self.addressModel.raID,
                            @"prov":self.addressModel.Province,
                            @"city":self.addressModel.city,
                            @"dist":self.addressModel.area,
                            @"address":paArr[3],
                            @"status":_isDefault?@1:@0,
                            @"mobile":paArr[1],
                            @"zipcode":paArr[5]};
    NSLog(@"%@", params);
    WK(weakSelf);
    [ZHttpRequestService POST:@"Address/editAddressByUser" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
            [weakSelf.navigationController popViewControllerAnimated:YES];

        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];

    
    
    
}

#pragma mark -UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ReceiveAddressModel *data = self.addressModel;
    
    if (indexPath.row == 3) {
        ZChangeAddressTableViewOtherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableOhterCell"];
        if (!cell) {
            cell = [[ZChangeAddressTableViewOtherCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TableOhterCell"];
        }
        cell.titleLb.text = @"详细地址";
        cell.addressTF.text = data.address;
        cell.addressTF.hidden = NO;
        cell.addressTF.delegate = self;
        return cell;
        
    }
    ZChangeAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell"];
    if (!cell) {
        cell = [[ZChangeAddressTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TableViewCell"];
    }
    cell.detailTV.delegate = self;
    cell.detailTV.tag = indexPath.row +22+indexPath.section;
    switch (indexPath.row) {
        case 0:
        {
            cell.titleLb.text = @"收货人";
            if (!IsNilString(_addressModel.realname)) {
                cell.detailTV.text =_addressModel.realname;
            }
            
        }
            break;
        case 1:
        {
            cell.titleLb.text = @"手机号码";
            if (!IsNilString(_addressModel.mobile)) {
                cell.detailTV.text = _addressModel.mobile;
            }
            cell.detailTV.keyboardType = UIKeyboardTypePhonePad;
        }
            break;
        case 2:
        {
            cell.titleLb.text = @"省市地区";
            cell.detailTV.text = [NSString stringWithFormat:@"%@%@%@",_addressModel.Province,_addressModel.city,_addressModel.area];;
        }
            break;
//        case 3:
//        {
//            cell.titleLb.text = @"详细地址";
//            cell.detailTV.hidden = YES;
//            cell.addressTF.text = @"百春园街道百春园22栋4单元401室";
//            cell.addressTF.hidden = NO;
//            cell.addressTF.delegate = self;
//        }
//            break;
        case 4:
        {
            cell.titleLb.text = @"设为默认地址";
            cell.detailTV.hidden = YES;
            cell.chooseIV.hidden = NO;
            if (_isDefault) {
                cell.chooseIV.image = [UIImage imageNamed:@"yuang02"];
            }else{
                cell.chooseIV.image = nil;
            }
        }
            break;
        default:
        {
            cell.titleLb.text = @"邮编";
            cell.detailTV.text = _addressModel.zipcode;
//            cell.detailTV.text = @"422000";
        }
            break;
    }
         return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        return 80;
    }
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row ==4) {
        _isDefault=!_isDefault;
        NSMutableArray *paArr = [@[] mutableCopy];
        
        for (int idx = 0; idx<6; idx++) {
            if (idx!=3) {
                ZChangeAddressTableViewCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                [paArr addObject:cell.detailTV.text];
                
            }
            if (idx==3) {
                ZChangeAddressTableViewOtherCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                [paArr addObject:cell.addressTF.text];
            }
        }
        self.addressModel.realname = paArr[0];
        self.addressModel.mobile = paArr[1];
        self.addressModel.address = paArr[3];
        self.addressModel.zipcode = paArr[5];
        [self.tableView reloadData];
    }else{
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
   }
#pragma mark -UITextFiledDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.tag == 24) {
        [self showPick];
        [self.view endEditing:YES];
        return NO;
    }
//    if (textField.tag == 22||textField.tag == 23) {
//        ZPersonConnectViewController *vc = [[ZPersonConnectViewController alloc]init];
//        vc.delegate = self;
//        [self.navigationController pushViewController:vc animated:YES];
//        return NO;
//    }

    if (textField.tag ==27) {
        if (__kHeight<__k5Height) {
            [UIView animateWithDuration:0.4 animations:^{
                _tableView.frame=CGRectMake(0, -110, __kWidth, __kHeight-130);
            }];
        }
        if (__kHeight==__k5Height) {
            [UIView animateWithDuration:0.4 animations:^{
                _tableView.frame = CGRectMake(0, 0, __kWidth, __kHeight-108);
            }];
        }
    }
        return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (textField.tag ==27) {
        if (__kHeight<=__k5Height) {
            [UIView animateWithDuration:0.4 animations:^{
                _tableView.frame=CGRectMake(0, 64, __kWidth, __kHeight-108);
            }];
        }
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -UITextViewDelegate
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
        if (__kHeight<__k5Height) {
            [UIView animateWithDuration:0.4 animations:^{
                _tableView.frame=CGRectMake(0, 0, __kWidth, __kHeight-108);
            }];
        }
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if (__kHeight<__k5Height) {
        [UIView animateWithDuration:0.4 animations:^{
            _tableView.frame=CGRectMake(0, 64, __kWidth, __kHeight-108);
        }];
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
