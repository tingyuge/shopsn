//
//  ZCenterMyPrintsCell.m
//  shopSN
//
//  Created by yisu on 16/7/11.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterMyPrintsCell.h"

@implementation ZCenterMyPrintsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
    }
    return self;
}


- (void)initSubViews {
    //背景
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 102)];
    [self addSubview:bgView];
    //bgView.backgroundColor = [UIColor orangeColor];
    
    
    //1 上边框 view
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 10)];
    [bgView addSubview:topView];
    topView.backgroundColor = HEXCOLOR(0xf0f0f0);
    
    
    //2 图片
    _cp_iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectYH(topView)+5, 108, 82)];
    [bgView addSubview:_cp_iconIV];
    _cp_iconIV.layer.borderWidth = 1.0f;
    _cp_iconIV.layer.borderColor = HEXCOLOR(0xf0f0f0).CGColor;
    _cp_iconIV.image = MImage(@"pic04.png");
    
    
    //3 标题 日本本州6或7日游
    _cp_titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_cp_iconIV)+5, CGRectYH(topView)+5, 160, 40)];
    [bgView addSubview:_cp_titleLb];
//    _cp_titleLb.backgroundColor = __TestOColor;
    _cp_titleLb.font = MFont(12);
    _cp_titleLb.numberOfLines = 0;
    _cp_titleLb.textColor = HEXCOLOR(0x333333);
    _cp_titleLb.text = @"日本本州6或7日游";
    
    //4 详情 精华本州一次看尽 感悟冬日浪漫美景
    _cp_detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_cp_iconIV)+5, CGRectYH(_cp_titleLb), 160, 0)];
//    [bgView addSubview:_cp_detailLb];
//    _cp_detailLb.backgroundColor = __TestOColor;
    _cp_detailLb.font = MFont(12);
    _cp_detailLb.textColor = HEXCOLOR(0x999999);
    _cp_detailLb.text = @"精华本州一次看尽 感悟冬日浪漫美景";
    
    
    //5 类型 跟团游
    _cp_typeLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_cp_iconIV)+5, CGRectYH(_cp_detailLb), 160, 0)];
//    [bgView addSubview:_cp_typeLb];
//    _cp_typeLb.backgroundColor = __TestOColor;
    _cp_typeLb.font = MFont(12);
    _cp_typeLb.textColor = HEXCOLOR(0x999999);
    _cp_typeLb.text = @"跟团游";
    
    
    //6 价格
    _cp_priceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_cp_iconIV)+5, CGRectYH(_cp_detailLb), 160, 22)];
    [bgView addSubview:_cp_priceLb];
//    _cp_priceLb.backgroundColor = __TestGColor;
    _cp_priceLb.font = MFont(12);
    _cp_priceLb.textColor = __MoneyColor;
    _cp_priceLb.text = @"￥1339起";
    
    [self setLabeltextAttributes:_cp_priceLb.text];
    
    
    //7 购物车 按钮 --- 【我的收藏】显示  【我的足迹】隐藏
    _cp_shoppingButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(bgView)-10-20), (CGRectH(bgView)-10-20), 20, 20)];
    [bgView addSubview:_cp_shoppingButton];
    [_cp_shoppingButton setImage:MImage(@"gwc.png") forState:BtnNormal];
    
    
}

//设置字体 指定位置大小和颜色
- (void)setLabeltextAttributes:(NSString *)text {
    NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    [attri addAttribute:NSFontAttributeName value:MFont(8) range:NSMakeRange(length-1, 1)];
    [attri addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x999999) range:NSMakeRange(length-1, 1)];
    
    [_cp_priceLb setAttributedText:attri];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
