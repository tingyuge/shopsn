//
//  ZHomeGridHeader2.h
//  shopSN
//
//  Created by yisu on 16/8/11.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 collectionView
 *
 *  会员商品展示 header
 *
 */
#import <UIKit/UIKit.h>

@interface ZHomeGridHeader2 : UICollectionReusableView

/** 普通商品展示 header 更多按钮 */
@property (nonatomic, strong) UIButton *commonGoodsMoreBtn;


/** 普通商品展示 header 标题label */
@property (nonatomic, strong) UILabel *commonGoodsTitleLb;

@end
