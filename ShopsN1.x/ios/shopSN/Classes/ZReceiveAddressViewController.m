//
//  ZReceiveAddressViewController.m
//  shopSN
//
//  Created by imac on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZReceiveAddressViewController.h"
#import "ZAddressTableCell.h"
#import "ZAddressDefaultTableCell.h"
#import "ZChangeAddressViewController.h"
#import "ReceiveAddressModel.h"
#import "ZIncreaseAddressViewController.h"
#import "ZPersonInfoModel.h"

#import "ZPayViewController.h"
#import "ZCenterPersonalInfoViewController.h"


@interface ZReceiveAddressViewController ()<UITableViewDelegate,UITableViewDataSource,ZAddressTableCellDelegate,ZAddressDefaultTableCelldelegate>
{
    UITableView *_tableView;
    UIView *footerView;
}

//@property (strong,nonatomic) UITableView *tableView;

@property (strong,nonatomic) NSMutableArray <ReceiveAddressModel *>*dataArr;

@end

@implementation ZReceiveAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLb.text = @"收货地址";
    [self initView];
    _dataArr = [NSMutableArray array];
    
    NSArray *infoArr = [ZPersonInfoModel shareInfoModel].infoArr;
    if (!infoArr || infoArr.count==0) {
        return;
    }
    for (int i=0; i<infoArr.count; i++) {
        
        ReceiveAddressModel *data = [[ReceiveAddressModel alloc]init];
        
        NSDictionary *dic = infoArr[i];
        data.realname = dic[@"realname"];
        data.mobile = dic[@"mobile"];
        data.address = dic[@"address"];
        data.area = dic[@"dist"];
        data.city = dic[@"city"];
        data.Province = dic[@"prov"];
        data.raID = dic[@"addressid"];
        data.zipcode = dic[@"zipcode"];
        data.isDefaultAddress = [dic[@"status"] isEqualToString:@"0"]?false:true;
        [_dataArr addObject:data];
        
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getIntetfacePersonInfo];
}
#pragma mark *** 获取网络个人信息 ***
-(void)getIntetfacePersonInfo{
    
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid]};

    [ZHttpRequestService POST:@"Address/showAddressList" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
            if ([jsonDic[@"data"] isKindOfClass:[NSNull class]]) {
                NSLog(@"无数据情况");
                footerView.hidden = NO;
            }else {
                //存入单例
                [ZPersonInfoModel shareInfoModel].infoArr = jsonDic[@"data"];
                _dataArr = [NSMutableArray array];
                NSArray *infoArr = [ZPersonInfoModel shareInfoModel].infoArr;
                if (!infoArr || infoArr.count==0) {
                    return;
                }
                for (int i=0; i<infoArr.count; i++) {
                    ReceiveAddressModel *data = [[ReceiveAddressModel alloc]init];
                    NSDictionary *dic = infoArr[i];
                    data.realname = dic[@"realname"];
                    data.mobile = dic[@"mobile"];
                    data.address = dic[@"address"];
                    data.area = dic[@"dist"];
                    data.city = dic[@"city"];
                    data.Province = dic[@"prov"];
                    data.raID = dic[@"id"];
                    data.zipcode = dic[@"zipcode"];
                    data.isDefaultAddress = [dic[@"status"] isEqualToString:@"0"]?false:true;
                    [_dataArr addObject:data];
                }
            }
            
            [_tableView reloadData];
            
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
    } animated:true withView:nil];
}

- (void)initView{
    _tableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-108)];
    [self.view addSubview:_tableView];
    _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, CGRectW(self.view), 30)];
    UILabel *endNoteLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectH(footerView)-20, CGRectW(footerView)-20, 20)];
    [footerView addSubview:endNoteLb];
    endNoteLb.font = MFont(14);
    endNoteLb.textAlignment = NSTextAlignmentCenter;
    endNoteLb.text = @"没有更多数据了 ...";
    footerView.hidden = YES;
    _tableView.tableFooterView = footerView;
    
    
    
    
    UIButton *addBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, CGRectYH(_tableView)+1, __kWidth, 43)];
    [self.view addSubview:addBtn];
    addBtn.backgroundColor = [UIColor whiteColor];
    addBtn.titleLabel.font = MFont(15);
    [addBtn setTitle:@"添加新地址" forState:BtnNormal];
    [addBtn setTitleColor:__DefaultColor forState:BtnNormal];
    [addBtn addTarget:self action:@selector(addAddress) forControlEvents:BtnTouchUpInside];
    
}

#pragma mark ==添加新地址==
-(void)addAddress{
    NSLog(@"添加地址");
    ZIncreaseAddressViewController *vc = [[ZIncreaseAddressViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReceiveAddressModel *data = _dataArr[indexPath.row];

    if (data.isDefaultAddress) {
        ZAddressDefaultTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZAddressDefaultTableCell"];
        if (!cell) {
            cell = [[ZAddressDefaultTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ZAddressDefaultTableCell"];
        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.delegate =self;
        cell.nameLb.text = data.realname;
        cell.mobileLb.text = data.mobile;
        cell.addressLb.text = [NSString stringWithFormat:@"%@%@%@%@",data.Province,data.city,data.area,data.address];
        cell.editBtn.tag = indexPath.row+11;
        cell.delBtn.tag = indexPath.row+81;
        return cell;
    }else{
        ZAddressTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZAddressTableCell"];
        if (!cell) {
            cell = [[ZAddressTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ZAddressTableCell"];
        }
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.delegate =self;
        cell.nameLb.text = data.realname;
        cell.mobileLb.text = data.mobile;
        cell.addressLb.text = [NSString stringWithFormat:@"%@%@%@%@",data.Province,data.city,data.area,data.address];
        cell.editBtn.tag = indexPath.row+11;
        cell.delBtn.tag = indexPath.row+81;
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *vcs = self.navigationController.viewControllers;
    NSInteger vcCount = vcs.count;
    
    if ([vcs[vcCount-2] isKindOfClass:[ZPayViewController class]]) {
        NSLog(@"回去是结算");
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        NSMutableArray *backArr = [@[] mutableCopy];
        
        ReceiveAddressModel *data = _dataArr[indexPath.row];
        
        [backArr addObject:data.realname];
        [backArr addObject:data.mobile];
        [backArr addObject:data.Province];
        [backArr addObject:data.city];
        [backArr addObject:data.area];
        [backArr addObject:data.address];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"backToAddress" object:backArr userInfo:nil];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    if ([vcs[vcCount-2] isKindOfClass:[ZCenterPersonalInfoViewController class]]) {
        NSLog(@"回去是个人信息 ");
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    

    
}

#pragma mark -==地址cell== Delegate

-(void)edit:(UIButton *)sender{
    ZChangeAddressViewController *vc= [[ZChangeAddressViewController alloc]init];
    ReceiveAddressModel *data = _dataArr[sender.tag-11];
    vc.addressModel = data;
    
    [vc.tableView reloadData];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)delAddress:(UIButton *)sender{
    ReceiveAddressModel *car =_dataArr[sender.tag-81];
    [self deleteAdd:car.raID];
    [self.dataArr removeObjectAtIndex:sender.tag-81];
    [_tableView reloadData];
}

-(void)deleteAdd:(NSString *)sender{
    NSLog(@"删除地址成功");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
