//
//  ZCAccountPointsViewController.m
//  shopSN
//
//  Created by chang on 16/7/10.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCAccountPointsViewController.h"
#import "ZAccountManagerModel.h"
@interface ZCAccountPointsViewController ()

/** 我的积分label */
@property (nonatomic, strong) UILabel *pointsLb;

@end

@implementation ZCAccountPointsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLb.text = @"积分";
    
    //初始化 中间视图 相关子视图 color(239 240 241) (0xeff0f1)
    self.mainMiddleView.backgroundColor = __AccountBGColor;
    [self initSubViews:self.mainMiddleView];
    
}

#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    //获取单例积分数据
    NSDictionary *dic = [ZAccountManagerModel shareAccountManagerData].accountDic;
    
    //边线
    UIImageView *topLineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 1)];
    [view addSubview:topLineIV];
    topLineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    //视图
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 1, CGRectW(view), 300)];
    [view addSubview:bgView];
    bgView.backgroundColor = __DefaultColor;
    
    //1 pointsLb
//    _pointsLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, CGRectW(view)-30, 50)];
//    [view addSubview:_pointsLb];
//    _pointsLb.font = MFont(45);
//    _pointsLb.textColor = HEXCOLOR(0xffffff);
//    _pointsLb.text = [NSString stringWithFormat:@"%@ 分",dic[@"integral"]];
//    [self setLabeltextAttributes:_pointsLb.text];
    
    NSArray *dataArr = @[@"A账户",[NSString stringWithFormat:@"%@ 分",dic[@"add_jf_limit"]],@"B账户",[NSString stringWithFormat:@"%@ 分",dic[@"add_jf_currency"]]];
    
    for (int idx = 0; idx<dataArr.count; idx++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 30+60*idx, CGRectW(view)-30, 50)];
        label.font = MFont(45);
        label.textColor = HEXCOLOR(0xffffff);
        label.text = dataArr[idx];
        if (idx%2==1) {
            [self setLabeltextAttributes:label];
        }
        [view addSubview:label];
    }
    
    
    //2 notelabel
    //  小积分，有大用，多领一些囤起来！
    UILabel *noteLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 270, CGRectW(view)-30, 20)];
    [view addSubview:noteLb];
    noteLb.font = MFont(14);
    noteLb.textColor = HEXCOLOR(0xffffff);
    noteLb.text = @"小积分，有大用，多领一些囤起来！";
    
}


//修改label中字头大小
- (void)setLabeltextAttributes:(UILabel *)label {
    NSInteger length = label.text.length;
    
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:label.text];
    [attri addAttribute:NSFontAttributeName value:MFont(14) range:NSMakeRange(length-1, 1)];
    
    [label setAttributedText:attri];
}



#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
