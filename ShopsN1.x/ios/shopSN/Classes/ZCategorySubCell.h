//
//  ZCategorySubCell.h
//  亿速
//
//  Created by chang on 16/6/24.
//  Copyright © 2016年 wshan. All rights reserved.
//

/* 提供 分类页面 中间分类视图
 *
 *  右侧侧 子分类 cell
 *
 */
#import <UIKit/UIKit.h>

@interface ZCategorySubCell : UICollectionViewCell

/** 子分类中 商品 imageView */
@property (nonatomic, strong) UIImageView *goodsIV;
/** 子分类中 商品 Label */
@property (nonatomic, strong) UILabel *goodsLb;




@end
