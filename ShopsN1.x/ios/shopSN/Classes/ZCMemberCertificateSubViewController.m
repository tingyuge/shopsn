//
//  ZCMemberCertificateSubViewController.m
//  shopSN
//
//  Created by yisu on 16/7/15.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCMemberCertificateSubViewController.h"

@interface ZCMemberCertificateSubViewController ()
{
    UIView *_topView;
    UIWebView *_webView;
}

@end

@implementation ZCMemberCertificateSubViewController
#pragma mark - ==== 页面设置 =====
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"会员资格";
    
    //视图 初始化
    [self initViews];
    
}

//视图 初始化
- (void)initViews {
    
    //上边线
    UIImageView *topLineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, 1)];
    [self.view addSubview:topLineIV];
    topLineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    //顶部视图
    _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, 99)];
    [self.view addSubview:_topView];
    [self addSubViewWithTopView:_topView];
    
    
    //网页视图
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, CGRectYH(_topView), __kWidth, __kHeight-64-100)];
    [self.view addSubview:_webView];
    _webView.backgroundColor = __TestGColor;
}

//添加顶部视图子控件
- (void)addSubViewWithTopView:(UIView *)topView {
    //1 底部图片
    UIImageView *bgIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(topView), 99)];
    [topView addSubview:bgIV];
    bgIV.image = MImage(@"hybg.png");
//    bgIV.contentMode = UIViewContentModeScaleAspectFit;
}



#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
