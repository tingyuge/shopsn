/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : shopsn_test

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-08-16 13:33:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for db_class_feel
-- ----------------------------
DROP TABLE IF EXISTS `db_class_feel`;
CREATE TABLE `db_class_feel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `class_id` int(11) DEFAULT NULL COMMENT '分类id',
  `title` varchar(50) DEFAULT NULL COMMENT '分类名称',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='评论相关表';

-- ----------------------------
-- Records of db_class_feel
-- ----------------------------
INSERT INTO `db_class_feel` VALUES ('1', '7', '手感很好', '1489641147', '1489641147');
INSERT INTO `db_class_feel` VALUES ('2', '7', '和手机很搭', '1489641147', '1489641147');
INSERT INTO `db_class_feel` VALUES ('3', '7', '材料不错', '1489641147', '1489641147');
INSERT INTO `db_class_feel` VALUES ('4', '7', '图案漂亮', '1489641147', '1489641147');
INSERT INTO `db_class_feel` VALUES ('5', '7', '保护到位', '1489641147', '1489641147');
INSERT INTO `db_class_feel` VALUES ('6', '7', '贴合手机', '1489641147', '1489641147');
INSERT INTO `db_class_feel` VALUES ('7', '7', '包装精美', '1489641147', '1489641147');
INSERT INTO `db_class_feel` VALUES ('8', '7', '颜色很喜欢', '1489641147', '1489641147');
INSERT INTO `db_class_feel` VALUES ('9', '7', '质量不错', '1489641147', '1489641147');
INSERT INTO `db_class_feel` VALUES ('10', '7', '很不错', '1489641147', '1489641147');
