#pragma once
#ifndef FILE_H
#define FILE_H

#include <iostream>
#include <fstream>
#include <string>
#include <Windows.h>
using namespace std;

/*
	@author 王强
	@date  2017-3-30
 */
class File
{
private:
	string fileName;
	string basePath;
	fstream * fs; //文件操作对象
	File(string & name, string &);
	
	File & operator = (File &);
	
	File();
	
	~File();
	static File * curret;//当前对象
protected:
	int count;//循环次数
	int start;//起始次数
	string fileStart;
	string putFile;
public:
	 static File * getInstance(string name, string path);
	 static File * getInstance();
	 void setFileStart(string &);
	 void setPutFile(string &);
	 string getFIleStart();
	 void setFileName(string &);
	 //设置开始读的次数
	 void setStart(int & num) { this->start = num; }
	 
	 int getStart() { return this->start; }
	 
	 void setBasePath(string &);//设置路径
	//替换指定的子串
	//src:原字符串 target:待被替换的子串 subs:替换的子串
	 string replaceALL(const char * src, const string& target, const string& subs)
	 {
		 string tmp(src);
		 string::size_type pos = tmp.find(target), targetSize = target.size(), resSize = subs.size();
		 while (pos != string::npos)//found  
		 {
			 tmp.replace(pos, targetSize, subs);
			 pos = tmp.find(target, pos + resSize);
		 }
		 return tmp;
	 }
	 void setFs(fstream *fs);//设置文件c操作对象
	 void setCount(int number);
	 string getFileName();

	 string getBasePath();//获取路径

	 string * fileLoad();

	 //处理乱码问题
	 string UTF8TOGB(const char *);
};




#endif